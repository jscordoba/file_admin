<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
class Sadmin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model('M_admin');

		if ($this->session->multi_user != NULL) {
			// echo "multi ok";
			
			if ($this->session->multi_user == 'true' && $this->session->id_perfil != 1) 
			{
				// echo "true <> 1";
				redirect(base_url().'Main/logout','refresh');
			}elseif($this->session->multi_user == 'false' && $this->session->id_perfil == 1)
			{
				// echo "false == 1";

				$this->load->view('head_style');
				$this->load->view('s_admin/nav');
			}else{
				redirect(base_url().'Main/logout','refresh');
			}
		}else{
			// echo "sin multiuser";
			redirect(base_url().'Main/logout','refresh');
		}
		
	}

	public function index()
	{
		$this->load->view('inicio');
		$this->load->view('footer_style');
	}

	// ===============  Usuario ====================
	public function nuevo_usuario($function=null)
	{
		if ($function!="c") {
			$data['secciones'] = $this->M_admin->sel_seccion();
			$data['oficinas'] = $this->M_admin->sel_oficina();
			$data['perfiles'] = $this->M_admin->sel_rol();

			$this->load->view('s_admin/nuevo_usuario',$data);
			$this->load->view('footer_style');
		}else{
			$res = $this->M_admin->ins_usuario();

			if ($res != "ok") {
				echo $res;
			}else{
				$this->lista_usuario();
				//echo "Nuevo Usuario registrado.";
			}
		}
	}

	public function consultar_usuario()
	{
		$id = $this->uri->segment(3);

		$obtenerId = $this->M_admin->obtenerId($id);
		$obtenerPerfil = $this->M_admin->obtenerPerfil($id);

		if ($obtenerId != false && $obtenerPerfil != false) {
			foreach ($obtenerId->result() as $row) {
				$nombre = $row->usuario_nombre;
				$user = $row->username;
				$clave = $row->password;
				$email = $row->email;
				$seccion = $row->seccion_company_id_seccion_company;
				$oficina = $row->oficina_company_id_oficina_company;
				$status = $row->status;
			}

			foreach ($obtenerPerfil->result() as $perfil) {
				$perfil = $perfil->perfil_usuario_id_perfil_usuario;
			}
			$data = array('id'=>$id,
						  'nombre'=>$nombre,
						  'user'=>$user,
						  'clave'=>$clave,
						  'email'=>$email,
						  'seccion'=>$seccion,
						  'oficina'=>$oficina,
						  'status'=>$status,
						  'perfil'=>$perfil
			);
			$data['secciones'] = $this->M_admin->sel_seccion();
			$data['oficinas'] = $this->M_admin->sel_oficina();
			$data['perfiles'] = $this->M_admin->sel_rol();
		}else{
			$data = '';
			return false;
		}
		$this->load->view('s_admin/editar_usuario',$data);
		$this->load->view('footer_style');
	}

	public function editar_usuario()
	{
		$id = $this->uri->segment(3);
		$data  = array(
			'usuario_nombre' => $this->input->post('nombre_user', true),
			'username' => $this->input->post('user', true),
			'password' => $this->input->post('password', true),
			'email' => $this->input->post('email', true),
			'seccion_company_id_seccion_company' => $this->input->post('id_seccion', true),
			'oficina_company_id_oficina_company' => $this->input->post('id_oficina', true),
			'status' => $this->input->post('status_user', true)
		);
		$data1 = array(
			'perfil_usuario_id_perfil_usuario' => $this->input->post('id_perfil', true)
		);
		$this->M_admin->upd_usuario($id, $data, $data1);
		$this->lista_usuario();
	}
	
	public function lista_usuario()
	{
		$data['usuarios'] = $this->M_admin->selecionar_usuarios();

		$this->load->view('s_admin/lista_usuario',$data);
		$this->load->view('footer_style');
	}

	public function status_usuario(){
		$id = $this->uri->segment(3);
		$this->M_admin->upd_status_usuario($id);
		redirect(base_url().'Sadmin/lista_usuario','refresh');
	}
	// ===============  Fin Usuario ====================


	// ===============  Grupo ====================
	public function nuevo_grupo($function=null)
	{
		if ($function!="c") {
			# code...
			$this->load->view('s_admin/nuevo_grupo');
			$this->load->view('footer_style');
		}else{
			$res = $this->M_admin->ins_grupo();

			if ($res != "ok") {
				echo $res;
			}else{
				// $this->lista_grupo();
				redirect(base_url().'Sadmin/lista_grupo','refresh');
				//echo "Nuevo Usuario registrado.";
			}
		}
	}

	public function lista_grupo($grupo_id=null)
	{
		if ($grupo_id!=null) {
			$data['usuarios'] = $this->M_admin->sel_usuarioGroup($grupo_id);
		}
		$data['grupos'] = $this->M_admin->sel_grupo();
		$cont = 0;
		if ($data['grupos'] != false) {
			foreach ($data['grupos']->result() as $grupo) {
				$grupos[$cont] = $this->M_admin->sel_usuariosXGroup($grupo->id_grupo_usuario);
				$cont++;
			}
			$data['gruposUsuariosXgrupo'] =  $grupos;
		}
		$this->load->view('s_admin/lista_grupo',$data);
		$this->load->view('footer_style');
	}


	public function add_grupoUsuarios()
	{
		# code...
		$res_upd = $this->M_admin->add_grupoUsuarios();

		if ($res_upd =="ok") {
			# code...
			// echo "Grupo actualizado.";

			// $this->lista_grupo();
			redirect(base_url().'Sadmin/lista_grupo','refresh');
		}else{
			echo $res_upd;
		}

	}

	public function consultar_grupo(){
		$id = $this->uri->segment(3);
		$obtenerGrupo = $this->M_admin->obtenerGrupo($id);
		if ($obtenerGrupo != false) {
			foreach ($obtenerGrupo->result() as $row) {
				$nombre = $row->grupo_usuario_nombre;
				$status = $row->status;
			}

			$data = array('id'=>$id,
						  'nombre'=>$nombre,
						  'status'=>$status
			);
		}else{
			$data = '';
			return false;
		}
		$this->load->view('s_admin/editar_grupo',$data);
		$this->load->view('footer_style');
	}

	public function editar_grupo()
	{
		$id = $this->uri->segment(3);
		$data  = array(
			'grupo_usuario_nombre' => $this->input->post('nom_grupo', true),
			'status' => $this->input->post('status', true)
		);
		$this->M_admin->upd_grupo($id, $data);
		redirect(base_url().'Sadmin/lista_grupo','refresh');
	}

	public function status_grupo(){
		$id = $this->uri->segment(3);
		$this->M_admin->upd_status_grupo($id);
		redirect(base_url().'Sadmin/lista_grupo','refresh');
	}
	// ===============  Fin Grupo ====================


	// ===============  Proceso ====================
	public function nuevo_proceso($function=null)
	{
		if ($function!="c") {
			# code...
			$data['responsables'] = $this->M_admin->sel_usuario("administradores");
			$data['secciones'] = $this->M_admin->sel_seccion();
			$data['oficinas'] = $this->M_admin->sel_oficina();

			$this->load->view('s_admin/nuevo_proceso',$data);
			$this->load->view('footer_style');
		}else{
			$res = $this->M_admin->ins_proceso();

			if ($res != "ok") {
				echo $res;
			}else{
				$this->lista_proceso();
				//echo "Nuevo Usuario registrado.";
			}
		}
	}

	public function lista_proceso()
	{
		$data['procesos'] = $this->M_admin->seleccionar_procesos();

		$this->load->view('s_admin/lista_proceso',$data);
		$this->load->view('footer_style');
	}

	public function consultar_proceso(){
		$id = $this->uri->segment(3);

		$obtenerProceso = $this->M_admin->obtenerProceso($id);

		if ($obtenerProceso != false) {
			foreach ($obtenerProceso->result() as $row) {
				$nombre = $row->proceso_nombre;
				$responsable = $row->usuario_id_responsable;
				$seccion = $row->seccion_company_id_seccion_company;
				$oficina = $row->oficina_company_id_oficina_company;
				$status = $row->status;
			}

			$data = array('id'=>$id,
						  'nombre'=>$nombre,
						  'responsable'=>$responsable,
						  'seccion'=>$seccion,
						  'oficina'=>$oficina,
						  'status'=>$status
			);
			$data['responsables'] = $this->M_admin->sel_usuario("administradores");
			$data['secciones'] = $this->M_admin->sel_seccion();
			$data['oficinas'] = $this->M_admin->sel_oficina();
		}else{
			$data = '';
			return false;
		}
		$this->load->view('s_admin/editar_proceso',$data);
		$this->load->view('footer_style');

	}

	public function editar_proceso()
	{
		$id = $this->uri->segment(3);
		$proceso_update = date('Y-m-d H:i:s');
		$data  = array(
			'proceso_nombre' => $this->input->post('nom_proceso', true),
			'proceso_update' => $proceso_update,
			'usuario_id_creacion' => $this->input->post('password', true),
			'usuario_id_responsable' =>  $this->input->post('id_responsable',true),
			'seccion_company_id_seccion_company' => $this->input->post('id_seccion', true),
			'oficina_company_id_oficina_company' => $this->input->post('id_oficina', true),
			'status' => $this->input->post('status', true)
		);
		$this->M_admin->upd_proceso($id, $data);
		$this->lista_proceso();
	}

	public function status_proceso(){
		$id = $this->uri->segment(3);
		$this->M_admin->upd_status_proceso($id);
		redirect(base_url().'Sadmin/lista_proceso','refresh');
	}
	// =============== Fin  Proceso ====================

	// ===============  Seccion ====================
	public function nueva_seccion($function=null)
	{
		if ($function!="c") {
			# code...
			$this->load->view('s_admin/nueva_seccion');
			$this->load->view('footer_style');
		}else{
			$res = $this->M_admin->ins_seccion();

			if ($res != "ok") {
				echo $res;
			}else{
				$this->lista_seccion();
				//echo "Nuevo Usuario registrado.";
			}
		}
	}

	public function lista_seccion()
	{
		$data['secciones'] = $this->M_admin->sel_seccion();

		$this->load->view('s_admin/lista_seccion',$data);
		$this->load->view('footer_style');
	}

	public function consultar_seccion(){
		$id = $this->uri->segment(3);
		$obtenerSeccion = $this->M_admin->obtenerSeccion($id);
		if ($obtenerSeccion != false) {
			foreach ($obtenerSeccion->result() as $row) {
				$nombre = $row->seccion_company_nombre;
				$status = $row->status;
			}

			$data = array('id'=>$id,
						  'nombre'=>$nombre,
						  'status'=>$status
			);
		}else{
			$data = '';
			return false;
		}
		$this->load->view('s_admin/editar_seccion',$data);
		$this->load->view('footer_style');
	}

	public function editar_seccion()
	{
		$id = $this->uri->segment(3);
		$seccion_update = date('Y-m-d H:i:s');
		$data  = array(
			'seccion_company_nombre' => $this->input->post('nom_seccion', true),
			'seccion_company_update' => $seccion_update,
			'status' => $this->input->post('status', true)
		);
		$this->M_admin->upd_seccion($id, $data);
		$this->lista_seccion();
	}

	public function status_seccion(){
		$id = $this->uri->segment(3);
		$this->M_admin->upd_status_seccion($id);
		redirect(base_url().'Sadmin/lista_seccion','refresh');
	}
	// ===============  Fin Seccion ====================

	// ===============  Oficina ====================
	public function nueva_oficina($function=null)
	{
		if ($function!="c") {
			# code...
			$this->load->view('s_admin/nueva_oficina');
			$this->load->view('footer_style');
		}else{
			$res = $this->M_admin->ins_oficina();

			if ($res != "ok") {
				echo $res;
			}else{
				$this->lista_oficina();
				//echo "Nuevo Usuario registrado.";
			}
		}
	}

	public function lista_oficina()
	{
		$data['oficinas'] = $this->M_admin->sel_oficina();

		$this->load->view('s_admin/lista_oficina',$data);
		$this->load->view('footer_style');
	}

	public function consultar_oficina(){
		$id = $this->uri->segment(3);
		$obtenerOficina = $this->M_admin->obtenerOficina($id);
		if ($obtenerOficina != false) {
			foreach ($obtenerOficina->result() as $row) {
				$nombre = $row->oficina_company_nombre;
				$status = $row->status;
			}

			$data = array('id'=>$id,
						  'nombre'=>$nombre,
						  'status'=>$status
			);
		}else{
			$data = '';
			return false;
		}
		$this->load->view('s_admin/editar_oficina',$data);
		$this->load->view('footer_style');
	}

	public function editar_oficina()
	{
		$id = $this->uri->segment(3);
		$seccion_update = date('Y-m-d H:i:s');
		$data  = array(
			'oficina_company_nombre' => $this->input->post('nom_oficina', true),
			'oficina_company_update' => $seccion_update,
			'status' => $this->input->post('status', true)
		);
		$this->M_admin->upd_oficina($id, $data);
		$this->lista_oficina();
	}

	public function status_oficina(){
		$id = $this->uri->segment(3);
		$this->M_admin->upd_status_oficina($id);
		redirect(base_url().'Sadmin/lista_oficina','refresh');
	}
	// ===============  Fin Oficina ====================

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */