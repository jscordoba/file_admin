<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		//Do your magic here

		if ($this->session->multi_user != NULL) {
			// echo "multi ok";
			
			if ($this->session->multi_user == 'true' && $this->session->id_perfil != 3) 
			{
				// echo "true <> 1";
				redirect(base_url().'Main/logout','refresh');
			}elseif($this->session->multi_user == 'false' && $this->session->id_perfil == 3)
			{
				// echo "false == 1";

				$this->load->model('M_admin');
				$this->load->model('M_user');
				$data['grupos'] = $this->M_user->grupos_nav();
				$this->load->view('head_style');
				$this->load->view('user/nav',$data);
			}else{
				redirect(base_url().'Main/logout','refresh');
			}
		}else{
			// echo "sin multiuser";
			redirect(base_url().'Main/logout','refresh');
		}
	}

	public function index(){
		$this->load->view('inicio');
		$this->load->view('footer_style');
	}

	public function lista(){
		$validar = $this->M_user->validar();
		if ($validar != false) {
			foreach ($validar->result() as $row_validar) {
				$grupo = $row_validar->grupo;
				$componente = $row_validar->componente;	
				$tipo = $row_validar->tipo;	
			}

			if ($tipo == 1) { //recurso
				$data['recursos'] = $this->M_user->recurso($componente,$grupo,$tipo);
				$this->load->view('User/lista_recurso',$data);
				$this->load->view('footer_style');
			}else if ($tipo == 2) { //Elemento
				$data['elementos'] = $this->M_user->elemento($grupo,$componente);
				$this->load->view('User/lista_elemento',$data);
				$this->load->view('footer_style');
			}else if ($tipo == 3) { //proceso
				$data['procesos'] = $this->M_user->procesos();
				$this->load->view('User/lista_proceso',$data);
				$this->load->view('footer_style');
			}
		}else{
			$this->load->view('inicio');
			$this->load->view('footer_style');
		}
	}

	public function lista_recurso_user($proceso_id=null){
		// $data['recursos'] = $this->M_user->sel_recurso_user($proceso_id);
		$data = array(
			'recursos' => $this->M_user->recursos()
		);

		$this->load->view('User/lista_recurso',$data);
		$this->load->view('footer_style');
	}

	public function lista_elemento($id_recurso=null){
		// echo $this->session->multi_user;
		// echo $this->session->id_perfil;

		// if ($id_recurso!=null) {
			# code...
			// $data['elementos'] = $this->M_user->sel_elemento_user($id_recurso);
		// }else{
			$data['elementos'] = $this->M_user->elementos();
		// }

		$this->load->view('User/lista_elemento',$data);
		$this->load->view('footer_style');
	}

	public function lista_version($recurso_has_elemento=null){
		$data['versiones'] = $this->M_admin->sel_version_user($recurso_has_elemento);
		$data['version'] = $recurso_has_elemento;

		$this->load->view('User/lista_version',$data);
		$this->load->view('footer_style');
	}

	public function listar_recursos_proceso(){
		$proceso = $this->uri->segment(3);
		$data = array(
			'recursos' => $this->M_user->recursos_X_proceso1($proceso)
		);

		$this->load->view('User/listar_recursos_proceso',$data);
		$this->load->view('footer_style');
	}

	public function listar_elementos_recurso(){
		$recurso = $this->uri->segment(3);
		$data = array(
			'elementos' => $this->M_user->elementos_x_recurso($recurso)
		);

		$this->load->view('User/listar_elementos_recurso',$data);
		$this->load->view('footer_style');
	}

	public function listar_documento_elemento(){
		$elemento = $this->uri->segment(3);
		$recurso = $this->uri->segment(4);
		$data = array(
			'documentos' => $this->M_user->documentos_x_elemento($elemento,$recurso)
		);

		$this->load->view('User/listar_documento_elemento',$data);
		$this->load->view('footer_style');
	}

	public function accesos(){
		$grupo = $this->uri->segment(3);
		$result = $this->M_user->accesos_x_grupo($grupo);
		if ($result != false) {
			foreach ($result->result() as $row) {
				// echo "<script>console.log(".json_encode($row).");</script>";
				if ($row->tipo == 1) { // Recurso
					// echo "<script>console.log(".$row->tipo.");</script>";
					$data['recursos'] = $this->M_user->recursos_x_grupo($row->grupo,$row->componente,$row->tipo);
				}else if ($row->tipo == 2) { // Elemento
					// echo "<script>console.log(".$row->tipo.");</script>";
				    // echo "<script>console.log(".json_encode($row->componente).");</script>";
					$data['elementos'] = $this->M_user->elementos_x_grupo($row->grupo,$row->componente,$row->tipo);
				}else if($row->tipo == 3) { // Proceso
					// echo "<script>console.log(".$row->tipo.");</script>";
					$data['procesos'] = $this->M_user->procesos_x_grupo($row->grupo,$row->componente,$row->tipo);
				}
			}

		$this->load->view('User/accesos',$data);
		$this->load->view('footer_style');
		}
	}

	public function user_recursos($proceso){
		$data['recursos'] = $this->M_user->recursos_X_proceso1($proceso);
		$this->load->view('User/lista_recurso',$data);
		$this->load->view('footer_style');
	}

	public function elemento_procesos(){
		$elemento = $this->uri->segment(3);
		$data['procesos'] = $this->M_user->elemento_procesos($elemento);
		
		$this->load->view('User/elemento_procesos',$data);
		$this->load->view('footer_style');
	}

	public function elemento_procesos_recursos(){
		// $elemento = $this->uri->segment(3);
		$proceso = $this->uri->segment(4);
		$data['recursos'] = $this->M_user->recursos_X_proceso1($proceso);
		$this->load->view('User/elemento_procesos_recursos',$data);
		$this->load->view('footer_style');
	}

	public function elemento_procesos_recursos_elemento(){
		$elemento = $this->uri->segment(3);
		$recurso = $this->uri->segment(5);
		$data = array(
			'elementos' => $this->M_user->elemento_procesos_recurso($recurso,$elemento)
		);

		$this->load->view('User/elemento_procesos_recursos_elemento',$data);
		$this->load->view('footer_style');
	}
	
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
?>

