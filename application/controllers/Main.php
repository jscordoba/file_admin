<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('login');
	}
	

	public function login(){
		$usuario = $this->input->post('usuario');
		$clave = $this->input->post('clave');

		$sql = "SELECT u.usuario_id,u.usuario_nombre,u.username,u.password,u.email,u.fecha_creacion,pu.id_perfil_usuario, pu.perfil_usuario_nombre,s.seccion_company_nombre,o.oficina_company_nombre
				FROM usuario u
					JOIN usuario_has_perfil up ON (u.usuario_id=up.usuario_usuario_id)
					JOIN perfil_usuario pu ON (pu.id_perfil_usuario=up.perfil_usuario_id_perfil_usuario)
					JOIN seccion_company s ON(u.seccion_company_id_seccion_company=s.id_seccion_company)
					JOIN oficina_company o ON(u.oficina_company_id_oficina_company=o.id_oficina_company)
				WHERE u.username = '$usuario'
				AND u.password = '$clave' AND u.status = 'Activo'
				";
		$query = $this->db->query($sql);
		if ( ($query->num_rows()) > 0){
			if ( ($query->num_rows()) > 1){
				$cont = 0;
				foreach ($query->result() as $login){
					$sesion_data[$cont++] = array(
						'usuario_id' => $login->usuario_id,
						'usuario_nombre' => $login->usuario_nombre,
						'usuario' => $login->username,
						'clave' => $login->password,
						'email' => $login->email,
						'id_perfil' =>  $login->id_perfil_usuario,
						'nombre_perfil' => $login->perfil_usuario_nombre,
						'multi_user' => 'true'
					);
				}
				$session_log ['login'] = $sesion_data;
				$this->session->set_userdata($sesion_data[0]);
				$this->load->view('selector_perfil',$session_log);
			}else{
				foreach ($query->result() as $login){
					$sesion_data = array(
											'usuario_id' => $login->usuario_id,
											'usuario_nombre' => $login->usuario_nombre,
											'usuario' => $login->username,
											'clave' => $login->password,
											'email' => $login->email,
											'id_perfil' =>  $login->id_perfil_usuario,
											'nombre_perfil' => $login->perfil_usuario_nombre,
											'seccion_nombre' => $login->seccion_company_nombre,
											'oficina_nombre' => $login->oficina_company_nombre,
											'multi_user' => 'false'
											);

					switch ($login->id_perfil_usuario){
						case 1:
							$pag_perfil = "Sadmin";
							break;
						case 2:
							$pag_perfil = "Admin";
							break;
						case 3:
							$pag_perfil = "User";
							break;
						default:
							break;
					}
				}
				$this->session->set_userdata($sesion_data);
				redirect(base_url().$pag_perfil,'refresh');
			}
		}else{
			redirect('#','refresh');
		}
	}

	public function multi_user($num_form){
		// echo "PRUEBA MULTIUSUARIO - $num_form";

		$sql = "SELECT u.usuario_id,u.email,u.fecha_creacion,pu.id_perfil_usuario, pu.perfil_usuario_nombre,s.seccion_company_nombre,o.oficina_company_nombre
				FROM usuario u
					JOIN usuario_has_perfil up ON (u.usuario_id=up.usuario_usuario_id)
					JOIN perfil_usuario pu ON (pu.id_perfil_usuario=up.perfil_usuario_id_perfil_usuario)
					JOIN seccion_company s ON(u.seccion_company_id_seccion_company=s.id_seccion_company)
					JOIN oficina_company o ON(u.oficina_company_id_oficina_company=o.id_oficina_company)
				WHERE 
					pu.id_perfil_usuario = '".$this->input->post('perfil_id')."'
					AND u.username = '".$this->input->post('usuario')."'
					AND u.password = '".$this->input->post('clave')."'
					AND u.status = 'Activo'
				";

		// echo "$sql";

		$query = $this->db->query($sql);

		//print_r($this->session->userdata());

		try 
		{
			
			foreach ($query->result() as $log)
			{
				$sesion_data = array(
							'id_perfil' =>  $log->id_perfil_usuario,
							'nombre_perfil' => $log->perfil_usuario_nombre,
							'seccion_nombre' => $log->seccion_company_nombre,
							'oficina_nombre' => $log->oficina_company_nombre,
							'multi_user' => 'false'
							);
			}

			$this->session->set_userdata( $sesion_data );

			// print_r($this->session->userdata());
			// echo $this->input->post('perfil_nombre');
			// echo $this->input->post('perfil_id');

			redirect(base_url().$this->input->post('perfil_nombre'),'refresh');

		} catch (Exception $e) 
		{
			echo "Error obteniendo los resultados de Login.";	
			redirect(base_url().$this->input->post('perfil_nombre'),'refresh');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'','refresh');
	}

	public function ver_version($document_id_document){
		$this->load->model('M_admin');
		$data['documento'] = $this->M_admin->obt_document($document_id_document);
		$this->load->view('print_document',$data);
	}

	public function lista_usuarioGroup(){
		$this->load->model('M_admin');
		$grupo_id = $this->input->post('id_grupo');
		if ($grupo_id!=null) {
			$usuarios = $this->M_admin->sel_usuarioGroup($grupo_id);
		}
		if ($usuarios!=null) {
			 if ($usuarios->num_rows() > 0) {
	            foreach ($usuarios->result() as $Usuario): ?>
	                 <tr>
	                    <td><?= $Usuario->usuario_id ?></td>
	                    <td><?= $Usuario->usuario_nombre ?></td>
	                    <td><?= $Usuario->seccion_company_nombre ?></td>
	                    <td><?= $Usuario->oficina_company_nombre ?></td>
	                    	<!-- <input type="hidden" class="" name="chk_addGroup[][id_user]"> -->
	                    <td><input type="checkbox" class="" name="chk_addGroup[<?= $grupo_id?>][<?= $Usuario->usuario_id ?>]"></td>
	                </tr>
	                <!-- <input type="hidden" name="usuario_id[]" value=""> -->
	            <?php endforeach;
	        }else{
	        	echo "Usuarios con respuesta 0.";
	        }
		}else{
        	echo "Sin usuarios Disponibles para este grupo.";
        }
	}

	public function lista_usuarioGroupUsuario(){
		$this->load->model('M_admin');
		$id_grupo = $this->input->post('id');
		if ($id_grupo!=null) {
			$result = $this->M_admin->usuario_has_grupo($id_grupo);
			if ($result != false) {
				foreach ($result->result() as $row) {
					$result1 = $this->M_admin->consult_user($row->usuario_usuario_id,$row->grupo_usuario_id_grupo_usuario);
					if ($result1 != false) {
						foreach ($result1->result() as $resultuser){ 
		                 echo'<tr>
		                 	<td class="text-center hidden"><input type="text" id="grupo" value="'.$resultuser->id_grupo_usuario.'"></td>
		                    <td class="text-center">'.$resultuser->usuario_id.'</td>
		                    <td class="text-center">'.$resultuser->usuario_nombre.'</td>
		                    <td class="text-center">'.$resultuser->email.'</td>
		                    <td class="text-center">'.$resultuser->seccion_company_nombre.'</td>
		                    <td class="text-center">'.$resultuser->oficina_company_nombre.'</td>
		                    <td class="text-center">
		                    	<button type="button" class="btn btn-xs btn-danger" title="Eliminar usuario de este grupo" onclick="remover_usuario_grupo('.$resultuser->usuario_id.');"><i class="fa fa-trash"></i></button>
		                    </td>
		                </tr>';
		            	}
					}
				}
			}else{
				echo'<tr>
		             <td colspan="5" class="text-center">Este grupo no tiene usuarios asignados</td>
		             </tr>';
			}
		}
	}

	public function lista_documentos_elemento(){
		$this->load->model('M_admin');
		$id_elemento = $this->input->post('id');
		if ($id_elemento!=null) {
			$result = $this->M_admin->documentos_has_elemento($id_elemento);
			if ($result != false) {
				foreach ($result->result() as $row) {?>
	                <tr>
	                	<!-- <td class="hidden"><input type="text" id="elemento" value="<?= $row->elemento ?>"></td> -->
	                    <td class="text-center"><?= $row->id_document ?></td>
	                    <td class="text-center"><a href="<?= base_url() ?>files/<?= $row->nombre ?>" target="_blank"><?= $row->nombre ?></a></td>
	                    <td class="text-center"><?= $row->status ?></td>

	                    <td class="text-center">
	                    	<?php if ($row->status == "Activo") {?>
	                    		<button onclick="status_document(<?= $row->id_document ?>,<?= $row->elemento ?>,<?= $row->recurso ?>);" class="btn btn-xs btn-success"><i class="fa fa-toggle-on"></i></button>
	                    	<?php }else{?>
	                    		<button class="btn btn-xs btn-default" onclick="status_document(<?= $row->id_document ?>,<?= $row->elemento ?>,<?= $row->recurso ?>);"><i class="fa fa-toggle-off"></i></button>
	                    	<?php	} ?>
	                    </td>
	                </tr>
	        <?php
            	}
			}else{
				echo'<tr>
		             <td colspan="4" class="text-center">Este elemento no tiene documentos</td>
		             </tr>';
			}

		}
	}

	public function lista_procesos_has_elemento(){
		$i = 1;
		$this->load->model('M_admin');
		$id_elemento = $this->input->post('id');
		if ($id_elemento!=null) {
			$result = $this->M_admin->recurso_has_elemento($id_elemento);
			if ($result != false) {
				foreach($result->result() as $row) {
					// echo "<script>console.log(".json_encode($row).");</script>";
					$result1 = $this->M_admin->consult_recursos($row->recurso_id_recurso);
					if ($result1 != false) {
						foreach($result1->result() as $rowrecurso) {

							$result2 = $this->M_admin->consult_proceso_has_recursos($rowrecurso->proceso_id_proceso);
							foreach ($result2->result() as $rowprocesos) {
								echo 	'<tr>
											<td class="text-center">'.$i++.'</td>
											<td class="text-center">'.$rowrecurso->recurso_nombre.'</td>
											<td class="text-center">'.$rowprocesos->proceso_nombre.'</td>
										</tr>';
							}
						}
					}else{
					echo'<tr>
		             	<td colspan="3" class="text-center">Recurso asignado no existe</td>
		            	 </tr>';
					}
				}
			}else{
				echo'<tr>
		             <td colspan="3" class="text-center">Este elemento no tiene recursos asignados</td>
		             </tr>';
			}
		}
	}

	public function status_document(){
		$this->load->model('M_admin');
		$documento = $this->input->post('documento');
		$elemento = $this->input->post('elemento');
		$recurso = $this->input->post('recurso');
		$result = $this->M_admin->upd_status_document($documento,$elemento,$recurso);
		if ($result!= false) {
			$result = $this->M_admin->documentos_has_elemento($elemento);
			if ($result != false) {
				foreach ($result->result() as $row) {?>
	                <tr>
	                	<td class="hidden"><input type="text" id="elemento" value="<?= $row->elemento ?>"></td>
	                    <td class="text-center"><?= $row->id_document ?></td>
	                    <td class="text-center"><a href="<?= base_url() ?>files/<?= $row->nombre ?>" target="_blank"><?= $row->nombre ?></a></td>
	                    <td class="text-center"><?= $row->status ?></td>

	                    <td class="text-center">
	                    	<?php if ($row->status == "Activo") {?>
	                    		<button onclick="status_document(<?= $row->id_document ?>,<?= $row->elemento ?>,<?= $row->recurso ?>);" class="btn btn-xs btn-success"><i class="fa fa-toggle-on"></i></button>
	                    	<?php }else{?>
	                    		<button class="btn btn-xs btn-default" onclick="status_document(<?= $row->id_document ?>,<?= $row->elemento ?>,<?= $row->recurso ?>);"><i class="fa fa-toggle-off"></i></button>
	                    	<?php	} ?>
	                    </td>
	                </tr>
	        <?php
            	}
			}else{
				echo'<tr>
		             <td colspan="4" class="text-center">Este elemento no tiene documentos</td>
		             </tr>';
			}
		}
	}

	public function cambiar_estado_documento(){
		$this->load->model('M_admin');
		$proceso = $this->uri->segment(3);
		$recurso = $this->uri->segment(4);
		$elemento = $this->uri->segment(5);
		$documento = $this->uri->segment(6);
		// echo "<script>console.log(".$proceso.",".$recurso.",".$elemento.",".$documento.")</script>";
		$result = $this->M_admin->upd_status_document($documento,$elemento,$recurso);
		if ($result != false) {
			$data['documentos'] = $this->M_admin->documentos_has_elemento1($proceso,$recurso,$elemento);
			$data['elementos'] = $this->M_admin->sel_elementos();
			redirect(base_url().'Admin/documents_has_element/'.$proceso.'/'.$recurso.'/'.$elemento.'','refresh');
		}else{
			$data = '';
			return false;
			redirect(base_url().'Admin/documents_has_element/'.$proceso.'/'.$recurso.'/'.$elemento.'','refresh');
		}
		

	}

	public function remover_usuario_grupo(){
		$this->load->model('M_admin');
		$usuario = $this->input->post('id');
		$grupo = $this->input->post('grupo');
		$result = $this->M_admin->remover_usuario_grupo($usuario,$grupo);
		if ($result != false) {
			echo 1;
		}else{
			echo 2;
		}
	}

	public function mostar_grupos_proceso(){
		$this->load->model('M_admin');
		$proceso = $this->input->post('id');
		$result = $this->M_admin->grupos_x_proceso($proceso);
		if ($result != false) {
			foreach ($result->result() as $row) {
				echo '	<tr>
							<td class="text-center">'.$row->id_grupo_usuario.'</td>
							<td class="text-center">'.$row->grupo_usuario_nombre.'</td>
							<td class="text-center">
								<button class="btn btn-danger" onclick="eliminar_grupo_proceso('.$row->id_grupo_usuario.','.$proceso.');"><i class="fa fa-trash"></i></buttton>
							</td>
						</tr>';
			}
		}else{
				echo '	<tr>
							<td colspan="3" class="text-center">Este proceso no tiene grupos asignados aun.</td>
						</tr>';
		}
	}

	public function mostar_grupos_recurso(){
		$this->load->model('M_admin');
		$recurso = $this->input->post('id');
		$result = $this->M_admin->grupos_x_recurso($recurso);
		if ($result != false) {
			foreach ($result->result() as $row) {
				echo '	<tr>
							<td class="text-center">'.$row->id_grupo_usuario.'</td>
							<td class="text-center">'.$row->grupo_usuario_nombre.'</td>
							<td class="text-center">
								<button class="btn btn-danger" onclick="eliminar_grupo_recurso('.$row->id_grupo_usuario.','.$recurso.');"><i class="fa fa-trash"></i></buttton>
							</td>
						</tr>';
			}
		}else{
			echo '	<tr>
							<td colspan="3" class="text-center">Este recurso no tiene grupos asignados aun.</td>
						</tr>';
		}
	}

	public function mostar_grupos_elemento(){
		$this->load->model('M_admin');
		$elemento = $this->input->post('id');
		$result = $this->M_admin->grupos_x_elemento($elemento);
		if ($result != false) {
			foreach ($result->result() as $row) {
				echo '	<tr>
							<td class="text-center">'.$row->id_grupo_usuario.'</td>
							<td class="text-center">'.$row->grupo_usuario_nombre.'</td>
							<td class="text-center">
								<button class="btn btn-sm btn-danger" onclick="eliminar_grupo_elemento('.$row->id_grupo_usuario.','.$elemento.');"><i class="fa fa-trash"></i></buttton>
							</td>
						</tr>';
			}
		}else{
			echo '	<tr>
						<td colspan="2" class="text-center">Este elemento no tiene grupos asignados aun.</td>
					</tr>';
		}
	}

	public function eliminar_grupo_proceso(){
		$this->load->model('M_admin');
		$grupo = $this->input->post('grupo');
		$proceso = $this->input->post('proceso');
		$rpt = $this->M_admin->delete_grupos_x_proceso($grupo,$proceso);
		if ($rpt != false) {
			echo true;
		}else{
			echo false;
		}
	}

	public function eliminar_grupo_recurso(){
		$this->load->model('M_admin');
		$grupo = $this->input->post('grupo');
		$recurso = $this->input->post('recurso');
		$rpt = $this->M_admin->delete_grupos_x_recurso($grupo,$recurso);
		if ($rpt != false) {
			echo true;
		}else{
			echo false;
		}
	}

	public function eliminar_grupo_elemento(){
		$this->load->model('M_admin');
		$grupo = $this->input->post('grupo');
		$elemento = $this->input->post('elemento');
		$rpt = $this->M_admin->delete_grupos_x_elemento($grupo,$elemento);
		if ($rpt != false) {
			echo true;
		}else{
			echo false;
		}
	}

	public function seleccionar_recursos_x_elemento(){
		$i = 1;
		$this->load->model('M_admin');
		$id = $this->input->post('id');
		$result = $this->M_admin->seleccionar_recursos_x_elemento($id);
		if ($result != false) {
			foreach ($result->result() as $row) {
				echo '<option value="'.$row->id_recurso.'">'.$row->recurso_nombre.'</option>';
			}
		}else{
			echo false;
		}
	}

	public function seleccionar_proceso_x_recurso(){
		$i = 1;
		$this->load->model('M_admin');
		$id = $this->input->post('id');
		$result = $this->M_admin->seleccionar_proceso_x_recurso($id);
		if ($result != false) {
			foreach ($result->result() as $row) {
				echo json_encode($row);
			}
		}else{
			echo false;
		}
	}

	public function seleccionar_recursos_no_asignados(){
		$this->load->model('M_admin');
		$id = $this->input->post('id');
		$result = $this->M_admin->seleccionar_recursos_no_asignados($id);
		if ($result != false) {
			foreach ($result->result() as $row) {
				echo '<option value="'.$row->id_recurso.'">'.$row->recurso_nombre.'</option>';
			}
		}else{
			echo false;
		}
	}

}
