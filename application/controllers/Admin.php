<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		if ($this->session->multi_user != NULL) {
			// echo "multi ok";
			
			if ($this->session->multi_user == 'true' && $this->session->id_perfil != 2) 
			{
				// echo "true <> 1";
				redirect(base_url().'Main/logout','refresh');
			}elseif($this->session->multi_user == 'false' && $this->session->id_perfil == 2)
			{
				// echo  $this->session->id_perfil;
				// echo  $this->session->multi_user;

				$this->load->model('M_admin');
				
				$this->load->view('head_style');
				$this->load->view('admin/nav');
			}else{
				// echo  $this->session->id_perfil;
				// echo  $this->session->multi_user;
				redirect(base_url().'Main/logout','refresh');
			}
		}else{
			// echo "sin multiuser";
			redirect(base_url().'Main/logout','refresh');
		}

		$this->load->helper(array('form','url'));

	}

	public function index()
	{
		$this->load->view('inicio');
		$this->load->view('footer_style');
	}

	public function lista_proceso()
	{
		// $data['grupos'] = $this->M_admin->sel_grupo1();
		$data['procesos'] = $this->M_admin->sel_proceso("responsable");

		$this->load->view('admin/lista_proceso',$data);
		$this->load->view('footer_style');
	}

	public function nuevo_recurso($function=null)
	{
		if ($function=="c") {
			$res = $this->M_admin->ins_recurso();
			if ($res != "ok") {
				echo $res;
			}else{
				$this->lista_recurso();
			}
		}else{
			$data['grupos'] = $this->M_admin->sel_grupo();
			$data['procesos'] = $this->M_admin->sel_proceso("responsable");

			$this->load->view('admin/nuevo_recurso',$data);
			$this->load->view('footer_style');
		}
	}

	public function lista_recurso($id_proceso=null)
	{
		if ($id_proceso!=null && $id_proceso!="r") {
			$result = $this->M_admin->sel_proceso($id_proceso);
			if ($result) {
				$data['proceso'] = $result->row();
			}
		}
		$data['recursos'] = $this->M_admin->sel_recurso($id_proceso,"p");
		$this->load->view('admin/lista_recurso',$data);
		$this->load->view('footer_style');
	}

	public function nuevo_elemento($function=null)
	{
		if ($function!="a" && $function!="c") {
			// $data['recursos'] = $this->M_admin->sel_recurso("responsable","p");
			$data['elementos'] = $this->M_admin->sel_elementos();
			$data['grupos'] = $this->M_admin->sel_grupo1(2);
			$this->load->view('admin/nuevo_elemento',$data);
			$this->load->view('footer_style');
		}elseif($function=="a"){
			$res = $this->M_admin->ins_elemento("asignar");
			if ($res != "ok") {
				echo $res;
			}else{
				echo $res;
				redirect(base_url().'Admin/lista_elementos','refresh');
			}
		}elseif($function=="c"){
			$res = $this->M_admin->ins_elemento("crear");

			if ($res != "ok") {
				echo $res;
			}else{
				echo $res;
				redirect(base_url().'Admin/lista_elementos','refresh');
			}
		}
	}

	

	public function crear_elemento()
	{
	
		$this->load->view('admin/crear_elemento');
		$this->load->view('footer_style');
	}

	public function lista_elemento($id_recurso=null)
	{
		if ($id_recurso!=null && $id_recurso!="r") {
			$data['recurso'] = $this->M_admin->sel_recurso($id_recurso,"r");
			// if ($result) {
			// 	$data['recurso'] = $result->row();
			// }
			$data['elementos'] = $this->M_admin->sel_elemento($id_recurso);
		}else{
			$data['elementos'] = $this->M_admin->sel_elemento("todo");	
		}
		$data['grupos'] = $this->M_admin->sel_grupo();
		$this->load->view('admin/lista_elementos',$data);
		$this->load->view('footer_style');
	}

	public function elementos_has_recurso(){
		$proceso = $this->uri->segment(3);
		$recurso = $this->uri->segment(4);
		if ($recurso != 0) {
			$data['recurso'] = $this->M_admin->seleccionar_recurso($recurso);	
			$data['elementos'] = $this->M_admin->sel_elemento_new($recurso);
			$data['grupos'] = $this->M_admin->sel_grupo();
			$this->load->view('admin/lista_elemento',$data);
			$this->load->view('footer_style');
		}else{
			// $this->load->view('admin/lista_elemento',$data);
			// $this->load->view('footer_style');
		}
	}

	public function lista_elementos()
	{
		$data['elementos'] = $this->M_admin->sel_elementos_usuarios();

		$this->load->view('admin/lista_elementos',$data);
		$this->load->view('footer_style');
	}

	public function nueva_version($id_recurso_has_elemento=null)
	{

		if ($id_recurso_has_elemento!="c") {
			# code...
			$data['id_recurso_has_elemento'] = $id_recurso_has_elemento;

			$this->load->view('admin/nueva_version',$data);
			$this->load->view('footer_style');

		}else{
			$res = $this->M_admin->ins_version();

			// if ($res != "ok") {
			// 	echo $res;
			// }else{
			// 	echo $res;
			// }
		}

	}

	public function lista_version($id_recurso_has_elemento=null)
	{
		$data['versiones'] = $this->M_admin->sel_version($id_recurso_has_elemento);
		$data['version'] = $id_recurso_has_elemento;

		$this->load->view('admin/lista_version',$data);
		$this->load->view('footer_style');
	}

	public function permiso_acceso($pantalla)
	{
		$result = $this->M_admin->ins_permiso();
		if ($result != "ok") {
				echo $result;
			}else{
				echo $result;
				redirect(base_url(),'refresh');

				// if ($pantalla == 'r') {
				// 	redirect(base_url().'Admin/lista_recurso','refresh');
				// }elseif($pantalla == 'e'){
				// 	redirect(base_url().'Admin/lista_elemento','refresh');
				// }elseif ($pantalla == 'p') {
				// 	redirect(base_url().'Admin/lista_proceso','refresh');
				// }
			}
	}

	public function asignar_acceso($tipo){
		if ($tipo != "") {
			$result = $this->M_admin->insertar_permiso($tipo);
			if ($result != false) {
				if ($tipo === "Process") {
					redirect(base_url().'Admin/lista_proceso','refresh');
				}else if ($tipo === "Element") {
					redirect(base_url().'Admin/lista_elemento','refresh');
				}else if($tipo === "Resource"){
					redirect(base_url().'Admin/lista_recurso','refresh');
				}
			}else{
				echo $result;
			}
		}
	
	}

	public function consultar_recurso(){
		$id = $this->uri->segment(3);
		$obtenerRecurso = $this->M_admin->obtenerRecurso($id);
		if ($obtenerRecurso != false) {
			foreach ($obtenerRecurso->result() as $row) {
				$recurso_nombre = $row->recurso_nombre;
				$proceso_id_proceso = $row->proceso_id_proceso;
				$tipo_recurso_id_tipo_recurso = $row->tipo_recurso_id_tipo_recurso;
				$status = $row->status;
			}

			$data = array('id'=>$id,
						  'recurso_nombre'=>$recurso_nombre,
						  'proceso_id_proceso'=>$proceso_id_proceso,
						  'tipo_recurso_id_tipo_recurso'=>$tipo_recurso_id_tipo_recurso,
						  'status'=>$status
			);
			$data['grupos'] = $this->M_admin->sel_grupo();
			$data['procesos'] = $this->M_admin->sel_proceso("responsable");
		}else{
			$data = '';
			return false;
		}
		$this->load->view('admin/editar_recurso',$data);
		$this->load->view('footer_style');
	}

	public function editar_recurso()
	{
		$id = $this->uri->segment(3);
		$recurso_update = date('Y-m-d H:i:s');
		$data  = array(
			'recurso_nombre' => $this->input->post('nom_recurso', true),
			'recurso_update' => $recurso_update,
			'proceso_id_proceso' => $this->input->post('id_proceso', true),
			'tipo_recurso_id_tipo_recurso' => $this->input->post('id_grupo_usuario', true),
			'status' => $this->input->post('status', true)
		);
		$this->M_admin->upd_recurso($id, $data);
		$this->lista_recurso();
	}

	public function consultar_elemento(){
		$id = $this->uri->segment(3);
		$obtenerElemento = $this->M_admin->obtenerElemento($id);
		if ($obtenerElemento != false) {
			foreach ($obtenerElemento->result() as $row) {
				$elemento_nombre = $row->elemento_nombre;
				$status = $row->status;
			}

			$data = array('id'=>$id,
						  'elemento_nombre'=>$elemento_nombre,
						  'status'=>$status
			);
		}else{
			$data = '';
			return false;
		}
		$this->load->view('admin/editar_elemento',$data);
		$this->load->view('footer_style');
	}

	public function editar_elemento()
	{
		$id = $this->uri->segment(3);
		$elemento_update = date('Y-m-d H:i:s');
		$data  = array(
			'elemento_nombre' => $this->input->post('nom_elemento', true),
			'elemento_update' => $elemento_update,
			'status' => $this->input->post('status', true)
		);
		$this->M_admin->upd_elemento($id, $data);
		redirect(base_url().'Admin/lista_elementos','refresh');
	}


	public function status_elemento(){
		$id = $this->uri->segment(3);
		$this->M_admin->upd_status_elemento($id);
		redirect(base_url().'Admin/lista_elementos','refresh');
	}

	//=================================================================================================================================

	public function upload_file(){
		$data['elementos'] = $this->M_admin->sel_elementos();
		$this->load->view('admin/upload',$data);
		$this->load->view('footer_style');
	}

	public function do_upload(){
    	$elemento = $this->input->post('id_elemento');
    	$recurso = $this->input->post('recursos');
    	$proceso = $this->input->post('proceso_id');
    	$archivo = $this->input->post('userfile_name');

    	$config['upload_path'] = './files/';
    	$config['allowed_types'] = 'pdf';
    	$config['file_name'] = $archivo.'_P'.$proceso.'R'.$recurso.'E'.$elemento.'_v0';
    	$this->load->library('upload',$config);

    	if (!$this->upload->do_upload()) {
    		$datos['error'] = $this->upload->display_errors();
    		$this->load->view('admin/upload',$datos);
			$this->load->view('footer_style');
    	}else{
    		// $datos['success'] = $this->upload->data();

    		$datos = array('file' => $this->upload->data());
    		$file = $this->upload->data();
    		$file_name = $file['file_name'];
    		$result = $this->M_admin->upload_file($file_name,$elemento,$recurso,$proceso);
    		if ($result == true) {
    			redirect(base_url().'Admin/lista_documentos','refresh');
    		}else{
    			redirect(base_url().'Admin/upload_file','refresh');
    		}
  
    	}
    }
    public function do_upload1(){
    	$elemento = $this->input->post('elemento');
    	$recurso = $this->input->post('recurso');
    	$proceso = $this->input->post('proceso');
    	$archivo = $this->input->post('userfile_name');

    	$config['upload_path'] = './files/';
    	$config['allowed_types'] = 'pdf';
    	$config['file_name'] = $archivo.'_P'.$proceso.'R'.$recurso.'E'.$elemento.'_v0';
    	$this->load->library('upload',$config);

    	if (!$this->upload->do_upload()) {
    		$datos['error'] = $this->upload->display_errors();
    		$this->load->view('admin/upload',$datos);
			$this->load->view('footer_style');
    	}else{
    		// $datos['success'] = $this->upload->data();

    		$datos = array('file' => $this->upload->data());
    		$file = $this->upload->data();
    		$file_name = $file['file_name'];
    		$result = $this->M_admin->upload_file($file_name,$elemento,$recurso,$proceso);
    		if ($result == true) {
    			redirect(base_url().'Admin/documents_has_element/'.$proceso.'/'.$recurso.'/'.$elemento.'','refresh');
    		}else{
    			redirect(base_url().'Admin/documents_has_element/'.$proceso.'/'.$recurso.'/'.$elemento.'','refresh');
    		}
  
    	}
    }
    public function lista_documentos(){
		$data['elementos'] = $this->M_admin->sel_elementos_usuarios();
		$this->load->view('admin/lista_documentos',$data);
		$this->load->view('footer_style');
	}

	public function documents_has_element(){
		$proceso = $this->uri->segment(3);
		$recurso = $this->uri->segment(4);
		$elemento = $this->uri->segment(5);
		$obtenerElemento = $this->M_admin->obtenerElemento($elemento);
		if ($obtenerElemento != false) {
			foreach ($obtenerElemento->result() as $row) {
				$elemento_nombre = $row->elemento_nombre;
			}

			$data = array('id'=>$elemento,
						  'elemento_nombre'=>$elemento_nombre
						  // 'status'=>$status
			);

			$data['documentos'] = $this->M_admin->documentos_has_elemento1($proceso,$recurso,$elemento);
			$data['elementos'] = $this->M_admin->sel_elementos();	
		}else{
			$data = '';
			return false;
		}
		$this->load->view('admin/documents_has_element',$data);
		$this->load->view('footer_style');

	}
	// ==========================================================================================

	public function asignar_grupo(){
		$data['elementos'] = $this->M_admin->sel_elementos();
		// $data['grupos'] = $this->M_admin->sel_grupo1(2);
		$this->load->view('admin/asignar_grupo_elemento',$data);
		$this->load->view('footer_style');
	}

	public function asignar_recurso(){
		$data['elementos'] = $this->M_admin->sel_elementos();
		$this->load->view('admin/asignar_recurso_elemento',$data);
		$this->load->view('footer_style');
	}

	public function Assign_resource_element(){
		$res = $this->M_admin->Assign_resource_element();
		if ($res != false) {
			redirect(base_url().'Admin/asignar_recurso','refresh');
		}else{
			redirect(base_url().'Admin/lista_elementos','refresh');
		}
	}

	public function Assign_element_group(){
		$res = $this->M_admin->Assign_element_group();
		if ($res != false) {
			redirect(base_url().'Admin/asignar_grupo','refresh');
		}else{
			redirect(base_url().'Admin/lista_elementos','refresh');
		}
	}

	public function listar_grupos_x_asignar(){
		$proceso = $this->input->post('componente');
		$tipo = $this->input->post('tipo');
		//$validar_elemento = $this->M_admin->validar_elemento($proceso,$tipo);
		//if ($validar_elemento == true) {
			$result = $this->M_admin->sel_grupo1($proceso,$tipo);
			if ($result != false) {
				foreach ($result->result() as $row) {
					echo "<option value='".$row->id_grupo_usuario."'>".$row->grupo_usuario_nombre."</option>";
				}
			}else{
				echo "<option seleted disabled>Este grupo ya tiene todos los grupos asignados.</option>";
			}
		//}else{
			//echo "<option seleted disabled>El elemento seleccionado no esta asignado a un recurso.</option>";
		//}
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */


?>
