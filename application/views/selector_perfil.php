<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>File Admin Magnetron S.A.S</title>
      <link rel="stylesheet" href="<?= base_url()?>styles/style_login.css">
      <link href="<?= base_url()?>styles/template/css/bootstrap.min.css" rel="stylesheet">  
</head>

<body>
  <hgroup>
  <h1>Gestor Documental Magnetron S.A.S</h1>
  <h2>Selector de Perfil</h2>
</hgroup>

<div class="container">
	<?php

	$num_form = 0; 
	foreach ($login as $log): 
	?>

	<form name="form_perfiles<?= $num_form;?>" action="<?= base_url(); ?>Main/multi_user/<?= $num_form;?>" method="POST">
	
		<?php 
			if ($log['id_perfil']==1) {
				$pag_perfil = "Sadmin";
			}elseif ($log['id_perfil']==2) {
				$pag_perfil = "Admin";
			}elseif ($log['id_perfil']==3) {
				$pag_perfil = "User";
			}

		?>
		<div class="list-group">
		  <a href="#" type="button" class="list-group-item" id="<?= $num_form; ?>" onclick="javascript:document.form_perfiles<?= $num_form;?>.submit();"><?= $log['nombre_perfil']; ?></a>
		</div>

		<input type="hidden" name="perfil_nombre" value="<?= $pag_perfil; ?>">
		<input type="hidden" name="perfil_id" value="<?= $log['id_perfil']; ?>">
		<input type="hidden" name="usuario" value="<?= $log['usuario']; ?>">
		<input type="hidden" name="clave" value="<?= $log['clave']; ?>">
	
	</form>

	<?php 
		$num_form++;
		endforeach;
	?>	

</div>

<footer>
  <p>También puede ingresar a nuestro <a href="http://portal.magnetron.com/" target="_blank">Portal</a></p>
</footer>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="<?= base_url()?>styles/template/js/jquery.js"></script>

</body>
</html>
