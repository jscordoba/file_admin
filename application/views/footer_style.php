<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
			</div>
		</div>
	</div>
    <!-- jQuery -->
    <script src="<?= base_url()?>styles/template/js/jquery3.js"></script>
    <script src="<?= base_url()?>styles/template/js/fileinput.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url()?>styles/template/js/bootstrap.min.js"></script>
    <!-- Custome CSS -->
    <script src="<?= base_url()?>styles/template/js/custom_style.js"></script>
    <!-- DataTable Js -->
    <script src="<?= base_url()?>styles/template/js/jquery.dataTables.js"></script>
    <script src="<?= base_url()?>styles/template/js/dataTables.bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!-- <script src="<?= base_url()?>styles/template/js/plugins/morris/raphael.min.js"></script> -->
    <!-- <script src="<?= base_url()?>styles/template/js/plugins/morris/morris.min.js"></script> -->
    <!-- <script src="<?= base_url()?>styles/template/js/plugins/morris/morris-data.js"></script> -->
    <script>
    $("#userfile").fileinput({'showUpload':false});
        $(document).ready(function(){
            $('#table').DataTable({
                "responsive": true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                    // "url": "../styles/Spanish.json"
                    // "url": "../../styles/Spanish.json"
                }
            });
        });

    $(".back").click(function(event) {
       javascript:history.go(-1);
    });
        
    </script>
    </body>
</html>