<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// print_r($this->session->userdata());
?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>File Admin Magnetron S.A.S</title>
  
  <link rel="stylesheet" href="<?= base_url()?>styles/style_login.css">

</head>

<body>

  <hgroup>
    <h1>Gestor Documental Magnetron S.A.S</h1>
    <h3>By TIC's 2016</h3>
  </hgroup>

  <form action="<?= base_url();?>Main/login" method="POST">
    <div class="group">
      <input type="text" name="usuario"><span class="highlight"></span><span class="bar"></span>
      <label>Usuario</label>
    </div>

    <div class="group">
      <input type="password" name="clave"><span class="highlight"></span><span class="bar"></span>
      <label>Clave</label>
    </div>

    <button type="submit" class="button buttonBlue">Ingresar
      <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
    </button>
    
  </form>

  <footer>
    <p>También puede ingresar a nuestro <a href="http://www.portal.magnetron.com/" target="_blank">Portal</a></p>
  </footer>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="<?= base_url()?>styles/template/js/jquery.js"></script>

</body>
</html>
