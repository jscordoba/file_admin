<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if (isset($recursos) && $recursos != false): ?>
    <div class="page-header">
        <h2><i class="fa fa-arrow-circle-o-left back" aria-hidden="true" title="Regresar a la pagina anterior."></i> Recursos </h2>
    </div>

    <table id="table" class="table table-striped table-bordered" >
        <thead class="bg-black">
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">Proceso</th>
                <th class="text-center">Recurso</th>
            </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
         <?php foreach ($recursos->result() as $row) {?>
           <tr>
                <td class="text-center">
                       <?= $i++ ?>
                </td>
                <td class="text-center">
                       <?= $row->proceso_nombre ?>
                </td>
                <td class="text-center">
                    <a href="<?= base_url('User/listar_elementos_recurso/'.$row->id_recurso); ?>">
                       <?= $row->recurso_nombre ?>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php endif ?>




