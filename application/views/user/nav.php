<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url() ?>User">File Admin - Magnetron S.A.S</a>
        </div>

        <!-- Top Menu Items DATOS DE USUARIO -->
        <ul class="nav navbar-right top-nav">
          

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $this->session->userdata('usuario');?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?= base_url();?>Main/logout"><i class="fa fa-fw fa-power-off"></i> Cerrar Sesión</a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens  MENU LATERAL-->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="<?= base_url(); ?>User/index"><i class="fa fa-fw fa-user"></i> Perfil Usuario</a>
                </li>
                <?php
                    if (isset($grupos) || $grupos != false) {
                      /*foreach ($grupos->result() as $grupo) {
                        echo "<li>
                                <a href='".base_url()."User/accesos/".$grupo->id_grupo_usuario."'><i class='fa fa-fw fa-cog'></i>".$grupo->grupo_usuario_nombre."</a>
                            </li>";
                        }*/
                    }else{
                        
                    }
                ?>
                <!-- <li>
                    <a href="<?= base_url(); ?>User/lista"><i class="fa fa-fw fa-cog"></i> Procesos </a>
                </li> -->
             <!--    <li>
                    <a href="<?= base_url(); ?>User/lista_recurso_user"><i class="fa fa-fw fa-list"></i> Recursos </a>
                </li>
                <li>
                    <a href="<?= base_url(); ?>User/lista_elemento"><i class="fa fa-fw fa-ticket"></i> Elementos </a>
                </li> -->
                <!-- <li>
                    <a href="index-rtl.html"><i class="fa fa-fw fa-arrows-h"></i>Derecha</a>
                </li> -->

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>