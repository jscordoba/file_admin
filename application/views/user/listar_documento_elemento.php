<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
    <?php 
        if ($documentos) {
            foreach ($documentos->result() as $documento){ 
            echo '<h2 class="title"><i class="fa fa-arrow-circle-o-left back" aria-hidden="true" title="Regresar a la pagina anterior."></i> Lista de documentos de <b>'.$documento->elemento_nombre.'</b></h2>';
            break;
            }
        }else{
                echo '<h2><i class="fa fa-ticket"></i> Lista de documentos Vacia</b></h2>';
            }?>
</div>

<table id="table" class="table table-striped table-bordered ">
     <thead class="bg-black">
        <tr>
            <th class="text-center">Codigo</th>
            <th class="text-center">Documento</th>
        </tr>
    </thead>
  
    <tbody>
        <?php 
        if ($documentos) {
            foreach ($documentos->result() as $documento){?>
                 <tr>
                    <td class="text-center"><?= $documento->id_document?></td>
                    <td class="text-center">
                        <a href="<?= base_url('files/'.$documento->nombre.'')?>" target="_blank"><?= $documento->nombre ?>
                            
                        </a>
                    </td>
                </tr>
            <?php
            }
        }else{
        ?>
            <tr>
                <td colspan="2" class="text-center"> Este elemento no contiene documentos</td>
            </tr>
        <?php  }?>
    </tbody>
</table>

