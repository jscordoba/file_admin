<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<legend>Lista de Versiones</legend>

<table id="" class="table table-striped table-bordered display" cellspacing="0" width="40%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Fecha Actualización</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Name</th>
            <th>Fecha Actualización</th>
        </tr>
    </tfoot>
    <tbody>
        <?php 
        if ($versiones) {
            # code...
            foreach ($versiones->result() as $version): ?>
                 <tr>
                    <td><a href="<?= base_url() ?>Main/ver_version/<?= $version->document_id_document ?>" target="_blanck"><?= $version->version_nombre ?></a></td>
                    </td>
                    <td></td>
                </tr>
            <?php endforeach;
        }
        ?>
    </tbody>
</table>