<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
    <?php 
        if ($recursos) {
            foreach ($recursos->result() as $row){ 
              echo '<h2 class="title"><i class="fa fa-arrow-circle-o-left back" aria-hidden="true" title="Regresar a la pagina anterior."></i> Lista de recurso de <b>'.$row->proceso_nombre.'</b></h2>';
              break;
            }
        } ?>
</div>

<table id="table" class="table table-striped table-bordered ">
    <thead class="bg-black">
        <tr>
            <th class="text-center">Codigo</th>
            <th class="text-center">Nombre</th>
            <!-- <th class="text-center">Proceso</th> -->
        </tr>
    </thead>

    <tbody>
        <?php 
        if ($recursos) {
            foreach ($recursos->result() as $row){
                    echo '<tr>
                            <td class="text-center">
                                   '.$row->id_recurso.'
                            </td>
                            <td class="text-center">
                                <a href="'.base_url().'User/listar_elementos_recurso/'.$row->id_recurso.'">
                                   '.$row->recurso_nombre.'
                                </a>
                            </td>
                            
                  
                    </tr>';
            }
        }
        ?>
    </tbody>
</table>
<!-- 
 <td class="text-center">
                                   '.$row->proceso_nombre.'
                            </td> -->