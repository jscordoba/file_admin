<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Form Name -->
<div class="page-header">
  <h2 class=""><i class="fa fa-cogs"></i> Accesos del <?php echo $this->session->userdata('usuario_nombre'); ?></h2p>
</div>
<?php if (isset($procesos) && $procesos != false) {?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Procesos</h3>
			</div>
			<div class="panel-body">
			 <table id="" class="table table-striped table-bordered ">
			    <thead class="bg-black">
			        <tr>
			            <th class="text-center col-md-1">#</th>
			            <th class="text-center col-md-11">Nombre</th>
			            <!-- <th class="text-center">Administrador del Proceso</th> -->
			        </tr>
			    </thead>
			    <tbody>
			    <?php
			        $i = 1;
			        foreach ($procesos->result() as $proceso){ 
			            if($proceso->status == "Activo"){?>
			             <tr>
			                <td class="text-center"><?= $i++ ?></td>
			                <td class="text-center"><a href="<?= base_url('User/user_recursos/'.$proceso->id_proceso.'');?>"><?= $proceso->proceso_nombre ?></a></td>
			            </tr>
			        <?php 
			            }else{?>
			            	<td class="text-center" colspan="3">El proceso asignado no se encuentra activo.</td>
			            <?php
			            }	     
			        }?>
			 
			    </tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<?php } ?>

<?php if (isset($recursos) && $recursos != false) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Recursos</h3>
			</div>
			<div class="panel-body">
				<table id="" class="table table-striped table-bordered" >
				    <thead class="bg-black">
				        <tr>
				            <th class="text-center col-md-1">#</th>
				            <th class="text-center">Proceso</th>
				            <th class="text-center">Recurso</th>
				        </tr>
				    </thead>
				    <tbody>
				        <?php
				            $i = 1;
				            foreach ($recursos->result() as $row){
				                if ($row->status == "Activo") {
				                     echo '<tr>
				                            <td class="text-center">
				                                   '.$i++.'
				                            </td>
				                          	<td class="text-center">
				                                   '.$row->proceso_nombre.'
				                            </td>
				                            <td class="text-center">
				                                <a href="'.base_url().'User/listar_elementos_recurso/'.$row->id_recurso.'">
				                                   '.$row->recurso_nombre.'
				                                </a>
				                            </td>
				                  
				                    </tr>';
				                }else{
				                     echo '<tr>
				                            <td class="text-center" colspan="3">
				                                El recurso asignado no se encuentra activo.
				                            </td>
				                    </tr>';
				                }
				        	}
				        ?>
				    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php } ?>


<?php if (isset($elementos) && $elementos != false) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Elementos</h3>
			</div>
			<div class="panel-body">
				<table id="" class="table table-striped table-bordered">
				    <thead class="bg-black">
				        <tr>
				            <th class="text-center col-md-1">#</th>
				            <th class="text-center col-md-11">Nombre Elemento</th>
				        </tr>
				    </thead>
				    <tbody>
				        <?php
				            $i = 1;
				            foreach ($elementos->result() as $elemento){ 
				                if ($elemento->status == "Activo"){?>
				                 <tr>
				                    <td class="text-center"><?= $i++?></td>
				                    <td class="text-center">
				                        <a href="<?php echo base_url('User/elemento_procesos/'.$elemento->id_elemento.''); ?>"><?= $elemento->elemento_nombre ?>
				                            
				                        </a>
				                    </td>
				                </tr>
				            	<?php }else{ ?>
				                    <td class="text-center" colspan="4">El elemento asignado no se encuentra activo</td>
				                <?php
				            	}
				            }
				        	?>
				    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php } ?>

