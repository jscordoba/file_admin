<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->

<script>
    function mostar_grupos_proceso(id) {
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/mostar_grupos_proceso', {id: id }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody_grupos").text("");
                $("#tbody_grupos").append(data);
                // $("#modal-grupos_x_proceso").modal("show");
            }
        });
    }

    function eliminar_grupo_proceso(grupo,proceso){
        // alert(id + " - "+proceso);
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/eliminar_grupo_proceso', {grupo: grupo, proceso: proceso }, function(data, textStatus, xhr) {
            if (data) {
                // $("#tbody_grupos").text("");
                // $("#tbody_grupos").append(data);
                $("#modal-grupos_x_proceso").modal("hide");
                setTimeout(function() {location.reload();}, 500);
            }else{
                alert("Error");
            }
        });
    }
</script>



<?php

/* EXPLORADOR DE CARPETAS Y ARCHIVOS CON PHP
 * Documento explora.php
 * Explorador de carpetas y archivos usando el sistema de ficheros de PHP.
 * Autor: Andrés de la Paz © 2010
 * Contacto: www.wextensible.com
 */

//Por defecto la primera vez que abrimos este explorador toma
//como carpeta la del php actual. La constante __FILE__ es la
//ruta del archivo PHP actual. Con dirname obtenemos la carpeta.
$ruta = dirname(__FILE__)."/";

//Para abrir archivos necesitaremos poner la codificación
//adecuada con estos valores
$array_codif = Array(
"UTF-8",
"ISO-8859-1",
"ISO-8859-15"
);

//Por defecto usamos esta para htmlentities (ver más abajo)
$codificacion = "ISO-8859-1";

//Vemos si hay algo en el GET
if (isset($_GET)){
    foreach($_GET as $campo=>$valor){
        switch ($campo) {
            //Obtenemos una ruta, carpeta o archivo
            case "una-ruta":
                $ruta = htmlspecialchars($valor, ENT_QUOTES); 
                if (get_magic_quotes_gpc() == 1) $ruta = stripslashes($ruta);
                break;
            //Vemos la codificación
            case "una-codificacion":
                $codificacion = htmlspecialchars($valor, ENT_QUOTES);
                if (get_magic_quotes_gpc() == 1) $codificacion = stripslashes($codificacion);
                break;

        }
    }
}

//Si la ruta es vacía, pone la del presente script
if ($ruta == "") $ruta = dirname(__FILE__)."/";

//Esta variable contendrá la lista de nodos (carpetas y archivos)
$presenta_nodos = "";

//Esta variable es para el contenido del archivo
$presenta_archivo = "";

//Si la ruta es una carpeta, la exploramos. Si es un archivo
//sacamos también el contenido del archivo.
if (is_dir($ruta)){//ES UNA CARPETA
    //Con realpath convertimos los /../ y /./ en la ruta real
    $ruta = realpath($ruta)."/";
    //exploramos los nodos de la carpeta
    $presenta_nodos = explora_ruta($ruta);
} else {//ES UN ARCHIVO
    $ruta = realpath($ruta);
    //Sacamos también los nodos de la carpeta
    $presenta_nodos = explora_ruta(dirname($ruta)."/");
    //Y sacamos el contenido del archivo
    
    $rutaUrl = str_replace(" ", "%20", $ruta);
    echo $rutaUrl."<a target='_blank' href='$rutaUrl'>Prueba</a>";

    $presenta_archivo = "<br />CONTENIDO DEL ARCHIVO: <a href='".$rutaUrl."'>".
    $ruta."</a><pre>".
    //explora_archivo($ruta, $codificacion).
    "</pre>";
}
//Función para explorar los nodos de una carpeta
//El signo @ hace que no se muestren los errores de restricción cuando
//por ejemplo open_basedir restringue el acceso a algún sitio
function explora_ruta($ruta){
    //En esta cadena haremos una lista de nodos
    $cadena = "";
    //Para agregar una barra al final si es una carpeta
    $barra = "";
    //Este es el manejador del explorador
    $manejador = @dir($ruta);
    while ($recurso = $manejador->read()){
        //El recurso sera un archivo o una carpeta
        $nombre = "$ruta$recurso";
        if (@is_dir($nombre)) {//ES UNA CARPETA
            //Agregamos la barra al final
            $barra = "/";
            $cadena .= "CARPETA: ";
        } else {//ES UN ARCHIVO
            //No agregamos barra
            $barra = "";
            $cadena .= "ARCHIVO: ";
        }
        //Vemos si el recurso existe y se puede leer
        if (@is_readable($nombre)){
            if (@is_dir($nombre)) {
                 $cadena .= "<a href=\"".$_SERVER["PHP_SELF"]."?una-ruta=$nombre$barra\">$recurso$barra</a>";
            }else{
                $nombreUrl = str_replace(" ", "%20", $nombre);
                $cadena .= "<a href='file:///$nombreUrl' target='_blank'>$recurso$barra</a>";
            }

        } else {
            $cadena .= "$recurso$barra";
        }
        $cadena .= "<br />";
    }
    $manejador->close();
    return $cadena;
}

//Función para extraer el contenido de un archivo
function explora_archivo($ruta, $codif){
    //abrimos un buffer para leer el archivo
    ob_start();
    readfile($ruta);
    //volcamos el buffer en una variable
    $contenido = ob_get_contents();
    //limpiamos el buffer
    ob_clean();
    //retornamos el contenido después de limpiarlo
    //aplicando la codificación seleccionada
    return htmlentities($contenido, ENT_QUOTES, $codif);
}

?>



    <!--<h3>Opciones de configuración PHP (restringen explorador)</h3>-->
    <ul>
    <?php
    /*
        $opciones = "<li><a href=\"http://docs.php.net/manual/es/ini.sect.safe-mode.php#ini.safe-mode\">".
        "<code>safe_mode</code></a> ";
        if (ini_get("safe_mode")){
            $opciones .= ": activado";
        } else {
            $opciones .= ": desactivado";
        }
        $opciones .= "</li>".
        "<li><a href=\"http://docs.php.net/manual/es/ini.core.php#ini.open-basedir\">".
        "<code>open_basedir</code></a>: ".ini_get("open_basedir")."</li>".
        "<li><a href=\"http://docs.php.net/manual/es/function.getmyuid.php\">".
        "<code>getmyuid()</code></a>: ".getmyuid()."</li>".
        "<li><a href=\"http://docs.php.net/manual/es/function.getmygid.php\">".
        "<code>getmygid()</code></a>: ".getmygid()."</li>";
        echo $opciones;
        */
    ?>
    </ul>
    
    <div class="col-md-6">
        <h3>Directorio</h3>
        <form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="get">
            Ruta <small>(En Windows pueden usarse ambas barras "/" y "\")</small>
            <br /><textarea rows="5" cols="50" name="una-ruta"
            ><?php echo $ruta; ?></textarea><br />
            Codificación para ver archivos:
            <select name="una-codificacion">
                <?php
                    foreach ($array_codif as $i=>$val){
                        echo "<option value=\"$val\"";
                        if ($codificacion == $val) echo " selected=\"selected\"";
                        echo ">$val</option>";
                    }
                ?>
            </select><br />
            <input type="submit" value="Recuperar..." />
        </form>
    </div>

    <div class="col-md-6">
        <h3>Explorador</h3>
        <?php
            echo "<br />$presenta_nodos";
            echo "<br />$presenta_archivo";
        ?>

    </div>

