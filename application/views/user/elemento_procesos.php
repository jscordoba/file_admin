<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-header">
  <h2 class=""><i class="fa fa-arrow-circle-o-left back" aria-hidden="true" title="Regresar a la pagina anterior."></i> Procesos</h2p>
</div>
<?php if (isset($procesos) && $procesos != false): ?>
<div class="row">
	<div class="col-md-12">
		<table id="" class="table table-striped table-bordered ">
		    <thead class="bg-black">
		        <tr>
		            <th class="text-center col-md-1">#</th>
		            <th class="text-center col-md-11">Nombre</th>
		            <!-- <th class="text-center">Administrador del Proceso</th> -->
		        </tr>
		    </thead>
		    <tbody>
		    <?php
		        $i = 1;
		        foreach ($procesos->result() as $proceso){ 
		            if($proceso->status == "Activo"){?>
		             <tr>
		                <td class="text-center"><?= $i++ ?></td>
		                <td class="text-center"><a href="<?= base_url('User/elemento_procesos_recursos/'.$this->uri->segment(3).'/'.$proceso->id_proceso.'');?>"><?= $proceso->proceso_nombre ?></a></td>
		            </tr>
		        <?php 
		            }else{?>
		            	<td class="text-center" colspan="3">El proceso asignado no se encuentra activo.</td>
		            <?php
		            }	     
		        }?>
		 
		    </tbody>
		</table>
	</div>
</div>
<?php endif ?>