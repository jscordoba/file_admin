<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
    <?php 
        if ($elementos) {
            foreach ($elementos->result() as $elemento){ 
            echo '<h2 class=""><i class="fa fa-arrow-circle-o-left back" aria-hidden="true" title="Regresar a la pagina anterior."></i> Elementos</h2>';
            break;
            }
        }else{
             echo '<h2><i class="fa fa-ticket"></i> Lista de elementos vacia</h2>';
        }

            ?>
</div>

<table id="table" class="table table-striped table-bordered ">
     <thead class="bg-black">
        <tr>
            <th class="text-center">Codigo</th>
             <th class="text-center">Proceso</th>
        </tr>
    </thead>

    <tbody>
        <?php 
        if ($elementos) {
            foreach ($elementos->result() as $elemento){ 
                if ($elemento->status == "Activo"){?>
                 <tr>
                    <td class="text-center"><?= $elemento->id_elemento?></td>
                    <td class="text-center">
                        <a href="<?= base_url() ?>User/listar_documento_elemento/<?= $elemento->id_elemento ?>/<?= $this->uri->segment(5);?>"><?= $elemento->elemento_nombre ?>
                        </a>
                    </td>
                    <!-- <td><?= $elemento->recurso_nombre ?></td>
                    <td>
                        <a href="<?= base_url() ?>User/lista_recurso_user/<?= $elemento->id_proceso ?>"><?= $elemento->proceso_nombre ?>
                        </a>
                    </td> -->
                </tr>
            <?php
                }
            }
        }
        ?>
    </tbody>
</table>
