<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
  <h1><i class="fa fa-ticket"></i> Lista de Elementos</h1>
</div>

<table id="table" class="table table-striped table-bordered">
    <thead class="bg-black">
        <tr>
            <th class="text-center">Codigo</th>
            <th class="text-center">Nombre Proceso</th>
            <th class="text-center">Nombre Recurso</th>
            <th class="text-center">Nombre Elemento</th>
        </tr>
    </thead>

    <tbody>
        <?php 
        if ($elementos) {
            $i = 1;
            foreach ($elementos->result() as $elemento){ 
                if ($elemento->status == "Activo"){?>
                 <tr>
                    <td class="text-center"><?= $i++?></td>
                    <td class="text-center"><?= $elemento->proceso_nombre ?></td>
                    <td class="text-center"><?= $elemento->recurso_nombre ?></td>
                    <td class="text-center">
                        <a href="<?= base_url() ?>User/listar_documento_elemento/<?= $elemento->id_elemento ?>/<?= $elemento->recurso_id_recurso ?>"><?= $elemento->elemento_nombre ?>
                            
                        </a>
                    </td>
                </tr>
            <?php
                }else{?>
                    <td class="text-center" colspan="4">El elemento asignado no se encuentra activo</td>
                <?php
            }
            }
        }
        ?>
    </tbody>
</table>