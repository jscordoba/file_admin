<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="icon" type="image/ico" href="<?= base_url()?>styles/template/images/logo1.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>File Admin - Magnetron S.A.S</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url()?>styles/template/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url()?>styles/template/css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?= base_url()?>styles/template/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- DataTable Css -->
    <link href="<?= base_url()?>styles/template/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
    <script src="<?= base_url()?>styles/template/js/jquery3.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Lobster|Oswald:300');
        body{font-family: 'Oswald', sans-serif;}.navbar-brand{font-family: 'Lobster', cursive;}
        ::-webkit-scrollbar-track{-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);background-color: #F5F5F5;}
        ::-webkit-scrollbar{width: 3px;height: 3px;background-color: #F5F5F5;}
        ::-webkit-scrollbar-thumb{background-color: #039be5;border: 2px solid #039be5;}
        .bg-black{
            background-color: #4e4e4e;
            color: #fff;
        }
        .back{
            width: 32px;
            height: 32px;
            color:#4e4e4e;
        }
        .back:hover{
            cursor: pointer;
        }
        .back1{
            width: 32px;
            height: 32px;
            color:#4e4e4e;
        }
        .back1:hover{
            cursor: pointer;
        }

        .title{
            text-transform: lowercase;
        }
    </style>
    <!-- jQuery -->
    <!-- <script src="<?= base_url()?>styles/template/js/jquery3.js"></script> -->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">

            


