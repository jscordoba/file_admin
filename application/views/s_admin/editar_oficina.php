<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>Sadmin/editar_oficina/<?=$id?>">
	<fieldset>

	<!-- Form Name -->
	<div class="page-header">
      <h1><i class="fa fa-building"></i> Editar Oficina</h1>
  	</div>

	<div class="form-group hidden">
      <label class="col-md-4 control-label" for="email">Status</label>  
      <div class="col-md-4">
      <input id="status" name="status" type="text" placeholder="email@magnetron.com.co" class="form-control input-md" required value="<?=$status?>">
      <span class="help-block">Estado del registro</span>  
      </div>
    </div>

	<!-- Text input-->
	<div class="form-group">
	  	<div class="col-md-6">
	  		<label for="">Nombre de la Oficina</label>  
		  	<input id="" name="nom_oficina" type="text" placeholder="Pereira Planta1, Pereira Planta 2, Medellin, Barranquilla..." class="form-control input-md" required="" value="<?=$nombre?>">
	  	</div>

    	<div class="col-md-6">
    		<label for="">Seleccione el estado</label>
      		<select id="status" name="status" class="form-control" required>
        		<option value="">Seleccione una Opción</option>
               	<option value="Activo">Activo</option>
               	<option value="Inactivo">Inactivo</option>
      		</select>
    	</div>
  	</div>

	<!-- Button -->
	<div class="form-group">
	<br>
	  <div class="col-md-12">
	    <button id="btn_guardar_oficina" name="btn_guardar_oficina" class="btn btn-block btn-primary">Guardar</button>
	  </div>
	</div>


	</fieldset>
</form>

<script>
  jQuery(document).ready(function($) {
    var status = $("#status").val();
    $('#status > option[value="'+status+'"]').attr('selected', 'selected');
  });
</script>