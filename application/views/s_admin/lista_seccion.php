<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
  <h3><i class="fa fa-th-list"></i> Secciones</h3>
</div>

<table id="table" class="table table-striped table-bordered">
    <thead class="bg-black">
        <tr>
            <th class="text-center">Codigo</th>
            <th class="text-center">Nombre</th>
            <th class="text-center">Fecha Update</th>
            <th class="text-center">Status</th>
            <th class="text-center col-md-1"><i class="fa fa-cogs"></i></th>
            <th class="text-center col-md-1"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody>
        
    <?php 
    if ($secciones) {
        # code...
        foreach ($secciones->result() as $seccion): ?>
             <tr>
                <td class="text-center"><?= $seccion->id_seccion_company ?></td>
                <td class="text-center"><?= $seccion->seccion_company_nombre ?></td>
                <td class="text-center"><?= $seccion->seccion_company_update ?></td>
                <td class="text-center"><?= $seccion->status ?></td>
                <td class="text-center">
                <a class="btn btn-xs btn-primary" id="btn_editar_usuario" href="<?= base_url(); ?>Sadmin/consultar_seccion/<?= $seccion->id_seccion_company ?>"><i class="fa fa-pencil"></i>
                </a>
                </td>
                <td>
                   <?php if ($seccion->status == "Activo") {
                        echo "<a  class='btn btn-xs btn-success' id='btn_editar_usuario' href='".base_url()."Sadmin/status_seccion/$seccion->id_seccion_company'><i class='fa fa-toggle-on'></i></a>";
                   }else{
                     echo "<a  class='btn btn-xs btn-default' id='btn_editar_usuario' href='".base_url()."Sadmin/status_seccion/$seccion->id_seccion_company'><i class='fa fa-toggle-off'></i></a>";
                    } ?>
                </td>
            </tr>
        <?php endforeach;
    }

    ?>

    </tbody>
</table>