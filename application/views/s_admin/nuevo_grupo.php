<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>Sadmin/nuevo_grupo/c">
<fieldset>

<!-- Form Name -->
<div class="page-header">
  <h1><i class="fa fa-users"></i> Nuevo Grupo de Usuarios</h1>
</div>

<!-- Text input-->
<div class="form-group">
	<div class="col-md-6">
		<label for="nom_grupo">Nombre del Grupo</label>  
		<input id="nom_grupo" name="nom_grupo" type="text" placeholder="Nombre del grupo a registrar" class="form-control input-md" required="">
	</div>

	<div class="col-md-6">
     	<label for="">Seleccione el estado</label>
      	<select id="status" name="status" class="form-control" required>
        	<option value="">Seleccione una Opción</option>
               <option value="Activo">Activo</option>
               <option value="Inactivo">Inactivo</option>
     	 </select>
	</div>
</div>

<!-- Button -->
<div class="form-group">
<br>
  <div class="col-md-12">
    <button id="btn_guardar_grupo" name="btn_guardar_grupo" class="btn btn-block btn-primary">Guardar</button>
  </div>
</div>

</fieldset>
</form>