<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>Sadmin/editar_usuario/<?=$id?>">
	<fieldset>

	<!-- Form Name -->
	<div class="page-header">
  		<h1><i class="fa fa-users"></i> Editar Usuario</h1>
	</div>

	<!-- Campos ocultos No borrar -->
		<div class="form-group hidden">
		  <label class="col-md-4 control-label" for="email">Seccion</label>  
		  <div class="col-md-4">
		  <input id="seccion" name="seccion" type="text" placeholder="seccion" class="form-control input-md" required value="<?=$seccion?>">
		  </div>
		</div>
		<div class="form-group hidden">
		  <label class="col-md-4 control-label" for="email">Oficina</label>  
		  <div class="col-md-4">
		  <input id="oficina" name="oficina" type="text" placeholder="Oficina" class="form-control input-md" required value="<?=$oficina?>">
		  </div>
		</div>
		<div class="form-group hidden">
		  <label class="col-md-4 control-label" for="email">Rol</label>  
		  <div class="col-md-4">
		  <input id="rol" name="rol" type="text" placeholder="Rol" class="form-control input-md" required value="<?=$perfil?>">
		  </div>
		</div>
		<div class="form-group hidden">
		  <label class="col-md-4 control-label" for="email">Status</label>  
		  <div class="col-md-4">
		  <input id="status" name="status" type="text" placeholder="email@magnetron.com.co" class="form-control input-md" required value="<?=$status?>">
		  <span class="help-block">Estado del registro</span>  
		  </div>
		</div>
	<!-- Fin campos ocultos -->

	<!-- Text input-->
	<div class="form-group">
	  	<div class="col-md-4">
		  <label for="nombre_user">Nombre</label>  
		  <input id="nombre_user" name="nombre_user" type="text" placeholder="Nombre de Usuario/Sección" class="form-control input-md" required value="<?=$nombre?>">
	  	</div>

	  	<div class="col-md-4">
		  <label for="user">Usuario</label>  
		  <input id="user" name="user" type="text" placeholder="User" class="form-control input-md" required value="<?=$user?>">
	  	</div>

	  	<div class="col-md-4">
		  	<label for="password">Clave</label>
		    <input id="password" name="password" type="password" placeholder="password" class="form-control input-md" required value="<?=$clave?>">
	  	</div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	  	<div class="col-md-4">
		  <label for="email">Correo electrónico corporativo</label>  
		  <input id="email" name="email" type="email" placeholder="email@magnetron.com.co" class="form-control input-md" required value="<?=$email?>">
	  	</div>

	  	<div class="col-md-4">
		  	<label for="id_seccion">Seleccione el estado</label>
		    <select id="status_user" name="status_user" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		             <option value="Activo">Activo</option>
		             <option value="Inactivo">Inactivo</option>
		    </select>
	  	</div>

	  	<div class="col-md-4">
		  	<label for="id_seccion">Seleccione la sección</label>
		    <select id="id_seccion" name="id_seccion" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		       <?php foreach ($secciones->result() as $seccion): ?>
		             <option value="<?= $seccion->id_seccion_company ?>"><?= $seccion->seccion_company_nombre ?></option>
		        <?php endforeach; ?>
		    </select>
	 	 </div>
	</div>

	<!-- Select Basic -->
	<div class="form-group">
	  	<div class="col-md-6">
		  	<label for="id_oficina">Seleccione la oficina</label>
		    <select id="id_oficina" name="id_oficina" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		       <?php foreach ($oficinas->result() as $oficina): ?>
		             <option value="<?= $oficina->id_oficina_company ?>"><?= $oficina->oficina_company_nombre ?></option>
		        <?php endforeach; ?>
		    </select>
	  	</div>
	
	  	<div class="col-md-6">
		  	<label for="id_perfil">Seleccione el Rol</label>
		    <select id="id_perfil" name="id_perfil" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		       <?php foreach ($perfiles->result() as $perfil): ?>
		             <option value="<?= $perfil->id_perfil_usuario ?>"><?= $perfil->perfil_usuario_nombre ?></option>
		        <?php endforeach; ?>
		    </select>
	  	</div>
	</div>

	<!-- Button -->
	<div class="form-group">
	  <div class="col-md-12">
	    <button id="btn_editar_user" name="btn_editar_user" class="btn btn-block btn-primary">Guardar</button>
	  </div>
	</div>

	</fieldset>
</form>

<script>
jQuery(document).ready(function($) {
	var seccion = $("#seccion").val();
	var oficina = $("#oficina").val();
	var rol = $("#rol").val();
	var status = $("#status").val();

	$('#id_seccion > option[value="'+seccion+'"]').attr('selected', 'selected');
	$('#id_oficina > option[value="'+oficina+'"]').attr('selected', 'selected');
	$('#id_perfil > option[value="'+rol+'"]').attr('selected', 'selected');
	$('#status_user > option[value="'+status+'"]').attr('selected', 'selected');
});
</script>