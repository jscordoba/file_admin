<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
  <h3><i class="fa fa-users"></i> Grupos</h3>
</div>

<table id="table" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
    <thead class="bg-black">
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">Nombre del Grupo</th>
            <th class="text-center">Status</th>
            <th class="text-center col-md-1"><i class="fa fa-cogs"></i></th>
            <th class="text-center col-md-1"><i class="fa fa-cogs"></i></th>
            <th class="text-center col-md-1"><i class="fa fa-cogs"></i></th>
            <th class="text-center col-md-1"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody>
    <?php 
    if ($grupos) {
        # code...
        foreach ($grupos->result() as $grupo): ?>
             <tr>
                <td class="text-center"><?= $grupo->id_grupo_usuario ?></td>
                <td class="text-center"><?= $grupo->grupo_usuario_nombre ?></td>
                <td class="text-center"><?= $grupo->status ?></td>
                <td class="text-center">
                    <a class="btn btn-xs btn-primary" id="btn_editar_usuario" href="<?= base_url(); ?>Sadmin/consultar_grupo/<?= $grupo->id_grupo_usuario ?>" title="Editar"><i class="fa fa-pencil"></i>
                    </a>
                </td>
                <td class="text-center">
                   <?php if ($grupo->status == "Activo") {
                        echo "<a  class='btn btn-xs btn-success' id='btn_editar_usuario' href='".base_url()."Sadmin/status_grupo/$grupo->id_grupo_usuario' title='Cambiar status a Inactivo'><i class='fa fa-toggle-on'></i></a>";
                   }else{
                     echo "<a  class='btn btn-xs btn-default' id='btn_editar_usuario' href='".base_url()."Sadmin/status_grupo/$grupo->id_grupo_usuario' title='Cambiar status a activo'><i class='fa fa-toggle-off'></i></a>";
                    } ?>
                </td>
                <td class="text-center">
                     <?php if ($grupo->status == "Activo") {
                        echo "<button class='btn btn-primary btn-xs btn_asignarUserGroup' data-toggle='modal' data-target='#m_addUsuariosGroup' data-user_id='$grupo->id_grupo_usuario' data-grupo_id='$grupo->id_grupo_usuario' title='Asignar grupo'><i class='fa fa-check'></i></button>";
                   }else{
                    echo "<button class='btn btn-primary btn-xs btn_asignarUserGroup disabled' data-toggle='modal' data-target='#m_addUsuariosGroup' data-user_id='$grupo->id_grupo_usuario' data-grupo_id='$grupo->id_grupo_usuario' title='Asignar grupo'><i class='fa fa-check'></i></button>";
                    } ?>
                </td>
                <td class="text-center">
                      <?php if ($grupo->status == "Activo") {
                        echo "<button type='button' class='btn btn-primary btn-xs' onclick='user_group($grupo->id_grupo_usuario);' title='Mostrar usuarios'><i class='fa fa-users'></i></button>";
                   }else{
                    echo "<button class='btn btn-primary btn-xs disabled' title='Mostrar usuarios'><i class='fa fa-users'></i></button>";
                    } ?>
                </td>
            </tr>
        <?php endforeach;
    }
    ?>
    </tbody>
</table>

<!-- Modal AGREGAR USUARIOS AL GRUPO -->
<div class="modal fade" id="m_addUsuariosGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    
    <form action="<?= base_url();?>Sadmin/add_grupoUsuarios/" method="POST">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><b>Agregando Usuarios el Grupo</b></h4>
          </div>
          <div class="modal-body">
            <table id="g3" class="table table-striped table-bordered display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Usuario ID</th>
                        <th>Nombre</th>
                        <th>Sección</th>
                        <th>Oficina</th>
                        <th>Incluir</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Usuario ID</th>
                        <th>Nombre</th>
                        <th>Sección</th>
                        <th>Oficina</th>
                        <th>Incluir</th>
                    </tr>
                </tfoot>
                <tbody id="tbody_UsuariosGroup">
                
                
                </tbody>
            </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary" id="btn_guardarUsuariosGroup">Guardar Cambios</button>
          </div>
        </div>
    </form>

  </div>
</div>

<div class="modal fade" id="usuarios_grupo">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Listado de usuarios</h3>
            </div>
            <div class="modal-body">
                <table id="g2" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center">Documento</th>
                            <th class="text-center">Nombre</th>
                            <th class="text-center">Correo electronico</th>
                            <th class="text-center">sección</th>
                            <th class="text-center">Oficina</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="text-center">Documento</th>
                            <th class="text-center">Nombre</th>
                            <th class="text-center">Correo electronico</th>
                            <th class="text-center">sección</th>
                            <th class="text-center">Oficina</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </tfoot>
                    <tbody id="tbody">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- <script scr="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->

<script>
    function user_group(id){
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/lista_usuarioGroupUsuario', {id: id }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody").text("");
                $("#tbody").append(data);
                $("#usuarios_grupo").modal("show");
            }
        });
    }

    function remover_usuario_grupo(id){
        var base_url = "<?php echo base_url(); ?>";
        var grupo = $("#grupo").val();
        $.post(''+base_url+'Main/remover_usuario_grupo', {id: id, grupo: grupo }, function(data, textStatus, xhr) {
            if (data != 2) {
                $("#usuarios_grupo").modal("hide");
            }else{
                 alert("Error al eliminar el usuario de este grupo");
            }
        });
    }
</script>