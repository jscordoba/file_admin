<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>Sadmin/editar_proceso/<?=$id?>">
  <fieldset>

  <!-- Form Name -->
    <div class="page-header">
      <h1><i class="fa fa-cog"></i> Editar Proceso</h1>
    </div>

    <div class="form-group hidden">
      <div class="col-md-4">
        <label for="email">Seccion</label>  
        <input id="seccion" name="seccion" type="text" placeholder="seccion" class="form-control input-md" required value="<?=$seccion?>">
      </div>

      <div class="col-md-4">
        <label for="email">Oficina</label>  
        <input id="oficina" name="oficina" type="text" placeholder="Oficina" class="form-control input-md" required value="<?=$oficina?>">
      </div>
  
      <div class="col-md-4">
        <label for="email">responsable</label>  
        <input id="responsable" name="responsable" type="text" placeholder="Rol" class="form-control input-md" required value="<?=$responsable?>">
      </div>
    </div>
    <div class="form-group hidden">
      <label class="col-md-4 control-label" for="email">Status</label>  
      <div class="col-md-4">
      <input id="status" name="status" type="text" placeholder="email@magnetron.com.co" class="form-control input-md" required value="<?=$status?>">
      <span class="help-block">Estado del registro</span>  
      </div>
    </div>

  <!-- Text input-->
  <div class="form-group">
    <div class="col-md-4">
      <label for="nom_proceso">Nombre del Proceso</label>  
      <input id="nom_proceso" name="nom_proceso" type="text" placeholder="Proceso del proceso" class="form-control input-md" required="" value="<?=$nombre?>">
    </div>


    <div class="col-md-4">
      <label for="id_responsable">Seleccionar el responsable</label>
      <select id="id_responsable" name="id_responsable" class="form-control">
        <option value="">Seleccione una Opción</option>
        <?php foreach ($responsables->result() as $responsable): ?>
             <option value="<?= $responsable->usuario_id ?>"><?= $responsable->username." - ".$responsable->usuario_id ?></option>
        <?php endforeach; ?>
      </select>
    </div>

 
    <div class="col-md-4">
      <label for="id_seccion">Seleccione la sección</label>
      <select id="id_seccion" name="id_seccion" class="form-control">
        <option value="">Seleccione una Opción</option>
        <?php foreach ($secciones->result() as $seccion): ?>
             <option value="<?= $seccion->id_seccion_company ?>"><?= $seccion->seccion_company_nombre ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>

  <!-- Select Basic -->
  <div class="form-group">
    <div class="col-md-6">
      <label for="id_oficina">Seleccionar oficina</label>
      <select id="id_oficina" name="id_oficina" class="form-control">
        <option value="">Seleccione una Opción</option>
        <?php foreach ($oficinas->result() as $oficina): ?>
             <option value="<?= $oficina->id_oficina_company ?>"><?= $oficina->oficina_company_nombre ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="col-md-6">
      <label for="">Selecionar el estado</label>
      <select id="status" name="status" class="form-control" required>
        <option value="">Seleccione una Opción</option>
               <option value="Activo">Activo</option>
               <option value="Inactivo">Inactivo</option>
      </select>
    </div>
  </div>

  <!-- Button -->
  <div class="form-group">
  <br>
    <div class="col-md-12">
      <button id="btn_editar_proceso" name="btn_editar_proceso" class="btn btn-block btn-primary">Guardar</button>
    </div>
  </div>

  </fieldset>
</form>

<script>
  jQuery(document).ready(function($) {
    var seccion = $("#seccion").val();
    var oficina = $("#oficina").val();
    var responsable = $("#responsable").val();
    var status = $("#status").val();
    
    $('#id_seccion > option[value="'+seccion+'"]').attr('selected', 'selected');
    $('#id_oficina > option[value="'+oficina+'"]').attr('selected', 'selected');
    $('#id_responsable > option[value="'+responsable+'"]').attr('selected', 'selected');
    $('#status > option[value="'+status+'"]').attr('selected', 'selected');
  });
</script>