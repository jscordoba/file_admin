<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>Sadmin/nuevo_proceso/c">
  <fieldset>

  <!-- Form Name -->
  <div class="page-header">
      <h1><i class="fa fa-cog"></i> Nuevo Proceso</h1>
  </div>

  <!-- Text input-->
    <div class="form-group">
      <div class="col-md-4">
        <label for="">Nombre del proceso</label>  
        <input id="nom_proceso" name="nom_proceso" type="text" placeholder="Nombre del proceso" class="form-control input-md" required="">
      </div>

      <div class="col-md-4">
          <label for="">Responsable</label>
          <select id="id_responsable" name="id_responsable" class="form-control">
            <option value="">Seleccione una Opción</option>
            <?php foreach ($responsables->result() as $responsable): ?>
                 <option value="<?= $responsable->usuario_id ?>"><?= $responsable->username.$responsable->usuario_id ?></option>
            <?php endforeach; ?>
          </select>
      </div>
      <div class="col-md-4">
        <label for="">Seleccione la sección</label>
          <select id="id_seccion" name="id_seccion" class="form-control">
            <option value="">Seleccione una Opción</option>
            <?php foreach ($secciones->result() as $seccion): ?>
            <option value="<?= $seccion->id_seccion_company ?>"><?= $seccion->seccion_company_nombre ?></option>
            <?php endforeach; ?>
          </select>
      </div>
  </div>

  <!-- Select Basic -->
  <div class="form-group">
    <div class="col-md-6">
     <label for="">Seleccione la oficina</label>
      <select id="id_oficina" name="id_oficina" class="form-control">
        <option value="">Seleccione una Opción</option>
        <?php foreach ($oficinas->result() as $oficina): ?>
             <option value="<?= $oficina->id_oficina_company ?>"><?= $oficina->oficina_company_nombre ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="col-md-6">
     <label for="">Seleccione el estado</label>
      <select id="status" name="status" class="form-control" required>
        <option value="">Seleccione una Opción</option>
               <option value="Activo">Activo</option>
               <option value="Inactivo">Inactivo</option>
      </select>
    </div>
  </div>

  <!-- Button -->
  <div class="form-group">
    <br>
    <div class="col-md-12">
      <button id="btn_guardar_proceso" name="btn_guardar_proceso" class="btn btn-block btn-primary">Guardar</button>
    </div>
  </div>

  </fieldset>
</form>
