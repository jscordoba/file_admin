<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url() ?>Sadmin"><i class="fa fa-tachometer" aria-hidden="true"></i> File Admin - Magnetron S.A.S</a>
        </div>

        <!-- Top Menu Items DATOS DE USUARIO -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $this->session->userdata('usuario');?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?= base_url();?>Main/logout"><i class="fa fa-fw fa-power-off"></i> Cerrar Sesión</a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens  MENU LATERAL-->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="<?= base_url(); ?>Sadmin/index"><i class="fa fa-fw log"></i> Cuenta Super Administrador</a>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#usuario-m"><i class="fa fa-fw fa-user"></i> Usuarios
                    <!-- <i class="fa fa-fw fa-caret-down"></i> -->
                    </a>
                    <ul id="usuario-m" class="collapse">
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/nuevo_usuario"><i class="fa fa-plus-square-o fa-fw"></i> Nuevo</a>
                        </li>                  
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/lista_usuario"><i class="fa fa-table fa-fw"></i> Lista</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#proceso-m"><i class="fa fa-fw fa-cog"></i> Procesos 
                    <!-- <i class="fa fa-fw fa-caret-down"></i> -->
                    </a>
                    <ul id="proceso-m" class="collapse">
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/nuevo_proceso"><i class="fa fa-plus-square-o fa-fw"></i> Nuevo</a>
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/lista_proceso"><i class="fa fa-table fa-fw"></i> Lista</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#seccion-m"><i class="fa fa-fw fa-th-list"></i> Secciones 
                    <!-- <i class="fa fa-fw fa-caret-down"></i> -->
                    </a>
                    <ul id="seccion-m" class="collapse">
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/nueva_seccion"><i class="fa fa-plus-square-o fa-fw"></i> Nuevo</a>
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/lista_seccion"><i class="fa fa-table fa-fw"></i> Lista</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#oficina-m"><i class="fa fa-fw fa-building"></i> Oficinas 
                    <!-- <i class="fa fa-fw fa-caret-down"></i> -->
                    </a>
                    <ul id="oficina-m" class="collapse">
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/nueva_oficina"><i class="fa fa-plus-square-o fa-fw"></i> Nuevo</a>
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/lista_oficina"><i class="fa fa-table fa-fw"></i> Lista</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#grupo-m"><i class="fa fa-fw fa-users"></i> Grupos 
                    <!-- <i class="fa fa-fw fa-caret-down"></i> -->
                    </a>
                    <ul id="grupo-m" class="collapse">
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/nuevo_grupo"><i class="fa fa-plus-square-o fa-fw"></i> Nuevo</a>
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>Sadmin/lista_grupo"><i class="fa fa-table fa-fw"></i> Lista</a>
                        </li>
                    </ul>
                </li>
                <!-- <li>
                    <a href="index-rtl.html"><i class="fa fa-fw fa-arrows-h"></i>Derecha</a>
                </li> -->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>