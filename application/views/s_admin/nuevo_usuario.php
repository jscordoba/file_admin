<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>Sadmin/nuevo_usuario/c">
	<fieldset>

	<!-- Form Name -->
	<div class="page-header">
  		<h1><i class="fa fa-users"></i> Nuevo Usuario</h1>
	</div>
	
	<!-- Text input-->
	<div class="form-group">
	  <div class="col-md-4">
	  <label for="">Cédula o codigo unico</label>
	  <input id="id" name="id" type="text" placeholder="Cedula o codigo unico" class="form-control input-md" required>
	  </div>

	  <div class="col-md-4">
	  <label for="">Nombre Completo</label>
	  <input id="nombre_user" name="nombre_user" type="text" placeholder="Nombre completo" class="form-control input-md" required>
	  </div>

	  <div class="col-md-4">
	  <label for="">Usuario</label>
	  <input id="user" name="user" type="text" placeholder="Usuario" class="form-control input-md" required>
	  </div>
	</div>

	<!-- Password input-->
	<div class="form-group">
		<div class="col-md-4">
		  <label for="">Contraseña</label>
		    <input id="password" name="password" type="password" placeholder="Contraseña" class="form-control input-md" required>
		</div>

	  	<div class="col-md-4">
	  		<label for="">Seleccione el estado del usuario</label>
	  		<input id="email" name="email" type="email" placeholder="email@magnetron.com.co" class="form-control input-md" required>
	  	</div>

		<div class="col-md-4">
		  <label for="">Seleccione la seccion</label>
		    <select id="id_seccion" name="id_seccion" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		       <?php foreach ($secciones->result() as $seccion): ?>
		             <option value="<?= $seccion->id_seccion_company ?>"><?= $seccion->seccion_company_nombre ?></option>
		        <?php endforeach; ?>
		    </select>
		</div>
	</div>

	<!-- Select Basic -->
	<div class="form-group">
	  	<div class="col-md-4">
		    <label for="">Seleccione la oficina</label>
		    <select id="id_oficina" name="id_oficina" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		       <?php foreach ($oficinas->result() as $oficina): ?>
		             <option value="<?= $oficina->id_oficina_company ?>"><?= $oficina->oficina_company_nombre ?></option>
		        <?php endforeach; ?>
		    </select>
	  	</div>

	  	<div class="col-md-4">
		    <label for="">Seleccione el perfil</label>
		    <select id="id_perfil" name="id_perfil" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		       <?php foreach ($perfiles->result() as $perfil): ?>
		             <option value="<?= $perfil->id_perfil_usuario ?>"><?= $perfil->perfil_usuario_nombre ?></option>
		        <?php endforeach; ?>
		    </select>
	  	</div>

	    <div class="col-md-4">
		  	<label for="">Seleccione el estado</label>
		    <select id="status_user" name="status_user" class="form-control" required>
		      <option value="">Seleccione una Opción</option>
		             <option value="Activo">Activo</option>
		             <option value="Inactivo">Inactivo</option>
		    </select>
		</div>
	</div>

	<!-- Button -->
	<div class="form-group">
	<br>
	  <div class="col-md-12">
	    <button id="btn_guardar_user" name="btn_guardar_user" class="btn btn-block btn-primary">Guardar</button>
	  </div>
	</div>

	</fieldset>
</form>

