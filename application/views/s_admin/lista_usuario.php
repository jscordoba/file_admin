<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
  <h3><i class="fa fa-user"></i> Usuarios</h3>
</div>



    <table id="table" class="table table-striped table-bordered">
    <thead class="bg-black">
        <tr>
            <th class="text-center col-md-1">#</th>
            <th class="text-center">Nombre</th>
            <th class="text-center">Usuario</th>
            <th class="text-center">Correo Electronico</th>
            <th class="text-center">Acceso</th>
            <!-- <th>Fecha Creación</th> -->
            <th class="text-center">Sección</th>
            <th class="text-center">Oficina</th>
            <th class="text-center">Estatus</th>
            <th class="text-center"><i class="fa fa-cogs"></i></th>
            <th class="text-center"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody>

    <?php 
    if ($usuarios) {
        # code...
        foreach ($usuarios->result() as $Usuario): ?>
             <tr>
                <td class="text-center"><?= $Usuario->usuario_id ?></td>
                <td class="text-center"><?= $Usuario->usuario_nombre ?></td>
                <td class="text-center"><?= $Usuario->username ?></td>
                <td class="text-center"><?= $Usuario->email ?></td>
                <td class="text-center"><?= $Usuario->perfil_usuario_nombre ?></td>
                <!-- <td><?= $Usuario->fecha_creacion ?></td> -->
                <td class="text-center"><?= $Usuario->seccion_company_nombre ?></td>
                <td class="text-center"><?= $Usuario->oficina_company_nombre ?></td>
                <td class="text-center"><?= $Usuario->status ?></td>
                <td class="text-center">
                <a  class="btn btn-xs btn-primary" id="btn_editar_usuario" href="<?= base_url(); ?>Sadmin/consultar_usuario/<?= $Usuario->usuario_id ?>" title="Editar"><i class="fa fa-pencil"></i></a>
                </td>
                <td>
                   <?php if ($Usuario->status == "Activo") {
                        echo "<a  class='btn btn-xs btn-success' id='btn_editar_usuario' href='".base_url()."Sadmin/status_usuario/$Usuario->usuario_id' title='Cambiar status a inactivo'><i class='fa fa-toggle-on'></i></a>";
                   }else{
                     echo "<a  class='btn btn-xs btn-default' id='btn_editar_usuario' href='".base_url()."Sadmin/status_usuario/$Usuario->usuario_id' title='Cambiar status a activo'><i class='fa fa-toggle-off'></i></a>";
                    } ?>
                </td>
            </tr>
        <?php endforeach;

    }

    ?>
    </tbody>
    </table>    

