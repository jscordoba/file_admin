<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
  <h3><i class="fa fa-cog"></i> Procesos</h3>
</div>

<table id="table" class="table table-striped table-bordered">
    <thead class="bg-black">
        <tr>
            <th class="text-center">Responsable</th>
            <!-- <th class="text-center">#</th> -->
            <th class="text-center">Nombre</th>
            <th class="text-center">Creación</th>
            <th class="text-center">Actualización</th>
            <!-- <th class="text-center">Usuario create</th> -->
            <th class="text-center">Sección</th>
            <th class="text-center">Oficina</th>
            <th class="text-center">Estatus</th>
            <th class="text-center"><i class="fa fa-cogs"></i></th>
            <th class="text-center"><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>

    <tbody>

    <?php 
    if ($procesos) {
        # code...
        foreach ($procesos->result() as $proceso): ?>
             <tr>
                <td class="text-center"><?= $proceso->usuario_nombre ?></td>
                <!-- <td class="text-center"><?= $proceso->id_proceso ?></td> -->
                <td class="text-center"><?= $proceso->proceso_nombre ?></td>
                <td class="text-center"><?= $proceso->proceso_creacion ?></td>
                <td class="text-center"><?= $proceso->proceso_update ?></td>
                <!-- <td class="text-center"><?= $proceso->usuario_id_creacion ?></td> -->
                <td class="text-center"><?= $proceso->seccion_company_nombre ?></td>
                <td class="text-center"><?= $proceso->oficina_company_nombre ?></td>
                <td class="text-center"><?= $proceso->status ?></td>
                <td class="text-center">
                <a  class="btn btn-xs btn-primary" id="btn_editar_usuario" href="<?= base_url(); ?>Sadmin/consultar_proceso/<?= $proceso->id_proceso ?>" title="Editar"><i class="fa fa-pencil"></i></a>
                </td>
                <td>
                   <?php if ($proceso->status == "Activo") {
                        echo "<a  class='btn btn-xs btn-success' id='btn_editar_usuario' href='".base_url()."Sadmin/status_proceso/$proceso->id_proceso' title='Cambiar status a inactivo'><i class='fa fa-toggle-on'></i></a>";
                   }else{
                     echo "<a  class='btn btn-xs btn-default' id='btn_editar_usuario' href='".base_url()."Sadmin/status_proceso/$proceso->id_proceso' title='Cambiar status a inactivo'><i class='fa fa-toggle-off'></i></a>";
                    } ?>
                </td>
            </tr>
        <?php endforeach;
    }

    ?>

    </tbody>
</table>