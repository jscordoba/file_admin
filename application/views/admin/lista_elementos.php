<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
    <h3><i class="fa fa-ticket"></i> Elementos de <?php echo $this->session->userdata('usuario_nombre'); ?> </h3>
</div>
    <table id="table" class="table table-striped table-bordered">
        <thead class="bg-black">
            <tr>
                <!-- <th class="text-center">Creador</th> -->
                <th class="text-center">Nombre Elemento</th>
                <th class="text-center">Última Actualización</th>
                <th class="text-center">Estado</th>
                <th class="text-center"><i class="fa fa-cogs"></i></th>
            </tr>
        </thead>
        <!-- <tfoot> -->
            <!-- <tr> -->
                <!-- <th class="text-center">Creador</th> -->
                <!-- <th class="text-center">Nombre Elemento</th> -->
                <!-- <th class="text-center">Última Actualización</th> -->
                <!-- <th class="text-center">Estado</th> -->
                <!-- <th class="text-center"><i class="fa fa-cogs"></i></th> -->
            <!-- </tr> -->
        <!-- </tfoot> -->
        <tbody>
            <?php 
            if ($elementos) {
                foreach ($elementos->result() as $elemento){ ?>
                     <tr>
                        <!-- <td class="text-center"><?= $elemento->usuario_id_creacion ?></td> -->
                        <td class="text-center"><?= $elemento->elemento_nombre ?></td>
                        <td class="text-center"><?= $elemento->elemento_update ?></td>
                        <td class="text-center"><?= $elemento->status ?></td>
                        <td class="text-center">
                        <?php 
                            $usuario_id = $this->session->userdata('usuario_id');
                            if($usuario_id == $elemento->usuario_id_creacion){ ?>
                                <a  class="btn btn-xs btn-primary" id="btn_editar_usuario" href="<?= base_url(); ?>Admin/consultar_elemento/<?= $elemento->id_elemento ?>" title="Editar"><i class="fa fa-pencil"></i></a>

                                <?php if($elemento->status == "Activo"){ ?>
                                    <a  class='btn btn-xs btn-success' id='btn_editar_usuario' href='<?= base_url(); ?>Admin/status_elemento/<?= $elemento->id_elemento ?>' title="Cambiar status a inactivo"><i class='fa fa-toggle-on'></i></a>
                                    <?php }else{ ?>
                                    <a  class='btn btn-xs btn-default' id='btn_editar_usuario' href='<?= base_url(); ?>Admin/status_elemento/<?= $elemento->id_elemento ?>' title="Cambiar status a activo"><i class='fa fa-toggle-on'></i></a>
                                <?php } ?>
                                <?php 
                                 echo "<button type='button' class='btn btn-primary btn-xs' onclick='document_element($elemento->id_elemento);' title='Mostar documentos de este elemento'><i class='fa fa-file'></i></button>";
                                ?>
                                <?php }else{ ?>
                                <a  class="btn btn-xs btn-primary disabled" id="btn_editar_usuario" href="<?= base_url(); ?>Admin/consultar_elemento/<?= $elemento->id_elemento ?>"><i class="fa fa-pencil"></i></a>

                                <a  class='btn btn-xs btn-success disabled' id='btn_editar_usuario' href='<?= base_url(); ?>Admin/status_elemento/<?= $elemento->id_elemento ?>'><i class='fa fa-toggle-on'></i></a>
                                <?php 
                                 echo "<button type='button' class='btn btn-primary btn-xs disabled' onclick='document_element($elemento->id_elemento);' title='Mostar documentos de este elemento'><i class='fa fa-file'></i></button>";
                                ?>
                             <?php } ?>
                             <button class="btn btn-primary btn-xs" onclick="mostar_grupos_elemento(<?= $elemento->id_elemento ?>)" title=""><i class="fa fa-group"></i></button>
                        </td>

                    </tr>
                <?php 
                }
            }
            ?>
        </tbody>
    </table>

<div class="modal fade" id="documentos_grupo">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">DOCUMENTOS</h3>
            </div>
            <div class="modal-body">
                <table id="g2" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                    <thead class="bg-black">
                        <tr>
                            <th class="text-center">Codigo</th>
                            <th class="text-center">Nombre documento</th>
                            <th class="text-center">Status</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                 
                    <tbody id="tbody">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-grupos_x_elemento">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">GRUPOS ASIGNADOS</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                              <table  class="table table-bordered" >
                                <thead class="bg-black">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Nombre del grupo</th>
                                        <th class="text-center"><i class="fa fa-cog"></i></th>
                                    </tr>
                                </thead>
                               
                                <tbody id="tbody_grupos">
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script scr="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    function document_element(id){
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/lista_documentos_elemento', {id: id }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody").text("");
                $("#tbody").append(data);
                $("#documentos_grupo").modal("show");
            }
        });
    }

    function mostar_grupos_elemento(id){
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/mostar_grupos_elemento', {id: id }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody_grupos").text("");
                $("#tbody_grupos").append(data);
                $("#modal-grupos_x_elemento").modal("show");
            }
        });
    }

    function eliminar_grupo_elemento(grupo,elemento){
        // alert(id + " - "+proceso);
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/eliminar_grupo_elemento', {grupo: grupo, elemento: elemento }, function(data, textStatus, xhr) {
            if (data) {
                // $("#tbody_grupos").text("");
                // $("#tbody_grupos").append(data);
                $("#modal-grupos_x_elemento").modal("hide");
                setTimeout(function() {location.reload();}, 500);
            }else{
                alert("Error");
            }
        });
    }
</script>