<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    if (!isset($recurso)) {
        $recurso_nombre = "Todo";
        $id_recurso = "r";
    }else{
       foreach($recurso->result() as $row){
         $recurso_nombre = $row->recurso_nombre;
        $id_recurso= $row->id_recurso;
       }
    }

?>
<!-- Form Name -->
  <div class="page-header">
    <h2>
    <a href="<?= base_url("Admin/lista_recurso/".$this->uri->segment(3)."");?>"><i class="fa fa-arrow-circle-o-left" aria-hidden="true" title="Regresar a la pagina anterior."></i></a> Elementos del recurso <b><?= $recurso_nombre ?></b></h2>
  </div>
<table id="table" class="table table-striped table-bordered ">
    <thead class="bg-black">
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">Nombre Elemento</th>
            <th class="text-center">Última Actualización</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if ($elementos) {
            $i = 1;
            foreach ($elementos->result() as $elemento){ ?>
            <?php echo "<script>console.log(".json_encode($elemento).");</script>"; 
            $proceso = $this->uri->segment(3);
            $recurso = $this->uri->segment(4);
            ?>
                 <tr>
                 <td class="text-center"><?= $i++ ?></td>
                    <td class="text-center"><a href="<?= base_url() ?>Admin/documents_has_element/<?= $proceso ?>/<?= $recurso ?>/<?= $elemento->id_elemento ?>"><?= $elemento->elemento_nombre ?></a></td>
                    <td class="text-center"><?= $elemento->elemento_update ?></td>
                
                </tr>
            <?php }
        }else{ ?>
            <tr>
                <td class="text-center" colspan="4">
                    Este recurso no tiene elementos asignados
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>



