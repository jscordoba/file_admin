<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>admin/Assign_element_group">
<fieldset>

  <!-- Form Name -->
  <div class="page-header">
    <h1><i class="fa fa-ticket"></i> Asignar elemento a un grupo</h1>
  </div>

  <!-- Text input-->
  <div class="form-group">
    <div class="col-md-6">
      <label  for="nom_elemento">Elemento</label>  
      <select id="id_elemento" name="id_elemento" class="form-control" required>
        <option value="">Seleccione una Opción</option>
        <?php  foreach ($elementos->result() as $elemento): ?>
             <option value="<?= $elemento->id_elemento ?>"><?= $elemento->elemento_nombre ?></option>
        <?php endforeach; ?>
      </select>
    </div>


    <div class="col-md-6">
      <label for="grupo">Grupo Principal</label>
      <select id="grupo" name="grupo" class="form-control" required>
      </select>
    </div>
  </div>
  <!-- Button -->
  <div class="form-group">
  <br>
    <div class="col-md-12">
      <button id="btn_guardar_grupo" name="btn_guardar_grupo" class="btn btn-block btn-primary">Guardar</button>
    </div>
  </div>

</fieldset>
</form>


<script>
jQuery(document).ready(function($) {
  var g = $("#grupo").val();
  if (g == 0 || g == null) {
    $("#btn_guardar_grupo").attr('disabled', 'disabled');
  }
});

$("#grupo").change(function(event) {
  var g = $("#grupo").val();
  if (g == 0 || g == null) {
    $("#btn_guardar_grupo").attr('disabled', 'disabled');
  }else{
     $("#btn_guardar_grupo").removeAttr('disabled');
  }
});

$("#id_elemento").change(function(event) {
  var base_url = "<?php echo base_url(); ?>";
  var id_elemento = $("#id_elemento").val();
  var parametros = {"componente":id_elemento,"tipo":"2"};
    $.ajax({
      url: ''+base_url+'Admin/listar_grupos_x_asignar',
      type: 'post',
      data: parametros,
    })
    .done(function(data) {
      console.log(data);
      $("#grupo").empty();
      $("#grupo").append('<option value="" selected disabled="">Seleccione una opción</option>');
      $("#grupo").append(data);
    })
    .fail(function(data_error) {
      alert(data_error);
    });
  });



</script>