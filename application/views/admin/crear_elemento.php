<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>admin/nuevo_elemento/c">
<fieldset>

  <!-- Form Name -->
    <div class="page-header">
    <h1><i class="fa fa-ticket"></i> Crear Elemento</h1>
  </div>

  <!-- Text input-->
  <div class="form-group">
    <div class="col-md-6">
      <label for="nom_elemento">Nombre Elemento</label>  
      <input id="nom_elemento" name="nom_elemento" type="text" placeholder="Nombre del elemento" class="form-control input-md" required>
    </div>

    <div class="col-md-6">
      <label for="">Seleccione el estado</label>
        <select id="status" name="status" class="form-control" required>
          <option value="">Seleccione una Opción</option>
               <option value="Activo">Activo</option>
               <option value="Inactivo">Inactivo</option>
       </select>
    </div>
  </div>

  <!-- Button -->
  <div class="form-group">
  <br>
    <div class="col-md-12">
      <button id="btn_guardar_grupo" name="btn_guardar_grupo" class="btn btn-block btn-primary">Guardar</button>
    </div>
  </div>

</fieldset>
</form>

