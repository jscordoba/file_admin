<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
   <h3><i class="fa fa-ticket"></i> Documentos de <?php echo $this->session->userdata('usuario_nombre'); ?> </h3>
</div>
    <table id="table" class="table table-striped table-bordered ">
        <thead class="bg-black">
            <tr>
                <th class="text-center">Creador</th>
                <th class="text-center">Nombre Elemento</th>
                <th class="text-center">Última Actualización</th>
                <th class="text-center">Estado</th>
                <th class="text-center"><i class="fa fa-cog"></i></th>
            </tr>
        </thead>
     <!--    <tfoot>
            <tr>
                <th class="text-center">Creador</th>
                <th class="text-center">Nombre Elemento</th>
                <th class="text-center">Última Actualización</th>
                <th class="text-center">Estado</th>
                <th class="text-center">Accón</th>
            </tr>
        </tfoot> -->
        <tbody>
            <?php 
            if ($elementos) {
                foreach ($elementos->result() as $elemento){ ?>
                     <tr>
                        <td class="text-center"><?= $elemento->usuario_id_creacion ?></td>
                        <td class="text-center"><?= $elemento->elemento_nombre ?></td>
                        <td class="text-center"><?= $elemento->elemento_update ?></td>
                        <td class="text-center"><?= $elemento->status ?></td>
                        <td class="text-center">
                        <?php 
                            echo "<button type='button' class='btn btn-primary btn-xs' onclick='document_element($elemento->id_elemento);' title='Mostar documentos de este elemento'><i class='fa fa-file'></i></button>";
                         ?>
                        </td>
                    </tr>
                <?php 
                }
            }
            ?>
        </tbody>
    </table>

<div class="modal fade" id="documentos_grupo">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <h3 class="modal-title text-center">DOCUMENTOS</h3>
            </div>
            <div class="modal-body">
                <table id="g2" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                    <thead class="bg-black">
                        <tr>
                            <th class="text-center">Codigo</th>
                            <th class="text-center">Nombre documento</th>
                            <th class="text-center">Status</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script scr="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    function document_element(id){
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/lista_documentos_elemento', {id: id }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody").text("");
                $("#tbody").append(data);
                $("#documentos_grupo").modal("show");
            }
        });
    }

    function status_document(documento,elemento,recurso){
        var base_url = "<?php echo base_url(); ?>";
        // var elemento = $("#elemento").val();
        $.post(''+base_url+'Main/status_document', {documento: documento, elemento: elemento, recurso: recurso }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody").text("");
                $("#tbody").append(data);
                // $("#documentos_grupo").modal("show");
            }
        });
    }
</script>