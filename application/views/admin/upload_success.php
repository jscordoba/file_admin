<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="page-header">
  <h1><i class="fa fa-upload"></i> Felicitaciones!</h1>
</div>
<ul>
<?php 
	
	foreach ($success as $item => $value) {
		echo '<li>'.$item. " - ".$value.'</li>';
	}

 ?>
</ul>
 