<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    if (!isset($recurso)) {
        # code...
        $recurso_nombre = "Todo";
        // $id_recurso = "r";
    }else{
        $recurso_nombre = $recurso->recurso_nombre;
        // $id_recurso= $recurso->id_recurso;
    }

?>
<!-- Form Name -->
<legend>Lista de Versiones de: <?= $recurso_nombre ?></legend>

<a href="<?= base_url() ?>Admin/nueva_version/<?= $version?>" class="btn btn-sm btn-primary block-rigth">Nueva Versión</a>
<div class="table-responsive">
<table id="" class="table table-striped table-bordered display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Versión</th>
            <th>Creación</th>
            <th>Visible</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Versión</th>
            <th>Creación</th>
            <th>Visible</th>
        </tr>
    </tfoot>
    <tbody>
        <?php 
        if ($versiones) {
            # code...
            foreach ($versiones->result() as $version): ?>
                 <tr>
                    <td><a href="<?= base_url() ?>Main/ver_version/<?= $version->document_id_document ?>" target="_blanck"><?= $version->version_nombre ?></a></td>
                    <td><?= $version->version_creacion ?></td>
                    <td>
                    <?php if ($version->document_version_visible == 1){ ?>
                        <div class="btn-group col-md-8" data-toggle="buttons">
                            <label class="btn btn-default chk_visible active" data-target="#cambiar_visible"><input name="chk_visible" value="1" type="radio">SI</label>
                            <label class="btn btn-default chk_visible"><input checked name="chk_visible" value="2" type="radio">No</label>
                        </div>

                    <?php }else{ ?>
                            <div class="btn-group col-md-8" data-toggle="buttons">
                                <label class="btn btn-default chk_visible" data-target="#cambiar_visible"><input name="chk_visible" value="1" type="radio">SI</label>
                                <label class="btn btn-default chk_visible active"><input checked name="chk_visible" value="2" type="radio">No</label>
                            </div>
                            
                    <?php } ?>
                    <!-- <input type="checkbox" checked> -->
                    </td>
                </tr>
            <?php endforeach;
        }
        ?>
    </tbody>
</table>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- <a href="<?= base_url() ?>Admin/lista_elemento" class="btn btn-sm btn-success">Volver</a> -->
    </div>
</div>
