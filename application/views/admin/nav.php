<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?= base_url() ?>Admin">File Admin - Magnetron S.A.S</a>
    </div>

    <!-- Top Menu Items DATOS DE USUARIO -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $this->session->userdata('usuario');?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?= base_url();?>Main/logout"><i class="fa fa-fw fa-power-off"></i> Cerrar Sesión</a>
                </li>
            </ul>
        </li>
    </ul>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens  MENU LATERAL-->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        
        <ul class="nav navbar-nav side-nav">
            <li>
                <a href="<?= base_url(); ?>Admin/index"><i class="fa fa-fw fa-user"></i> Usuario Administrador</a>
            </li>
            <li>
                <a href="<?= base_url(); ?>Admin/lista_proceso" data-target="#proceso-m"><i class="fa fa-fw fa-cog"></i> Procesos Asignados</i></a>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#recurso-m"><i class="fa fa-fw fa-list"></i> Recursos </a>
                <ul id="recurso-m" class="collapse">
                    <li>
                        <a href="<?= base_url(); ?>Admin/nuevo_recurso"><i class="fa fa-plus-square-o fa-fw"></i> Nuevo</a>
                    </li>
                    <li>
                        <a href="<?= base_url(); ?>Admin/lista_recurso"><i class="fa fa-table fa-fw"></i> Lista</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#elemento-m"><i class="fa fa-fw fa-ticket"></i> Elementos </a>
                <ul id="elemento-m" class="collapse">
                 <!--    <li>
                        <a href="<?= base_url(); ?>Admin/nuevo_elemento"><i class="fa fa-thumb-tack fa-fw"></i> Asignar</a>
                    </li> -->
                     <li>
                        <a href="<?= base_url(); ?>Admin/asignar_grupo"><i class="fa fa-thumb-tack fa-fw"></i> Asignar Grupo</a>
                    </li>
                     <li>
                        <a href="<?= base_url(); ?>Admin/asignar_recurso"><i class="fa fa-thumb-tack fa-fw"></i> Asignar Recurso</a>
                    </li>
                    <li>
                        <a href="<?= base_url(); ?>Admin/crear_elemento"><i class="fa fa-plus-square-o fa-fw"></i> Crear Elemento</a>
                    </li>
                    <li>
                        <a href="<?= base_url(); ?>Admin/lista_elementos"><i class="fa fa-table fa-fw"></i> Lista</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#documentos-m"><i class="fa fa-fw fa-file"></i> Documentos </a>
                <ul id="documentos-m" class="collapse">
                    <li>
                        <a href="<?= base_url(); ?>Admin/upload_file"><i class="fa fa-upload fa-fw"></i> Subir</a>
                    </li>
                    <li>
                        <a href="<?= base_url(); ?>Admin/lista_documentos"><i class="fa fa-plus-square-o fa-fw"></i> Lista</a>
                    </li>
                  
                </ul>
            </li>
            <!-- <li>
                <a href="index-rtl.html"><i class="fa fa-fw fa-arrows-h"></i>Derecha</a>
            </li> -->
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>