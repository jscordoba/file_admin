<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="<?= base_url()?>styles/template/css/fileinput.css" rel="stylesheet">
<!-- Form Name -->
<div class="page-header">
    <h2>
    <a href="<?= base_url("Admin/elementos_has_recurso/".$this->uri->segment(3)."/".$this->uri->segment(4)."");?>"><i class="fa fa-arrow-circle-o-left" aria-hidden="true" title="Regresar a la pagina anterior."></i></a>Documentos del elemento <b><?=$elemento_nombre?></b></h2>
    <input type="hidden" id="input_id_elemento" class="form-control" value="<?=$id?>">
</div>
<div class="row">
	<div class="col-md-3">
		<a data-toggle="modal" href="#modal-upload" onclick="open_upload()" type="button" class="btn btn-sm btn-primary">Subir documento</a>
	</div>
</div>
<br>
<div class="row">
	<div class="col-md-12">
		<table id="table" class="table table-striped table-bordered ">
			<thead class="bg-black">
			    <tr>
			        <th class="text-center">Codigo</th>
			        <th class="text-center">Nombre documento</th>
			        <th class="text-center">Status</th>
			        <th class="text-center"><i class="fa fa-cog"></i></th>
			    </tr>
			</thead>
			<tfoot>
			    <tr>
			        <th class="text-center">Codigo</th>
			        <th class="text-center">Nombre documento</th>
			        <th class="text-center">Status</th>
			        <th class="text-center"><i class="fa fa-cog"></i></th>
			    </tr>
			</tfoot>
			<tbody id="tbody-dhe">
			<?php 
				if ($documentos != false) {
					foreach ($documentos->result() as $row) {?>
				        <tr>
				            <td class="text-center"><?= $row->id_document ?></td>
				            <td class="text-center"><a href="'.base_url().'files/'<?= $row->nombre ?>" target="_blank"><?= $row->nombre ?></a></td>
				            <td class="text-center"><?= $row->status ?></td>
				            <td class="text-center">
				            <?php if ($row->status == "Activo") {?>
	                    		<a href="<?= base_url("Main/cambiar_estado_documento/".$this->uri->segment(3)."/".$this->uri->segment(4)."/".$this->uri->segment(5)."/$row->id_document");?>" class="btn btn-xs btn-success"><i class="fa fa-toggle-on"></i></a>
	                    	<?php }else{?>
	                    		<a href="<?= base_url("Main/cambiar_estado_documento/".$this->uri->segment(3)."/".$this->uri->segment(4)."/".$this->uri->segment(5)."/$row->id_document");?>" class="btn btn-xs btn-default"><i class="fa fa-toggle-off"></i></a>
	                    	<?php	} ?>
				          </td>
				        </tr>

					<?php }
				}else{ ?>
					<tr><td colspan="3" class="text-center">Este elemento no tiene documentos asignados</td></tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-upload" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
				<h4 class="modal-title"><i class="fa fa-upload"></i> Subir Documento</h4>
			</div>
			<form action="<?php echo base_url('Admin/do_upload1') ?>" method="post" enctype="multipart/form-data">
			<div class="modal-body">
				<div class="row">
				    <div class="col-md-12 ">
					    <div class="form-group">
					        <label for="userfile">Seleccione un documento pdf</label>
					        <input type="file" id="userfile" class="file form-control" name="userfile" data-show-preview="false" required>
					    	<input type="text" name="userfile_name" id="userfile_name" class="form-control hidden" required value="<?=$elemento_nombre?>">
					    	<input type="text" name="proceso" id="proceso" class="form-control hidden" required>
					    	<input type="text" name="recurso" id="recurso" class="form-control hidden" required>
					    	<input type="text" name="elemento" id="elemento" class="form-control hidden" required>
					    	<p><b>Nota : </b>El documento subido se le asignara el procesoy el recurso proceso por el que ud a entrado.</p>
					    </div>
				    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" id="btn_upload" class="btn btn-primary">Guardar</button>
				<button type="button" class="btn btn-default" onclick="cancelar();">Cancelar</button>

			</div>
			</form>
		</div>
	</div>
</div>
<script>
function open_upload(){
	var proceso = "<?php echo $this->uri->segment(3); ?>";
	var recurso = "<?php echo $this->uri->segment(4); ?>";
	var elemento ="<?php echo $this->uri->segment(5); ?>";

	$("#proceso").val(proceso);
	$("#recurso").val(recurso);
	$("#elemento").val(elemento);
}

// document.getElementById('userfile').onchange = function () {
//   console.log(this.value);
//   // document.getElementById('userfile_name').value = document.getElementById('userfile').files[0].name;
//   var name = document.getElementById('userfile').files[0].name;
//   var file = name.replace(/^.*\/|\.[^.]*$/g, '')
//   document.getElementById('userfile_name').value = file;
// }

function cancelar(){
	$("#proceso_id").val("");
	$("#proceso_name").val("");
	$("#userfile").val(null);
	$("#userfile_name").val();
	// console.log($("#userfile").val());
	$("#modal-upload").modal("hide");
}


</script>