<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<form class="form-horizontal" method="POST" action="<?= base_url();?>admin/nueva_version/c" enctype="multipart/form-data">
<fieldset>

<!-- Form Name -->
<legend>Nueva Versión</legend>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="upl_version">Cargar versión</label>
  <div class="col-md-4">
    <input id="upl_version" name="upl_version" class="input-file" type="file">
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="g_visible">Visibilidad</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="g_visible-0">
      <input type="radio" name="g_visible" id="g_visible-0" value="1" checked="checked">
      Visible
    </label>
  </div>
  <div class="radio">
    <label for="g_visible-1">
      <input type="radio" name="g_visible" id="g_visible-1" value="2">
      Oculto
    </label>
  </div>
  </div>
</div>

<input type="hidden" value="<?= $id_recurso_has_elemento ?>" name="id_recurso_has_elemento">

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="btn_guardarVersion"></label>
  <div class="col-md-4">
    <button id="btn_guardarVersion" name="btn_guardarVersion" class="btn btn-primary">Guardar</button>
  </div>
</div>

</fieldset>
</form>
