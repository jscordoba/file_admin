<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Form Name -->
<div class="page-header">
  <h3><i class="fa fa-cog"></i> Procesos de <b><?php  echo $this->session->userdata('usuario_nombre');?></b> </h3>
</div>

<table id="table" class="table table-bordered table-condensed " >
    <thead class="bg-black">
        <tr>
            <th class="text-center">Nombre</th>
            <th class="text-center">Sección</th>
            <th class="text-center">Oficina</th>
            <th class="text-center col-md-1"><i class="fa fa-cog"></i></th>
            <th class="text-center col-md-1"><i class="fa fa-cog"></i></th>
        </tr>
    </thead>
<!--      <tfoot class="bg-black">
        <tr>
            <th class="text-center">Nombre</th>
            <th class="text-center">Sección</th>
            <th class="text-center">Oficina</th>
            <th class="text-center col-md-1"><i class="fa fa-cog"></i></th>
            <th class="text-center col-md-1"><i class="fa fa-cog"></i></th>
        </tr>
    </tfoot> -->
    <tbody>
        <?php 
            if ($procesos) {
                foreach ($procesos->result() as $proceso){ ?>
                     <tr>
                        <td class="text-center"><a href="<?= base_url() ?>Admin/lista_recurso/<?= $proceso->id_proceso?>"><?= $proceso->proceso_nombre ?></a></td>
                        <td class="text-center"><?= $proceso->seccion_company_nombre ?></td>
                        <td class="text-center"><?= $proceso->oficina_company_nombre ?></td>
                        <td class="text-center">
                            <button class="btn btn-primary btn-xs btn-asignarPermisosProc" data-toggle="modal" data-target="#m_accesoRecurso" data-proceso_id="<?=  $proceso->id_proceso ?>" title="Asignar grupo a este proceso"><i class="fa fa-check"></i></button>
                        </td>
                        <td class="text-center"><button class="btn btn-primary btn-xs" onclick="mostar_grupos_proceso(<?= $proceso->id_proceso ?>)" title="Asignar grupo a este proceso" data-toggle="modal" data-target="#modal-grupos_x_proceso"><i class="fa fa-group"></i></button></td>

                    </tr>
                <?php 
                }
            }
        ?>
    </tbody>
</table>
<!-- Modal ASIGNAR PERMISOS AL MODULO DE UN PROCESO -->
<div class="modal fade" id="m_accesoRecurso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">ASIGNAR PERMISOS A ESTE PROCESO</h3>
          </div>

           <form action="<?= base_url();?>admin/asignar_acceso/Process" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                          <label for="grupo">Seleccione el grupo que desea asignar</label>
                            <select id="grupo" name="grupo" class="form-control" required="">
                              
                            </select>
                          </div>
                        </div>            
                    </div>
                </div>
                <input type="hidden" value="" name="componente" id="componente" required>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="modal-grupos_x_proceso">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">GRUPOS ASIGNADOS</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                              <table  class="table table-bordered " >
                                <thead class="bg-black">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Nombre del grupo</th>
                                        <th class="text-center"><i class="fa fa-cog"></i></th>
                                    </tr>
                                </thead>
                               
                                <tbody id="tbody_grupos">
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function mostar_grupos_proceso(id) {
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/mostar_grupos_proceso', {id: id }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody_grupos").text("");
                $("#tbody_grupos").append(data);
                // $("#modal-grupos_x_proceso").modal("show");
            }
        });
    }

    function eliminar_grupo_proceso(grupo,proceso){
        // alert(id + " - "+proceso);
        var base_url = "<?php echo base_url(); ?>";
        $.post(''+base_url+'Main/eliminar_grupo_proceso', {grupo: grupo, proceso: proceso }, function(data, textStatus, xhr) {
            if (data) {
                // $("#tbody_grupos").text("");
                // $("#tbody_grupos").append(data);
                $("#modal-grupos_x_proceso").modal("hide");
                setTimeout(function() {location.reload();}, 500);
            }else{
                alert("Error");
            }
        });
    }
</script>


<!-- =================================================================== -->
<!-- =================================================================== -->

<?php

/* EXPLORADOR DE CARPETAS Y ARCHIVOS CON PHP
 * Documento explora.php
 * Explorador de carpetas y archivos usando el sistema de ficheros de PHP.
 * Autor: Andrés de la Paz © 2010
 * Contacto: www.wextensible.com
 */

//Por defecto la primera vez que abrimos este explorador toma
//como carpeta la del php actual. La constante __FILE__ es la
//ruta del archivo PHP actual. Con dirname obtenemos la carpeta.
$ruta = dirname(__FILE__)."/";

//Para abrir archivos necesitaremos poner la codificación
//adecuada con estos valores
$array_codif = Array(
                        "UTF-8",
                        "ISO-8859-1",
                        "ISO-8859-15"
                );

//Por defecto usamos esta para htmlentities (ver más abajo)
$codificacion = "ISO-8859-1";

//Vemos si hay algo en el GET
if (isset($_GET)){

    foreach($_GET as $campo=>$valor){

        switch ($campo) {

            //Obtenemos una ruta, carpeta o archivo
            case "una-ruta":
                $ruta = htmlspecialchars($valor, ENT_QUOTES); 

                if (get_magic_quotes_gpc() == 1) $ruta = stripslashes($ruta);
                break;

            //Vemos la codificación
            case "recur":
                $recur = $valor;
                // $codificacion = htmlspecialchars($valor, ENT_QUOTES);
                if (get_magic_quotes_gpc() == 1) $ruta = stripslashes($ruta);
                break;
        }
    }
}

//Si la ruta es vacía, pone la del presente script
if ($ruta == "") $ruta = dirname(__FILE__)."/";

//Esta variable contendrá la lista de nodos (carpetas y archivos)
$presenta_nodos = "";

//Esta variable es para el contenido del archivo
$presenta_archivo = "";

//Si la ruta es una carpeta, la exploramos. Si es un archivo
//sacamos también el contenido del archivo.
if (is_dir($ruta)){//ES UNA CARPETA
    //Con realpath convertimos los /../ y /./ en la ruta real
    $ruta = realpath($ruta)."/";
    //exploramos los nodos de la carpeta
    $presenta_nodos = explora_ruta($ruta);
} else {//ES UN ARCHIVO
    $ruta = realpath($ruta);
    //Sacamos también los nodos de la carpeta
    $presenta_nodos = explora_ruta(dirname($ruta)."/");
    //Y sacamos el contenido del archivo
    
    // $rutaUrl = str_replace(" ", "%20", $ruta);
    // echo $rutaUrl."<a target='_blank' href='$rutaUrl'>Prueba</a>";


    $presenta_archivo = "<br />CONTENIDO DEL ARCHIVO: <pre> $ruta  </pre>";//.explora_archivo($ruta, $codificacion).
}
//Función para explorar los nodos de una carpeta
//El signo @ hace que no se muestren los errores de restricción cuando
//por ejemplo open_basedir restringue el acceso a algún sitio
function explora_ruta($ruta){
    //En esta cadena haremos una lista de nodos
    $cadena = "";
    //Para agregar una barra al final si es una carpeta
    $barra = "";
    //Este es el manejador del explorador
    $manejador = @dir($ruta);
    while ($recurso = $manejador->read()){
        //El recurso sera un archivo o una carpeta
        $nombre = "$ruta$recurso";
        if (@is_dir($nombre)) {//ES UNA CARPETA
            //Agregamos la barra al final
            $barra = "/";
            $cadena .= "CARPETA: ";
        } else {//ES UN ARCHIVO
            //No agregamos barra
            $barra = "";
            $cadena .= "ARCHIVO: ";
        }
        //Vemos si el recurso existe y se puede leer
        if (@is_readable($nombre)){
            if (@is_dir($nombre)) {
                 $cadena .= "<a href=\"".$_SERVER["PHP_SELF"]."?una-ruta=$nombre$barra\">$recurso$barra</a>";
            }else{
                // $nombreUrl = str_replace(" ", "%20", $nombre);
                // $cadena .= "<a href='file:///$nombreUrl' target='_blank'>$recurso$barra</a>";
                $cadena .= "<a href=\"".$_SERVER["PHP_SELF"]."?recur=$recurso\" target=\"_self\">$recurso$barra</a>";
            }

        } else {
            $cadena .= "$recurso$barra";
        }
        $cadena .= "<br />";
    }
    $manejador->close();
    return $cadena;
}

//Función para extraer el contenido de un archivo
function explora_archivo($ruta, $codif){
    //abrimos un buffer para leer el archivo
    ob_start();
    readfile($ruta);
    //volcamos el buffer en una variable
    $contenido = ob_get_contents();
    //limpiamos el buffer
    ob_clean();
    //retornamos el contenido después de limpiarlo
    //aplicando la codificación seleccionada
    return htmlentities($contenido, ENT_QUOTES, $codif);
}

?>



    <!--<h3>Opciones de configuración PHP (restringen explorador)</h3>-->
    <ul>
    <?php
    /*
        $opciones = "<li><a href=\"http://docs.php.net/manual/es/ini.sect.safe-mode.php#ini.safe-mode\">".
        "<code>safe_mode</code></a> ";
        if (ini_get("safe_mode")){
            $opciones .= ": activado";
        } else {
            $opciones .= ": desactivado";
        }
        $opciones .= "</li>".
        "<li><a href=\"http://docs.php.net/manual/es/ini.core.php#ini.open-basedir\">".
        "<code>open_basedir</code></a>: ".ini_get("open_basedir")."</li>".
        "<li><a href=\"http://docs.php.net/manual/es/function.getmyuid.php\">".
        "<code>getmyuid()</code></a>: ".getmyuid()."</li>".
        "<li><a href=\"http://docs.php.net/manual/es/function.getmygid.php\">".
        "<code>getmygid()</code></a>: ".getmygid()."</li>";
        echo $opciones;
        */
    ?>
    </ul>
    
    <div class="col-md-6">
        <h3>Directorio</h3>
        <form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="get">
            Ruta <small>(En Windows pueden usarse ambas barras "/" y "\")</small> <br />

            <textarea rows="5" cols="50" name="una-ruta">
                <?php echo $ruta; ?>
            </textarea> <br />
            
            Codificación para ver archivos:
            <select name="una-codificacion">
                <?php
                    foreach ($array_codif as $i=>$val){
                        echo "<option value=\"$val\"";
                        if ($codificacion == $val) echo " selected=\"selected\"";
                        echo ">$val</option>";
                    }
                ?>
            </select><br />
            <input type="submit" value="Recuperar..." />
        </form>
    </div>

    <div class="col-md-6">
        <h3>Explorador</h3>
        <?php
            echo "<br />$presenta_nodos";
            echo "<br />$presenta_archivo";
        ?>
    </div>

    <div class="col-md-12">
        
         <?php 
         if (!isset($recur)) {
             # code...
         }else{
        ?>
            <embed src="<?= base_url()."files/$recur" ?>" width="80%" height="600">
            <?php echo "Directorio:".base_url()."files/$recur";
        } 

        ?>

    </div>
     
    
<!-- =================================================================== -->
<!-- =================================================================== -->
