<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>Admin/editar_recurso/<?=$id?>">
  <fieldset>

  <!-- Form Name -->
  <div class="page-header">
    <h1><i class="fa fa-list"></i> Editar Recurso</h1>
  </div>

  <div class="col-md-6 hidden">
    <input id="proceso" name="proceso" type="text" placeholder="Nombre del recurso" class="form-control input-md" value="<?=$proceso_id_proceso?>">
    </div>

    <div class="col-md-6 hidden">
    <input id="grupo" name="grupo" type="text" placeholder="Nombre del recurso" class="form-control input-md" value="<?=$tipo_recurso_id_tipo_recurso?>">
    </div>

    <div class="col-md-6 hidden">
    <input id="status_actual" name="status_actual" type="text" placeholder="Nombre del recurso" class="form-control input-md" value="<?=$status?>">
    </div>

  <!-- Text input-->
  <div class="form-group">
    <div class="col-md-6">
    <label for="nom_recurso">Nombre del Recurso</label>  
    <input id="nom_recurso" name="nom_recurso" type="text" placeholder="Nombre del recurso" class="form-control input-md" value="<?=$recurso_nombre?>">
    </div>

    <div class="col-md-6">
    <label for="id_proceso">Proceso Padre</label>
      <select id="id_proceso" name="id_proceso" class="form-control">
        <option value="">Seleccione una Opción</option>
        <?php  foreach ($procesos->result() as $proceso): ?>
             <option value="<?= $proceso->id_proceso ?>"><?= $proceso->proceso_nombre ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-md-6">
    <label for="id_grupo_usuario">Grupo Principal</label>
      <select id="id_grupo_usuario" name="id_grupo_usuario" class="form-control">
        <option value="">Seleccione una Opción</option>
        <?php foreach ($grupos->result() as $grupo): ?>
             <option value="<?= $grupo->id_grupo_usuario ?>"><?= $grupo->grupo_usuario_nombre ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="col-md-6">
      <label for="">Seleccione el estado</label>
        <select id="status" name="status" class="form-control" required>
          <option value="" disabled="">Seleccione una Opción</option>
               <option value="Activo">Activo</option>
               <option value="Inactivo">Inactivo</option>
       </select>
    </div>
  </div>

  <!-- Button -->
  <div class="form-group">
  <br>
    <div class="col-md-12">
      <button id="btn_guardar_grupo" name="btn_guardar_grupo" class="btn btn-block btn-primary">Guardar</button>
    </div>
  </div>

  </fieldset>
</form>

<script>
jQuery(document).ready(function($) {
  var proceso = $("#proceso").val();
  var id_grupo_usuario = $("#grupo").val();
  var status = $("#status_actual").val();

  $('#id_proceso > option[value="'+proceso+'"]').attr('selected', 'selected');
  $('#id_grupo_usuario > option[value="'+id_grupo_usuario+'"]').attr('selected', 'selected');
  $('#status > option[value="'+status+'"]').attr('selected', 'selected');
});
</script>