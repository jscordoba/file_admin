<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($proceso)) {
    $proceso_nombre = '<i class="fa fa-ticket back"></i>Recursos de '. $this->session->userdata('usuario_nombre');;
}else{
    $proceso_nombre = '<a href="'.base_url("Admin/lista_proceso").'"><i class="fa fa-arrow-circle-o-left" aria-hidden="true" title="Regresar a la pagina anterior."></i></a>  Recursos del proceso '.$proceso->proceso_nombre;
}
?>
<!-- Form Name -->
  <div class="page-header">
    <h2 id="title_admin"> <?= $proceso_nombre ?></h2>
  </div>
<table id="table" class="table table-striped table-bordered ">
    <thead class="bg-black">
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">Nombre</th>
            <th class="text-center">Creación</th>
            <th class="text-center">Actualización</th>
            <!-- <th class="text-center">Estado</th> -->
            <th class="text-center"><i class="fa fa-cog"></i></th>
            <th class="text-center"><i class="fa fa-cog"></i></th>
            <th class="text-center"><i class="fa fa-cog"></i></th>
        </tr>
    </thead>
    <tbody>
         <?php 
        if ($recursos) {
            foreach ($recursos->result() as $recurso){?>
                 <tr>
                    <td class="text-center"><?= $recurso->id_recurso ?></td>
                    <td class="text-center"><a href="<?= base_url() ?>Admin/elementos_has_recurso/<?= $recurso->proceso_id_proceso ?>/<?= $recurso->id_recurso ?>"><?= $recurso->recurso_nombre ?></a></td>
                    <td class="text-center"><?= $recurso->recurso_creacion ?></td>
                    <td class="text-center"><?= $recurso->recurso_update ?></td>
                    <!-- <td class="text-center"><?= $recurso->status ?></td> -->
                    <td class="text-center">
                        <button class="btn btn-success btn-xs btn-asignarPermisosRec" data-toggle="modal" data-target="#m_accesoRecurso" data-recurso_id="<?=  $recurso->id_recurso ?>" title="Asignar grupo a este recurso"><i class="fa fa-check"></i></button>
                    </td>
                    <td class="text-center">
                         <a  class="btn btn-xs btn-primary" id="btn_editar_usuario" href="<?= base_url(); ?>Admin/consultar_recurso/<?= $recurso->id_recurso ?> " title="Editar"><i class="fa fa-pencil"></i></a>
                    </td>
                     <td class="text-center"><button class="btn btn-primary btn-xs" onclick="mostar_grupos_recurso(<?= $recurso->id_recurso ?>)" title="Mostrar grupos asignados a este recurso" data-toggle="modal" data-target="#modal-grupos_x_recurso"><i class="fa fa-group"></i></button></td>
                </tr>
            <?php }
        }
        ?>
    </tbody>
</table>
<br>
<!-- <a href="<?= base_url() ?>Admin/lista_proceso" class="btn btn-sm btn-success">Volver</a> -->


<!-- Modal ASIGNAR PERMISOS AL MODULO DE UN RECURSO -->
<div class="modal fade" id="m_accesoRecurso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary  text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">ASIGNAR PERMISOS A ESTE RECURSO</h3>
          </div>

           <form action="<?= base_url();?>admin/asignar_acceso/Resource" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                          <label for="grupo">Seleccione el grupo a asignar</label>
                            <select id="grupo" name="grupo" class="form-control">
                            </select>
                          </div>
                        </div>            

                    </div>

                </div>

                <input type="hidden" value="" name="componente" id="componente">

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="modal-grupos_x_recurso">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">GRUPOS ASIGNADOS</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                              <table  class="table table-bordered " >
                                <thead class="bg-black">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Nombre del grupo</th>
                                        <th class="text-center"><i class="fa fa-cog"></i></th>
                                    </tr>
                                </thead>
                               
                                <tbody id="tbody_grupos">
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function mostar_grupos_recurso(id) {
        var base_url = "<?php echo base_url(); ?>";

        $.post(''+base_url+'Main/mostar_grupos_recurso', {id: id }, function(data, textStatus, xhr) {
            if (data) {
                $("#tbody_grupos").text("");
                $("#tbody_grupos").append(data);
                // $("#modal-grupos_x_recurso").modal("show");
            }
        });
    }

    function eliminar_grupo_recurso(grupo,recurso){
        var base_url = "<?php echo base_url(); ?>";
        // alert(grupo + " - "+recurso);
        $.post(''+base_url+'Main/eliminar_grupo_recurso', {grupo: grupo, recurso: recurso }, function(data, textStatus, xhr) {
            if (data) {
                // $("#tbody_grupos").text("");
                // $("#tbody_grupos").append(data);
                $("#modal-grupos_x_recurso").modal("hide");
                setTimeout(function() {location.reload();}, 500);
            }else{
                alert("Error");
            }
        });
    }
</script>