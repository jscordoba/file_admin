<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <!-- File Input -->
<link href="<?= base_url()?>styles/template/css/fileinput.css" rel="stylesheet">
<div class="page-header">
  <h1><i class="fa fa-upload"></i> Subir Documento</h1>
</div>
<?php 
  if(isset($error)){
    echo $error;
  } 
?>

<form action="<?php echo base_url('Admin/do_upload') ?>" method="post" enctype="multipart/form-data">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="userfile">Archivo</label>
        <input type="file" id="userfile" class="file form-control" name="userfile" data-show-preview="false" required>
        <input type="text" name="userfile_name" id="userfile_name" class="form-control hidden">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="id_elemento">Elementos</label>
        <select id="id_elemento" name="id_elemento" class="form-control" required>
          <option value="" selected disabled>Seleccione una Opción</option>
          <?php  foreach ($elementos->result() as $elemento): ?>
               <option value="<?= $elemento->id_elemento ?>"><?= $elemento->elemento_nombre ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="recursos">Recursos</label>
        <select id="recursos" name="recursos" class="form-control" required>
        </select>
      </div>
    </div>
    <div class="col-md-6">
        <label for="inlineFormInput">Proceso del recurso</label>
        <input type="text" class="form-control hidden" id="proceso_id" name="proceso_id" placeholder="" required>
        <input type="text" class="form-control" id="proceso_name" name="proceso_name" placeholder="" disabled required>
    </div>
  </div>
  <div class="page-header">
    <h3 id="header"><i class='fa fa-cogs' ></i> No se a seleccionado ningun elemento</h3>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-hover table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Nombre Recurso</th>
              <th class="text-center">Nombre Proceso</th>
            </tr>
          </thead>
          <tbody id="tbody">
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <br>
  <input type="submit" value="Guardar" class="btn btn-block btn-primary">
  <br>
</form>

<script>

$("#id_elemento").change(function(event) {
  var base_url = "<?php echo base_url(); ?>";
  var id = $("#id_elemento").val();
  var name = $("#id_elemento option:selected").text();
  $("#header").text("");
  $("#header").append("<i class='fa fa-cogs' ></i> Procesos del elemento " + "<b>"+name+"</b>");
  $("#header").css('text-transform', 'lowercase');
  var parametro = {"id":id};
  $.ajax({
    url: ''+base_url+'Main/lista_procesos_has_elemento',
    type: 'post',
    data: parametro,
  })
  .done(function(data) {
    $("tbody").empty();
    $("tbody").append(data);
  })
  .fail(function(data_error) {
    console.log(data_error);
  });
 });

$("#id_elemento").change(function(event) {
  var base_url = "<?php echo base_url(); ?>";
  var id = $("#id_elemento").val();
  if ($.trim(id).length > 0) {
    var parametro = {"id":id};
    $.ajax({
      url: ''+base_url+'Main/seleccionar_recursos_x_elemento',
      type: 'post',
      data: parametro,
    })
    .done(function(data) {
      if (data != false) {
        $("#recursos").empty();
        $("#recursos").append('<option value="" selected disabled>Seleccione una Opción</option>');
        $("#recursos").append(data);
      }else{
        $("#recursos").empty();
        $("#recursos").append('<option value="" selected disabled>No se encontraron recursos</option>');
      }
    })
    .fail(function(data_error) {
      console.log(data_error);
    });
  }else{

  }
});

$("#recursos").change(function(event) {
  var base_url = "<?php echo base_url(); ?>";
  var id = $("#recursos").val();
  if ($.trim(id).length > 0) {
    var parametro = {"id":id};
    $.ajax({
      url: ''+base_url+'Main/seleccionar_proceso_x_recurso',
      type: 'post',
      data: parametro,
    })
    .done(function(data) {
      if (data != false) {
        var result = JSON.parse(data);
        $("#proceso_id").val(result.id_proceso);
        $("#proceso_name").val(result.proceso_nombre);
      }else{
        $("#proceso_id").val('#');
        $("#proceso_name").val('No se encontro ningun proceso');
      }
    })
    .fail(function(data_error) {
      console.log(data_error);
    });
  }else{

  }
});

document.getElementById('id_elemento').onchange = function () {
  // document.getElementById('userfile_name').value = document.getElementById('userfile').files[0].name;
  var name =  $("#id_elemento option:selected").text();
  var file = name.replace(/^.*\/|\.[^.]*$/g, '')
  document.getElementById('userfile_name').value = file;
}
</script>