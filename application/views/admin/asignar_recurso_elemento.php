<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="form-horizontal" method="POST" action="<?= base_url();?>admin/Assign_resource_element">
<fieldset>

  <!-- Form Name -->
  <div class="page-header">
    <h1><i class="fa fa-ticket"></i> Asignar elemento a un recurso</h1>
  </div>

  <!-- Text input-->
  <div class="form-group">
    <div class="col-md-6">
      <label  for="nom_elemento">Elemento</label>  
      <select id="id_elemento" name="id_elemento" class="form-control" required>
        <option value="" disabled selected>Seleccione una Opción</option>
        <?php  foreach ($elementos->result() as $elemento): ?>
             <option value="<?= $elemento->id_elemento ?>"><?= $elemento->elemento_nombre ?></option>
        <?php endforeach; ?>
      </select>
    </div>

     <div class="col-md-6">
      <label for="id_recurso">Recurso Padre</label>
      <select id="id_recurso" name="id_recurso" class="form-control" required>
        <option value="" disabled selected>Seleccione una Opción</option>
      </select>
    </div>

  </div>

  <!-- Button -->
  <div class="form-group">
  <br>
    <div class="col-md-12">
      <button id="btn_guardar_grupo" name="btn_guardar_grupo" class="btn btn-block btn-primary">Guardar</button>
    </div>
  </div>

</fieldset>
</form>


<script>
jQuery(document).ready(function($) {
  var g = $("#id_recurso").val();
  if (g == 0 || g == null) {
    $("#btn_guardar_grupo").attr('disabled', 'disabled');
  }
});
$("#id_recurso").change(function(event) {
  var g = $("#id_recurso").val();
  if (g == 0 || g == null) {
    $("#btn_guardar_grupo").attr('disabled', 'disabled');
  }else{
     $("#btn_guardar_grupo").removeAttr('disabled');
  }
});

$("#id_elemento").change(function(event) {
  var base_url = "<?php echo base_url(); ?>";
  var id = $("#id_elemento").val();
  if ($.trim(id).length > 0) {
    var parametro = {"id":id};
    $.ajax({
      url: ''+base_url+'Main/seleccionar_recursos_no_asignados',
      type: 'post',
      data: parametro,
    })
    .done(function(data) {
      // conso  le.log(data);
      if (data != false) {
        $("#id_recurso").empty();
        $("#id_recurso").append('<option value="" selected disabled>Seleccione una Opción</option>');
        $("#id_recurso").append(data);
      }else{
         $("#id_recurso").empty();
        $("#id_recurso").append('<option value="" selected disabled>No se encontraron recursos</option>');
      }
    })
    .fail(function(data_error) {
      console.log(data_error);
    });
  }else{

  }
});
</script>