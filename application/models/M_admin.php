<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
class M_admin extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

	}	


	// 
	// SELECTS de tablas principales
	// 

	public function sel_usuario($tipo_sel=NULL){
		if ($tipo_sel != NULL) {
			$this->db->where('perfil_usuario_id_perfil_usuario', 2);
		}
		$this->db->join('seccion_company', 'seccion_company.id_seccion_company = usuario.seccion_company_id_seccion_company', 'join');

		$this->db->join('oficina_company', 'oficina_company.id_oficina_company = usuario.oficina_company_id_oficina_company', 'join');

		$this->db->join('usuario_has_perfil', 'usuario_has_perfil.usuario_usuario_id = usuario.usuario_id', 'join');

		$this->db->join('perfil_usuario', 'perfil_usuario.id_perfil_usuario = usuario_has_perfil.perfil_usuario_id_perfil_usuario', 'left');
		$usuarios = $this->db->get('usuario');
		if ($usuarios->num_rows() > 0) {
			return $usuarios;
		}
		return null;
	}

	public function selecionar_usuarios(){
		$sql = "SELECT u.usuario_id,u.usuario_nombre,u.username,u.email,u.status,
		up.perfil_usuario_id_perfil_usuario,pu.perfil_usuario_nombre,s.seccion_company_nombre,o.oficina_company_nombre FROM usuario u JOIN usuario_has_perfil up ON up.usuario_usuario_id = u.usuario_id JOIN perfil_usuario pu ON pu.id_perfil_usuario = up.perfil_usuario_id_perfil_usuario JOIN seccion_company s ON s.id_seccion_company = u.seccion_company_id_seccion_company JOIN oficina_company o ON o.id_oficina_company = u.oficina_company_id_oficina_company";
		try {
				$usuarios = $this->db->query($sql);	
				if ($usuarios->num_rows() > 0) {
					return $usuarios;
				}else{
					return null;
				}
			} catch (Exception $e) {
				return null;
			}
	}

	public function sel_usuarioGroup($grupo=NULL){
		$sql = "SELECT 	us.usuario_id,
				        us.usuario_nombre,
				        ug.usuario_usuario_id,
				        ug.grupo_usuario_id_grupo_usuario,
				        us.seccion_company_id_seccion_company,
				        sc.seccion_company_nombre,
				        us.oficina_company_id_oficina_company,
				        oc.oficina_company_nombre,
				        up.perfil_usuario_id_perfil_usuario
				FROM grupo_usuario gr
				JOIN usuario_has_grupo ug ON ug.grupo_usuario_id_grupo_usuario=gr.id_grupo_usuario
				RIGHT JOIN usuario us ON us.usuario_id=ug.usuario_usuario_id
				JOIN seccion_company sc ON sc.id_seccion_company = us.seccion_company_id_seccion_company
				JOIN oficina_company oc ON oc.id_oficina_company = us.oficina_company_id_oficina_company
				JOIN usuario_has_perfil up ON up.usuario_usuario_id = us.usuario_id AND up.perfil_usuario_id_perfil_usuario = 3
				WHERE us.usuario_id NOT IN (SELECT u.usuario_id
				FROM usuario u
				JOIN usuario_has_grupo ug ON ug.usuario_usuario_id=u.usuario_id
				WHERE ug.grupo_usuario_id_grupo_usuario = '$grupo')
				GROUP BY us.usuario_id"; 
		try {
			$usuarios = $this->db->query($sql);	
			if ($usuarios->num_rows() > 0) {
				return $usuarios;
			}else{
				return null;
			}
		} catch (Exception $e) {
			return null;
		}
	}

	public function sel_usuarioGroup1($grupo=NULL){
		$sql = "SELECT 	us.usuario_id,
				        us.usuario_nombre,
				        ug.usuario_usuario_id,
				        ug.grupo_usuario_id_grupo_usuario,
				        us.seccion_company_id_seccion_company,
				        sc.seccion_company_nombre,
				        us.oficina_company_id_oficina_company,
				        oc.oficina_company_nombre,
				        up.perfil_usuario_id_perfil_usuario
				FROM grupo_usuario gr
				JOIN usuario_has_grupo ug ON ug.grupo_usuario_id_grupo_usuario=gr.id_grupo_usuario
				RIGHT JOIN usuario us ON us.usuario_id=ug.usuario_usuario_id
				JOIN seccion_company sc ON sc.id_seccion_company = us.seccion_company_id_seccion_company
				JOIN oficina_company oc ON oc.id_oficina_company = us.oficina_company_id_oficina_company
				JOIN usuario_has_perfil up ON up.usuario_usuario_id = us.usuario_id AND up.perfil_usuario_id_perfil_usuario = 3
				WHERE us.usuario_id NOT IN (SELECT usuario_usuario_id
				FROM usuario_has_grupo)"; 
		try {
			$usuarios = $this->db->query($sql);	
			if ($usuarios->num_rows() > 0) {
				return $usuarios;
			}else{
				return null;
			}
		} catch (Exception $e) {
			return null;
		}
	}

	public function sel_proceso($tipo_sel=NULL){
		$usuario_id = $this->session->userdata('usuario_id');
		if ($tipo_sel != NULL) {
			// $this->db->join('Table', 'table.column = table.column', 'left');
			if ($tipo_sel!="responsable") {
				$this->db->where('id_proceso', $tipo_sel);
				$procesos = $this->db->get('proceso');
			}else{
				$this->db->where('usuario_id_responsable', $usuario_id);

				$sql = "SELECT 	p.*, sc.seccion_company_nombre,oc.oficina_company_nombre
						FROM proceso p 
						JOIN seccion_company sc ON p.seccion_company_id_seccion_company = sc.id_seccion_company
						JOIN oficina_company oc ON p.oficina_company_id_oficina_company = oc.id_oficina_company
						WHERE p.usuario_id_responsable = '$usuario_id'";
				$procesos = $this->db->query($sql);	
			}
		}
		
		if ($procesos->num_rows() > 0) {
			return $procesos;
		}
		return null;
	}

	public function seleccionar_procesos(){
		$sql = "SELECT p.id_proceso,
		p.proceso_nombre,
		p.proceso_creacion,
		p.proceso_update,
		p.usuario_id_creacion,
		p.usuario_id_responsable,
		u.usuario_nombre,
		p.status,s.seccion_company_nombre,
		o.oficina_company_nombre 
		FROM proceso p 
		JOIN seccion_company s ON s.id_seccion_company = p.seccion_company_id_seccion_company 
		JOIN oficina_company o ON o.id_oficina_company = p.oficina_company_id_oficina_company
		JOIN usuario u ON u.usuario_id = p.usuario_id_responsable";
		try {
				$procesos = $this->db->query($sql);	
				if ($procesos->num_rows() > 0) {
					return $procesos;
				}else{
					return null;
				}
			} catch (Exception $e) {
				return null;
			}
	}

	public function sel_proceso_user(){
		$usuario_id = $this->session->userdata('usuario_id');

		$this->db->join('usuario_has_grupo', 'usuario_has_grupo.grupo_usuario_id_grupo_usuario = acceso.grupo_usuario_id_grupo_usuario', 'join');
		$this->db->join('usuario', 'usuario.usuario_id = usuario_has_grupo.usuario_usuario_id', 'join');
		$this->db->join('proceso', 'proceso.id_proceso = acceso.recurso_id_recurso', 'join');
		$this->db->join('oficina_company', 'oficina_company.id_oficina_company = proceso.oficina_company_id_oficina_company', 'join');
		$this->db->join('seccion_company', 'seccion_company.id_seccion_company = proceso.seccion_company_id_seccion_company', 'join');
		$this->db->select('proceso.id_proceso,proceso.proceso_nombre,proceso.usuario_id_responsable,seccion_company.seccion_company_nombre,oficina_company.oficina_company_nombre');
		$this->db->where('usuario.usuario_id', $usuario_id);
		$procesos = $this->db->get('acceso');
		if ($procesos->num_rows() > 0) {
			return $procesos;
		}
		return null;
	}

	public function sel_recurso($tipo_sel=NULL,$tipo_busqueda=NULL){
		$usuario_id = $this->session->userdata('usuario_id');
		if ($tipo_sel != NULL && $tipo_sel!="responsable") {
			if ($tipo_busqueda=="p") {
				$this->db->where('proceso.id_proceso', $tipo_sel);
			}elseif ($tipo_busqueda=="r") {
				$this->db->where('recurso.id_recurso', $tipo_sel);
			}
		}
		$this->db->join('proceso', 'proceso.id_proceso = recurso.proceso_id_proceso', 'join');
		$this->db->where('usuario_id_responsable', $usuario_id);
		$recurso = $this->db->get('recurso');
		if ($recurso->num_rows() > 0) {
			return $recurso;
		}
		return null;
	}

	public function sel_elementos(){
		$usuario_id = $this->session->userdata('usuario_id');
		$result = $this->db->get('elemento');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return null;
		}
	}

	public function sel_elementos_usuarios(){
		$id = $this->session->userdata('usuario_id');
		$this->db->where('usuario_id_creacion',$id);
		$result = $this->db->get('elemento');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return null;
		}
	}

	public function sel_elemento($id_recurso=NULL){
		$usuario_id = $this->session->userdata('usuario_id');

		if ($id_recurso != NULL && $id_recurso !="todo") {
			$this->db->where('recurso.id_recurso', $id_recurso);
		}else {
			$this->db->group_by('recurso_has_elemento.elemento_id_elemento');
		}
		$this->db->join('recurso_has_elemento', 'recurso_has_elemento.elemento_id_elemento = elemento.id_elemento', 'join');
		$this->db->join('recurso', 'recurso.id_recurso = recurso_has_elemento.recurso_id_recurso', 'join');
		$this->db->join('proceso', 'proceso.id_proceso = recurso.proceso_id_proceso', 'join');
		$this->db->where('usuario_id_responsable', $usuario_id);
		
		$recurso = $this->db->get('elemento');
		if ($recurso->num_rows() > 0) {
			return $recurso;
		}
		return null;
	}

	public function sel_elemento_new($recurso = NULL){
		// $usuario_id = $this->session->userdata('usuario_id');
		$sql = "SELECT DISTINCT e.*
				FROM elemento e
				JOIN recurso_has_elemento re ON re.elemento_id_elemento = e.id_elemento
				JOIN recurso r ON r.id_recurso = re.recurso_id_recurso 
				JOIN proceso p ON p.id_proceso = r.proceso_id_proceso
				WHERE r.id_recurso = '$recurso'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return null;
		}
	}

	public function sel_seccion(){
		$secciones = $this->db->get('seccion_company');
		if ($secciones->num_rows() > 0) {
			return $secciones;
		}
		return null;
	}

	public function sel_oficina(){
		$oficinas = $this->db->get('oficina_company');
		if ($oficinas->num_rows() > 0) {
			return $oficinas;
		}
		return null;
	}

	public function sel_rol(){
		$roles = $this->db->get('perfil_usuario');
		if ($roles->num_rows() > 0) {
			return $roles;
		}
		return null;
	}

	public function sel_grupo(){
		$grupos = $this->db->get('grupo_usuario');
		if ($grupos->num_rows() > 0) {
			return $grupos;
		}else{
			return false;
		}
	}

	public function sel_grupo1($componente, $tipo){
		$sql = "SELECT * FROM grupo_usuario WHERE id_grupo_usuario
				NOT IN (SELECT grupo FROM accesos WHERE componente = $componente AND tipo = $tipo)";
		$grupos = $this->db->query($sql);
		if ($grupos->num_rows() > 0) {
			return $grupos;
		}else{
			return false;
		}
	}


	public function sel_usuariosXGroup($grupo){
		$sql = "SELECT gu.id_grupo_usuario,gu.grupo_usuario_nombre,u.usuario_id,u.usuario_nombre,u.email,sc.seccion_company_nombre,oc.oficina_company_nombre
				FROM grupo_usuario gu
					JOIN usuario_has_grupo ug ON (gu.id_grupo_usuario=ug.grupo_usuario_id_grupo_usuario)
					JOIN usuario u ON (u.usuario_id=ug.usuario_usuario_id)
					JOIN seccion_company sc ON (sc.id_seccion_company=u.seccion_company_id_seccion_company)
					JOIN oficina_company oc ON (OC.id_oficina_company=U.oficina_company_id_oficina_company)
				WHERE gu.id_grupo_usuario=$grupo";
		$gruposUsuarios = $this->db->query($sql);
		if ($gruposUsuarios->num_rows() > 0) {
			foreach ($gruposUsuarios->result() as $usuarios) {
				$usuariosXgrupo = array(
								'grupo_usuario_nombre' => $usuarios->grupo_usuario_nombre,
								'id_grupo_usuario' => $usuarios->id_grupo_usuario,
								'usuario_id' => $usuarios->usuario_id,
								'usuario_nombre' => $usuarios->usuario_nombre,
								'email' => $usuarios->email,
								'seccion_company_nombre' => $usuarios->seccion_company_nombre,
								'oficina_company_nombre' => $usuarios->oficina_company_nombre 
								);
			}
			return $usuariosXgrupo;
		}
	}

	// 
	// INSERTS de tablas principales
	// 

	public function ins_usuario(){
		# code...
		$id = $this->input->post('id');
		$nombre_user = $this->input->post('nombre_user');
		$user = $this->input->post('user');
		$password = $this->input->post('password');
		$email = $this->input->post('email');
		$id_seccion = $this->input->post('id_seccion');
		$id_oficina = $this->input->post('id_oficina');
		$id_perfil = $this->input->post('id_perfil');
		$status = $this->input->post('status_user');

		$object_user = array(
						'usuario_id' => $id, 
						'usuario_nombre' => $nombre_user, 
						'username' => $user,
						'password' => $password,
						'email' => $email,
						'seccion_company_id_seccion_company' => $id_seccion,
						'oficina_company_id_oficina_company' => $id_oficina,
						'status' => $status,
						);

		$object_perfil = array(
							'usuario_usuario_id' => $id,
							'perfil_usuario_id_perfil_usuario' =>  $id_perfil
							);


		try {
			$this->db->insert('usuario', $object_user);

			try {
					$this->db->insert('usuario_has_perfil', $object_perfil);
				} catch (Exception $e) {
					return "Error al ingresar el perfil del Usuario. Error: ".$e;
				}

			return "ok";
		} catch (Exception $e) {
			return "Error al ingresar el nuevo Usuario. Error: ".$e;
		}	
	}

	public function ins_proceso(){
		$nom_proceso = $this->input->post('nom_proceso');
		$id_responsable = $this->input->post('id_responsable'); //"1002593980";
		$id_seccion = $this->input->post('id_seccion'); //"2"; 
		$id_oficina = $this->input->post('id_oficina'); //"1"; 
		$id_usuario_registro = $this->session->userdata('usuario_id');
		$status = $this->input->post('status');

		$object = array(
						'proceso_nombre' => $nom_proceso,
						'usuario_id_creacion' => $id_usuario_registro,
						'usuario_id_responsable' => $id_responsable,
						'seccion_company_id_seccion_company' => $id_seccion,
						'oficina_company_id_oficina_company' => $id_oficina,
						'status' => $status
						);

		try {
			$this->db->insert('proceso', $object);
			return "ok";
		} catch (Exception $e) {
			return "Error al insertar el Nuevo Proceso. Error: ".$e;
		}
	}

	public function ins_seccion(){
		$nom_seccion = $this->input->post('nom_seccion');
		$status = $this->input->post('status');

		$object = array('seccion_company_nombre' => $nom_seccion,'status' => $status );

		try {
			$this->db->insert('seccion_company', $object);
			return "ok";
		} catch (Exception $e) {
			return "Error al insertar la nueva Sección. Error: ".$e;
		}
	}

	public function ins_oficina(){
		$nom_oficina = $this->input->post('nom_oficina');
		$status = $this->input->post('status');

		$object = array('oficina_company_nombre' => $nom_oficina ,'status' => $status );

		try {
			$this->db->insert('oficina_company', $object);
			return "ok";
		} catch (Exception $e) {
			return "Error al insertar la nueva Oficina. Error: ".$e;
		}
	}

	public function ins_grupo(){
		$nom_grupo = $this->input->post('nom_grupo');
		$status = $this->input->post('status');

		$object = array('grupo_usuario_nombre' => $nom_grupo ,'status' => $status );

		try {
			$this->db->insert('grupo_usuario', $object);
			return "ok";

			redirect(base_url().'Sadmin/lista_grupo','refresh');
		} catch (Exception $e) {
			return "Error al insertar la nueva Grupo. Error: ".$e;
		}
	}

	public function add_grupousuarios(){
		$chk_grp = $this->input->post('chk_addGroup');

	    foreach($chk_grp as $chk_grupo => $usuarios)
	    {
	        echo "Grupo_ID: ".$chk_grupo;
	        foreach ($usuarios as $user => $chk)
	        {
	            echo "Usuario_ID: ".$user;
	            echo "Valor chk: ".$chk;
	            echo "-----<br>";

	            $object_addUserGroup = array(
	            							'usuario_usuario_id' => $user,
	            							'grupo_usuario_id_grupo_usuario' => $chk_grupo
	            							 );

	            print_r($object_addUserGroup);
           		try {
           			$this->db->insert('usuario_has_grupo', $object_addUserGroup);
           		} catch (Exception $e) {
           			return "Error al actualizar el grupo. Error: ".$e;
           		}
	        }
	    }

	    return "ok";

	    

	    // try {
     //    	$this->db->insert('usuario_has_grupo', $object_addUserGroup);
        	
     //    	return "ok";
     //    } catch (Exception $e) {
     //    	return "Error al actualizar el grupo. Error: ".$e;
     //    }
	}

	public function ins_permiso($value=null){
		$id_grupo_usuario = $this->input->post('grupo_id');
		$id_recurso = $this->input->post('id_recurso');
		$id_tipo_recurso = $this->input->post('id_tipo_recurso');
		$object_acceso = array(
								'grupo_usuario_id_grupo_usuario' => $id_grupo_usuario,
								'recurso_id_recurso' => $id_recurso,
								'tipo_recurso_id_tipo_recurso' => $id_tipo_recurso
							); 

		try {
			$this->db->insert('acceso', $object_acceso);

			if ($id_tipo_recurso=="3") { //PROCESO
				$this->db->join('recurso', 'recurso.proceso_id_proceso = proceso.id_proceso', 'join');
				$this->db->select('id_recurso, tipo_recurso_id_tipo_recurso');
				$recursos_heredados = $this->db->get('proceso');
				if ($recursos_heredados) {
					$control_elemento = "Bandera";

					foreach ($recursos_heredados->result() as $recursos) { // insertar recursos del grupo heredados del proceso
						$object_acceso_rec = array(
													'grupo_usuario_id_grupo_usuario' => $id_grupo_usuario,
													'recurso_id_recurso' => $recursos->id_recurso,
													'tipo_recurso_id_tipo_recurso' => $recursos->tipo_recurso_id_tipo_recurso
												);

						$this->db->insert('acceso', $object_acceso_rec);
						$this->db->join('recurso_has_elemento', 'recurso_has_elemento.elemento_id_elemento = elemento.id_elemento', 'join');
						$this->db->select('id_elemento, tipo_recurso_id_tipo_recurso');
						$this->db->where('recurso_id_recurso', $recursos->id_recurso);
						$elementos_heredados = $this->db->get('elemento');
						if ($elementos_heredados) {
							foreach ($elementos_heredados->result() as $elementos) { // insertar elementos del grupo heredados del recurso
								$object_acceso_el = array(
															'grupo_usuario_id_grupo_usuario' => $id_grupo_usuario,
															'recurso_id_recurso' => $elementos->id_elemento,
															'tipo_recurso_id_tipo_recurso' => $elementos->tipo_recurso_id_tipo_recurso
														);
								if ($control_elemento <> $elementos->id_elemento) {
									$this->db->insert('acceso', $object_acceso_el);
									$control_elemento = $elementos->id_elemento; // Validador Bandera Control_recurso
								}

							}
						}
					}
				}
			}elseif($id_tipo_recurso=="1"){//RECURSO
				$control_elemento = "Bandera";
				$this->db->join('recurso_has_elemento', 'recurso_has_elemento.elemento_id_elemento = elemento.id_elemento', 'join');
				$this->db->select('id_elemento, tipo_recurso_id_tipo_recurso');
				$this->db->where('recurso_id_recurso', $id_recurso);
				$elementos_heredados = $this->db->get('elemento');
				if ($elementos_heredados) {
					foreach ($elementos_heredados->result() as $elementos) { // insertar elementos del grupo heredados del recurso
						$object_acceso_el = array(
													'grupo_usuario_id_grupo_usuario' => $id_grupo_usuario,
													'recurso_id_recurso' => $elementos->id_elemento,
													'tipo_recurso_id_tipo_recurso' => $elementos->tipo_recurso_id_tipo_recurso
												);
						if ($control_elemento <> $elementos->id_elemento) {
							$this->db->insert('acceso', $object_acceso_el);
							$control_elemento = $elementos->id_elemento; // Validador Bandera Control_recurso
						}
					}
				}
			}
			echo "ok";
		} catch (Exception $e) {
			echo "Error al actualizar permisos. ".$e;
		}
	}

	public function insertar_permiso($tipo){

		if ($tipo === "Process") {
			try {
				$grupo = $this->input->post('grupo');
				$componente = $this->input->post('componente');
				$tipo = 3;
				$data = array('grupo'=>$grupo,'componente'=>$componente,'tipo'=>$tipo);
				$this->db->insert('accesos', $data);
				return true;
			} catch (Exception $e) {
				return $e;
			}
		}else if ($tipo === "Element") {
			try {
				$grupo = $this->input->post('grupo');
				$componente = $this->input->post('componente');
				$tipo = 2;
				$data = array('grupo'=>$grupo,'componente'=>$componente,'tipo'=>$tipo);
				$this->db->insert('accesos', $data);
				return true;
			} catch (Exception $e) {
				return $e;
			}
		}else if($tipo === "Resource"){
			try {
				$grupo = $this->input->post('grupo');
				$componente = $this->input->post('componente');
				$tipo = 1;
				$data = array('grupo'=>$grupo,'componente'=>$componente,'tipo'=>$tipo);
				$this->db->insert('accesos', $data);
				return true;
			} catch (Exception $e) {
				return $e;
			}
		}
	}
	// 
	// UPDATES de tablas principales
	// 
// ============================================================================
	public function obtenerId($id){
		$this->db->where('usuario_id', $id);
		$query = $this->db->get('usuario');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function obtenerPerfil($id){
		$this->db->where('usuario_usuario_id', $id);
		$query = $this->db->get('usuario_has_perfil');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function upd_usuario ($id, $data, $data1){
		try {
			$this->db->where('usuario_id',$id);
			$this->db->update('usuario',$data);
			try {
				$this->db->where('usuario_usuario_id',$id);
				$this->db->update('usuario_has_perfil',$data1);
				} catch (Exception $e) {
					return $e;
				}

			return "ok";
		} catch (Exception $e) {
			return $e;
		}
	}

	public function upd_status_usuario($id){
		try {
		$this->db->where('usuario_id', $id);
		$query = $this->db->get('usuario');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$status_actual = $row->status;
			}
			if ($status_actual == "Activo") {
				$data = array('status'=>'Inactivo');
				try {
					$this->db->where('usuario_id',$id);
					$this->db->update('usuario',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}else{
				$data = array('status'=>'Activo');
				try {
					$this->db->where('usuario_id',$id);
					$this->db->update('usuario',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}
		}else{
			return false;
		}
		} catch (Exception $e) {
			return $e;
		}
	}

	// ============================================================================
	public function obtenerProceso($id){
		$this->db->where('id_proceso', $id);
		$query = $this->db->get('proceso');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function upd_proceso ($id, $data){
		try {
			$this->db->where('id_proceso',$id);
			$this->db->update('proceso',$data);
		} catch (Exception $e) {
			return $e;
		}
	}
	public function upd_status_proceso($id){
		try {
		$this->db->where('id_proceso', $id);
		$query = $this->db->get('proceso');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$status_actual = $row->status;
			}
			if ($status_actual == "Activo") {
				$data = array('status'=>'Inactivo');
				try {
					$this->db->where('id_proceso',$id);
					$this->db->update('proceso',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}else{
				$data = array('status'=>'Activo');
				try {
					$this->db->where('id_proceso',$id);
					$this->db->update('proceso',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}
		}else{
			return false;
		}
		} catch (Exception $e) {
			return $e;
		}
	}
	// ============================================================================
	public function obtenerSeccion($id){
		$this->db->where('id_seccion_company', $id);
		$query = $this->db->get('seccion_company');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function upd_seccion ($id, $data){
		try {
			$this->db->where('id_seccion_company',$id);
			$this->db->update('seccion_company',$data);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function upd_status_seccion($id){
		try {
		$this->db->where('id_seccion_company', $id);
		$query = $this->db->get('seccion_company');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$status_actual = $row->status;
			}
			if ($status_actual == "Activo") {
				$data = array('status'=>'Inactivo');
				try {
					$this->db->where('id_seccion_company',$id);
					$this->db->update('seccion_company',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}else{
				$data = array('status'=>'Activo');
				try {
					$this->db->where('id_seccion_company',$id);
					$this->db->update('seccion_company',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}
		}else{
			return false;
		}
		} catch (Exception $e) {
			return $e;
		}
	}
	// ============================================================================
	public function obtenerOficina($id){
		$this->db->where('id_oficina_company', $id);
		$query = $this->db->get('oficina_company');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function upd_oficina($id, $data){
		try {
			$this->db->where('id_oficina_company',$id);
			$this->db->update('oficina_company',$data);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function upd_status_oficina($id){
		try {
		$this->db->where('id_oficina_company', $id);
		$query = $this->db->get('oficina_company');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$status_actual = $row->status;
			}
			if ($status_actual == "Activo") {
				$data = array('status'=>'Inactivo');
				try {
					$this->db->where('id_oficina_company',$id);
					$this->db->update('oficina_company',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}else{
				$data = array('status'=>'Activo');
				try {
					$this->db->where('id_oficina_company',$id);
					$this->db->update('oficina_company',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}
		}else{
			return false;
		}
		} catch (Exception $e) {
			return $e;
		}
	}
	// ============================================================================
	public function obtenerGrupo($id){
		$this->db->where('id_grupo_usuario', $id);
		$query = $this->db->get('grupo_usuario');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function upd_grupo($id, $data){
		try {
			$this->db->where('id_grupo_usuario',$id);
			$this->db->update('grupo_usuario',$data);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function upd_status_grupo($id){
		try {
		$this->db->where('id_grupo_usuario', $id);
		$query = $this->db->get('grupo_usuario');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$status_actual = $row->status;
			}
			if ($status_actual == "Activo") {
				$data = array('status'=>'Inactivo');
				try {
					$this->db->where('id_grupo_usuario',$id);
					$this->db->update('grupo_usuario',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}else{
				$data = array('status'=>'Activo');
				try {
					$this->db->where('id_grupo_usuario',$id);
					$this->db->update('grupo_usuario',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}
		}else{
			return false;
		}
		} catch (Exception $e) {
			return $e;
		}
	}
	// ============================================================================
	// 

	// INSERTS De tablas de sesion ADMIN
	//

	public function ins_recurso(){
		$nom_recurso = $this->input->post('nom_recurso');
		$id_proceso = $this->input->post('id_proceso');
		// $id_tipo_recurso = 1;
		$id_grupo_usuario = $this->input->post('id_grupo_usuario');
		$usuario_creacion = $this->session->userdata('usuario_id');
		$recurso_update = date('Y-m-d H:i:s');
		$status = $this->input->post('status');

		$object_recurso = array(
								'recurso_nombre' => $nom_recurso,
								'usuario_id_creacion' => $usuario_creacion,
								'proceso_id_proceso' => $id_proceso,
								'tipo_recurso_id_tipo_recurso' => $id_grupo_usuario,
								'recurso_update' => $recurso_update,
								'status' => $status
								);

		try {
			$this->db->insert('recurso', $object_recurso);

			return "ok";
		} catch (Exception $e) {
			return $e;
		}
	}

	public function ins_elemento($tipo){
		$id_recurso = $this->input->post('id_recurso');
		if ($tipo!="asignar" && $tipo=="crear") {
			$nom_elemento = $this->input->post('nom_elemento');
			$id_tipo_recurso = 2;
			$id_grupo_usuario = $this->input->post('id_grupo_usuario');//para asignar un grupo
			$usuario_creacion = $this->session->userdata('usuario_id');
			$status = $this->input->post('status');
			$elemento_update = date('Y-m-d H:i:s');
			$elemento = array(
									'elemento_nombre' => $nom_elemento,
									'usuario_id_creacion' => $usuario_creacion,
									'tipo_recurso_id_tipo_recurso' => $id_tipo_recurso,
									'elemento_update' => $elemento_update,
									'status' => $status
									);

			try {
				$this->db->insert('elemento', $elemento);
				return "ok";
			} catch (Exception $e) {
				return $e;
			}
			$id_elemento = $this->db->insert_id();
		}else{
			$id_elemento = $this->input->post('id_elemento');
			$grupo = $this->input->post('id_grupo_usuario');
			$recurso_has_elemento = array('recurso_id_recurso' => $id_recurso,'elemento_id_elemento' =>  $id_elemento,'grupo_id_grupo'=>$grupo);
			try {
				$this->db->insert('recurso_has_elemento', $recurso_has_elemento);
				try {
					$accesos = array('grupo'=>$grupo,'componente'=>$id_elemento,'tipo'=>'2');
					$this->db->insert('accesos', $accesos);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			} catch (Exception $e) {
				return $e;
			}
		}
	}

	public function sel_version ($index=NULL){
		$usuario_id = $this->session->userdata('usuario_id');

		if ($index != NULL) {
			// $this->db->join('recurso_has_elemento', 'recurso_has_elemento.elemento_id_elemento = elemento.id_elemento', 'join');
			// $this->db->join('recurso', 'recurso.id_recurso = recurso_has_elemento.recurso_id_recurso', 'join');
			// $this->db->join('proceso', 'proceso.id_proceso = recurso.proceso_id_proceso', 'join');
			$this->db->where('id_recurso_has_elemento', $index);
		}

		$document_version = $this->db->get('document_version');
		
		if ($document_version->num_rows() > 0) {
			# code...
			return $document_version;
		}

		// print_r($document_version);

		return null;
	}

	public function sel_version_user ($index=NULL){
		$usuario_id = $this->session->userdata('usuario_id');

		if ($index != NULL) {
			// $this->db->join('recurso_has_elemento', 'recurso_has_elemento.elemento_id_elemento = elemento.id_elemento', 'join');
			// $this->db->join('recurso', 'recurso.id_recurso = recurso_has_elemento.recurso_id_recurso', 'join');
			// $this->db->join('proceso', 'proceso.id_proceso = recurso.proceso_id_proceso', 'join');
			$this->db->where('id_recurso_has_elemento', $index);
		}

		$this->db->where('document_version_visible', '1');

		$document_version = $this->db->get('document_version');
		
		if ($document_version->num_rows() > 0) {
			# code...
			return $document_version;
		}

		// print_r($document_version);

		return null;
	}

	public function ins_version(){
		$usuario_id = $this->session->userdata('usuario_id');
		$id_recurso_has_elemento = $this->input->post('id_recurso_has_elemento');
		$g_visible = $this->input->post('g_visible');
		$archivo_path = $_FILES["upl_version"]["tmp_name"]; 
		$tamanio = $_FILES["upl_version"]["size"];
		$tipo    = $_FILES["upl_version"]["type"];
		$explode_tmp_name = explode('.',$_FILES["upl_version"]["name"]);
		$archivo_name = array_shift($explode_tmp_name);

		// $this->db->join('recurso_has_elemento', 'recurso_has_elemento.elemento_id_elemento = document_version.elemento_id_elemento', 'JOIN');
		// $this->db->join('elemento', 'elemento.id_elemento = recurso_has_elemento.elemento_id_elemento', 'JOIN');
		// $this->db->select('document_version.version_nombre');
		// $this->db->select_max('document_version.version_num');
		// $this->db->where('document_version.elemento_id_elemento', $elemento_id);
		// $this->db->get('document_version');
		
		$query = "SELECT MAX(dv.version_num) as version_num, e.elemento_nombre as version_nombre
					FROM document_version dv
					JOIN recurso_has_elemento re ON re.id_recurso_has_elemento = dv.id_recurso_has_elemento
					JOIN elemento e ON e.id_elemento = re.elemento_id_elemento
					WHERE dv.id_recurso_has_elemento =  ".$id_recurso_has_elemento." ";

		// echo $archivo_name;

		try {
			if ($result = $this->db->query($query)) {
				$row = $result->row();
				$version_num = $row->version_num + 1;
				$nom_elemento_version = $archivo_name."_v".$version_num;
			}else{
				$version_num = 1;
				$nom_elemento_version = $archivo_name."_v".$version_num;
			}
		} catch (Exception $e) {
			echo $e;
		}

		if ( $archivo_path != "none" )
		{
		    $fp = fopen($archivo_path, "rb");
		    $contenido = fread($fp, $tamanio);//$contenido = fread($fp, $tamanio);
		    // $contenido = addslashes($contenido);
		    fclose($fp); 
		}

		$object_document = array(
								'document_document' => $contenido,
								'document_tipo' => $tipo
								);

		try {
			$this->db->insert('document', $object_document);
			$id_document = $this->db->insert_id();
			$object_document_version = array(
										'version_num' => $version_num,
										'version_nombre' => $nom_elemento_version,
										'usuario_id_creacion' => $usuario_id,
										'id_recurso_has_elemento' => $id_recurso_has_elemento,
										'document_version_visible' => $g_visible,
										'document_id_document' => $id_document
										);

			$this->db->insert('document_version', $object_document_version);

			if ($g_visible==1) {
				$id_document_version = $this->db->insert_id();
				$res_validar = $this->validar_version($id_recurso_has_elemento,$id_document_version);
				if ($res_validar=="ok") {
					redirect(base_url().'Admin/lista_version/','refresh');
				}else{
					echo "Error al Validar Versión. Error: ".$res_validar;
				}
			}
		} catch (Exception $e) {
			return $e;
		}
		redirect(base_url().'Admin/lista_version/','refresh');
	}


	public function obt_document($document_id_document){
		$this->db->where('id_document', $document_id_document);
		$documento = $this->db->get('document');
		
		if ($documento->num_rows() > 0) {
			# code...
			return $documento;
		}

		print_r($documento);

		return null;
	}

	public function validar_version($id_recurso_has_elemento,$id_document_version){
		$object_document_version_active = array('document_version_visible' => 2 );
		
		$this->db->where('id_version !=', $id_document_version);
		$this->db->where('id_recurso_has_elemento', $id_recurso_has_elemento);
		
		try {
			$result_update = $this->db->update('document_version', $object_document_version_active);

			if ($result_update) {
				# code...
				// echo "Documento ID: $id_recurso_has_elemento";
				// echo "Recorrer todas las versiones del componente y solo dejar el id_document seleccionado";

				return "ok";
			}
			
		} catch (Exception $e) {
			return $e;
		}	
	}

	// ============================================================================
	// 


	public function obtenerRecurso($id){
		$this->db->where('id_recurso', $id);
		$query = $this->db->get('recurso');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function upd_recurso($id, $data){
		try {
			$this->db->where('id_recurso',$id);
			$this->db->update('recurso',$data);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function obtenerElemento($id){
		$this->db->where('id_elemento', $id);
		$query = $this->db->get('elemento');
		if ($query->num_rows() > 0) {
			return $query;
		}else{
			return false;
		}
	}

	public function upd_elemento($id, $data){
		try {
			$this->db->where('id_elemento',$id);
			$this->db->update('elemento',$data);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function upd_status_elemento($id){
		try {
		$this->db->where('id_elemento', $id);
		$query = $this->db->get('elemento');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$status_actual = $row->status;
			}
			if ($status_actual == "Activo") {
				$data = array('status'=>'Inactivo');
				try {
					$this->db->where('id_elemento',$id);
					$this->db->update('elemento',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}else{
				$data = array('status'=>'Activo');
				try {
					$this->db->where('id_elemento',$id);
					$this->db->update('elemento',$data);
					return "ok";
				} catch (Exception $e) {
					return $e;
				}
			}
		}else{
			return false;
		}
		} catch (Exception $e) {
			return $e;
		}
	}

	public function usuario_has_grupo($id){
		try {
			$this->db->where('grupo_usuario_id_grupo_usuario', $id);
			$query = $this->db->get('usuario_has_grupo');
			if ($query->num_rows() > 0) {
				return $query;
			}else{
				return false;
			}
		} catch (Exception $e) {
			return $e;
		}
	}

	public function consult_user1($id){
		try {
			$this->db->where('usuario_id', $id);
			$query = $this->db->get('usuario');
			if ($query->num_rows() > 0) {
				return $query;
			}else{
				return false;
			}
		} catch (Exception $e) {
			return $e;
		}
	}

	public function consult_user($id,$grupo){
		try {
			$sql  ="SELECT u.usuario_id,u.usuario_nombre,u.email,s.seccion_company_nombre,o.oficina_company_nombre, ug.id_grupo_usuario 
					FROM usuario u 
					JOIN seccion_company s ON s.id_seccion_company = u.seccion_company_id_seccion_company 
					JOIN oficina_company o ON o.id_oficina_company = u.oficina_company_id_oficina_company
					JOIN grupo_usuario ug ON ug.id_grupo_usuario = '$grupo'  
					WHERE u.usuario_id = ".$id;
			$result = $this->db->query($sql);	
			if ($result->num_rows() > 0) {
				return $result;
			}else{
				return false;
			}
		} catch (Exception $e) {
			return $e;
		}
	}

	public function upload_file($file_name,$elemento,$recurso,$proceso){
		try {
			$status = 'Inactivo';
			$sql = "UPDATE document SET status='$status' WHERE elemento=$elemento AND recurso=$recurso AND proceso=$proceso";
			$this->db->query($sql);	
			try {
				$data = array(
					'proceso' => $proceso,
					'recurso' => $recurso,
					'elemento' => $elemento,
					'nombre' => $file_name,
					'status'=>'Activo'
				);
				$this->db->insert('document', $data);
				return true;
			} catch (Exception $e) {
				return false;
			}
		} catch (Exception $e) {
			return false;
		}	
	}

	public function documentos_has_elemento($id){
		try {
			$sql = "SELECT * FROM document WHERE elemento = $id ORDER BY id_document DESC";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query;
			}else{
				return false;
			}
		} catch (Exception $e) {
			return $e;
		}
	}

	public function documentos_has_elemento1($proceso,$recurso,$elemento){
		try {
			$sql = "SELECT * FROM document WHERE elemento = '$elemento' AND recurso = '$recurso' AND proceso = '$proceso'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query;
			}else{
				return false;
			}
		} catch (Exception $e) {
			return $e;
		}
	}

	public function seleccionar_recurso($id){
		$this->db->where('id_recurso',$id);
		$result = $this->db->get('recurso');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return null;
		}
	}

	public function recurso_has_elemento($id){
		$this->db->where('elemento_id_elemento',$id);
		$result = $this->db->get('recurso_has_elemento');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function consult_recursos($id){
		$this->db->where('id_recurso',$id);
		$result = $this->db->get('recurso');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function consult_proceso_has_recursos($id){
		$this->db->where('id_proceso',$id);
		$result = $this->db->get('proceso');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function upd_status_document($documento,$elemento,$recurso){
		try {
			$status = 'Inactivo';
			$sql = "UPDATE document SET status='$status' WHERE elemento=$elemento AND recurso = '$recurso'";
			$this->db->query($sql);	
			try {
				$status1 = 'Activo';
				$sql1 = "UPDATE document SET status='$status1' WHERE id_document=$documento";
				$this->db->query($sql1);	
				return true;
			} catch (Exception $e) {
				return false;
			}
		} catch (Exception $e) {
			return false;
		}
	}

	public function remover_usuario_grupo($usuario,$grupo){
		try {
			$sql = "DELETE FROM usuario_has_grupo 
					WHERE usuario_usuario_id = '$usuario' 
					AND grupo_usuario_id_grupo_usuario = '$grupo'";
			$this->db->query($sql);	
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function grupos_x_proceso($id){
		$sql = "SELECT DISTINCT gu.*
				FROM accesos a 
				JOIN grupo_usuario gu ON gu.id_grupo_usuario = a.grupo
				WHERE a.componente = '$id' AND a.tipo = '3'";
		$result = $this->db->query($sql);	
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function grupos_x_recurso($id){
		$sql = "SELECT DISTINCT gu.*
				FROM accesos a 
				JOIN grupo_usuario gu ON gu.id_grupo_usuario = a.grupo
				WHERE a.componente = '$id' AND a.tipo = '1'";
		$result = $this->db->query($sql);	
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	// public function grupos_x_elemento($id){
	// 	$sql1 = "SELECT re.* 
	// 			FROM recurso_has_elemento re
	// 			WHERE re.elemento_id_elemento = $id";
	// 	$result1 = $this->db->query($sql1);
	// 	if ($result1->num_rows() > 0) {
	// 		foreach ($result1->result() as $row) {
	// 			$sql = "SELECT DISTINCT gu.*
	// 			FROM accesos a 
	// 			JOIN grupo_usuario gu ON gu.id_grupo_usuario = a.grupo
	// 			WHERE a.componente = '$row->recurso_id_recurso' AND a.tipo = '1'";
	// 			$result = $this->db->query($sql);	
	// 			if ($result->num_rows() > 0) {
	// 				return $result;
	// 			}else{
	// 				return false;
	// 			}
	// 		}
	// 	}else{
	// 		return false;
	// 	}
	// }

	public function grupos_x_elemento($id){
		$sql = "SELECT gu.*
				FROM accesos a 
				JOIN grupo_usuario gu ON gu.id_grupo_usuario = a.grupo
				WHERE a.componente = '$id' AND a.tipo = '2'";
		$result = $this->db->query($sql);	
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function delete_grupos_x_proceso($grupo,$componente){
		try {
			$sql = "DELETE FROM accesos WHERE grupo = '$grupo' AND componente = '$componente' AND tipo = '3'";
			$this->db->query($sql);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function delete_grupos_x_recurso($grupo,$componente){
		try {
			$sql = "DELETE FROM accesos WHERE grupo = '$grupo' AND componente = '$componente' AND tipo = '1'";
			$this->db->query($sql);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function delete_grupos_x_elemento($grupo, $componente){
		try {
			$sql = "DELETE FROM accesos WHERE grupo = '$grupo' AND componente = '$componente' AND tipo = '2'";
			$this->db->query($sql);
			try {
				$sql1 = "DELETE FROM recurso_has_elemento WHERE grupo_id_grupo = '$grupo' AND elemento_id_elemento = '$componente'";
				$this->db->query($sql1);
				return true;
			} catch (Exception $e) {
				return false;
			}
		} catch (Exception $e) {
			return false;
		}
	}

	public function seleccionar_recursos_x_elemento($id){
	 	$sql = "SELECT r.* FROM recurso_has_elemento re 
				JOIN recurso r ON r.id_recurso = re.recurso_id_recurso
				WHERE re.elemento_id_elemento = '$id'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function seleccionar_proceso_x_recurso($id){
		$sql = "SELECT p.* FROM recurso r 
				JOIN proceso p ON p.id_proceso = r.proceso_id_proceso
				WHERE r.id_recurso = '$id'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function seleccionar_recursos_no_asignados($id){
		$sql = "SELECT * FROM recurso 
				WHERE id_recurso NOT IN 
				(SELECT recurso_id_recurso FROM recurso_has_elemento WHERE elemento_id_elemento = '$id')";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	// ===============================================================================

	public function Assign_resource_element(){
		$id_elemento = $this->input->post('id_elemento');
		$id_recurso = $this->input->post('id_recurso');
		$recurso_has_elemento = array('recurso_id_recurso'=>$id_recurso,'elemento_id_elemento'=>$id_elemento);
		try {
			$this->db->insert('recurso_has_elemento', $recurso_has_elemento);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function Assign_element_group(){
		$id_elemento = $this->input->post('id_elemento');
		$grupo = $this->input->post('grupo');
		try {
			$accesos = array('grupo'=>$grupo,'componente'=>$id_elemento,'tipo'=>'2');
			$this->db->insert('accesos', $accesos);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function validar_elemento($elemento,$tipo){
		$sql ="SELECT *  FROM recurso_has_elemento WHERE elemento_id_elemento = '$elemento'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}

}

/* End of file admin.php */
/* Location: ./application/models/admin.php */

// $sql = "SELECT * FROM grupo_usuario WHERE id_grupo_usuario
// 				NOT IN (SELECT grupo FROM accesos )";
// 		$grupos = $this->db->query($sql);
// 		if ($grupos->num_rows() > 0) {
// 			return $grupos;
// 		}else{
// 			return false;
// 		}