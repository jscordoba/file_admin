<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_user extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function sel_proceso_user ()
	{
		$usuario_id = $this->session->userdata('usuario_id');

		$this->db->join('usuario_has_grupo', 'usuario_has_grupo.grupo_usuario_id_grupo_usuario = acceso.grupo_usuario_id_grupo_usuario', 'join');

		$this->db->join('usuario', 'usuario.usuario_id = usuario_has_grupo.usuario_usuario_id', 'join');

		$this->db->join('proceso', 'proceso.id_proceso = acceso.recurso_id_recurso', 'join');

		$this->db->join('oficina_company', 'oficina_company.id_oficina_company = proceso.oficina_company_id_oficina_company', 'join');

		$this->db->join('seccion_company', 'seccion_company.id_seccion_company = proceso.seccion_company_id_seccion_company', 'join');

		$this->db->select('proceso.id_proceso,proceso.proceso_nombre,proceso.usuario_id_responsable,proceso.status,seccion_company.seccion_company_nombre,oficina_company.oficina_company_nombre');

		$this->db->where('usuario.usuario_id', $usuario_id);
	
		$procesos = $this->db->get('acceso');
		
		if ($procesos->num_rows() > 0) {
			# code...
			return $procesos;
		}

		// print_r($procesos);

		return null;
	}

	public function sel_recurso_user($proceso_id=null)
	{
    	// $status = "Activo";
		if ($proceso_id != NULL) {
			$whereProceso = "AND p.id_proceso=$proceso_id";
		}else{
			$whereProceso="";
		}
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT r.id_recurso,r.recurso_nombre,u.usuario_id,u.usuario_nombre
				FROM accesos a
				JOIN usuario_has_grupo ug ON ug.grupo_usuario_id_grupo_usuario = a.grupo
				JOIN usuario u ON u.usuario_id = ug.usuario_usuario_id
				JOIN recurso r ON r.tipo_recurso_id_tipo_recurso = ug.grupo_usuario_id_grupo_usuario
				WHERE u.usuario_id = '$usuario'";
		try {
			$recurso = $this->db->query($sql);
			if ($recurso->num_rows() > 0) {
				return $recurso;
			}
		} catch (Exception $e) {
			echo "Error";
		}
	}

	public function sel_elemento_user ($recurso_id=null)
	{
		$usuario = $this->session->userdata('usuario_id');

		if ($recurso_id != NULL) {
			$this->db->where('recurso.id_recurso', $recurso_id);
			$where = "AND `recurso`.`id_recurso` =  $recurso_id";
		}else{
			$where="";
		}	
		
		// SELECT * 
		// FROM usuario_has_grupo ug
		// 	JOIN acceso a ON a.grupo_usuario_id_grupo_usuario=ug.grupo_usuario_id_grupo_usuario
		// 	JOIN recurso r ON r.id_recurso=a.recurso_id_recurso
		// WHERE ug.grupo_usuario_id_grupo_usuario=4
		// GROUP BY ug.grupo_usuario_id_grupo_usuario

		$sql = "SELECT  elemento.id_elemento,elemento.elemento_nombre,recurso.id_recurso,recurso.recurso_nombre,proceso.id_proceso,proceso.proceso_nombre,proceso.status,recurso_has_elemento.id_recurso_has_elemento
				FROM usuario 
				JOIN usuario_has_grupo ON usuario_has_grupo.usuario_usuario_id = usuario.usuario_id 
				JOIN accesos ON accesos.grupo = usuario_has_grupo.grupo_usuario_id_grupo_usuario
				JOIN recurso_has_elemento ON recurso_has_elemento.elemento_id_elemento = accesos.componente
				JOIN recurso ON recurso.id_recurso = recurso_has_elemento.recurso_id_recurso 
				JOIN proceso ON proceso.id_proceso = recurso.proceso_id_proceso 
				JOIN elemento ON elemento.id_elemento=recurso_has_elemento.elemento_id_elemento
				WHERE accesos.tipo = 2 AND usuario.usuario_id ='$usuario' 
				GROUP BY elemento.id_elemento";

		$elemento = $this->db->query($sql);


		if ($elemento->num_rows() > 0) {
			# code...
			return $elemento;
		}

		// print_r($elemento);

		return null;
	}

// ================================== Procesos ============================================

	public function user_has_group(){
		$usuario_id = $this->session->userdata('usuario_id');
		$this->db->where('usuario_usuario_id',$usuario_id);
		$result = $this->db->get('usuario_has_grupo');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function accesos($group){
		$this->db->where('grupo',$group);
		$result = $this->db->get('accesos');
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}
//================================================================================
	public function procesos(){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT proceso.id_proceso,
				proceso.proceso_nombre,
				proceso.usuario_id_responsable,
				proceso.status,
				seccion_company.seccion_company_nombre,
				oficina_company.oficina_company_nombre
				FROM accesos accesos
				JOIN usuario_has_grupo ON usuario_has_grupo.grupo_usuario_id_grupo_usuario = accesos.grupo
				JOIN usuario ON usuario.usuario_id = usuario_has_grupo.usuario_usuario_id
				JOIN proceso ON proceso.id_proceso = accesos.componente AND accesos.tipo = 3
				JOIN oficina_company ON oficina_company.id_oficina_company = proceso.oficina_company_id_oficina_company
				JOIN seccion_company ON seccion_company.id_seccion_company = proceso.seccion_company_id_seccion_company
				WHERE usuario.usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function recursos_X_proceso($proceso){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT  DISTINCT recurso.id_recurso,
				recurso.recurso_nombre,
				recurso.proceso_id_proceso,
				recurso.status,
				proceso.proceso_nombre
				FROM accesos accesos
				JOIN usuario_has_grupo ON usuario_has_grupo.grupo_usuario_id_grupo_usuario = accesos.grupo AND accesos.tipo = 1
				JOIN usuario ON usuario.usuario_id = usuario_has_grupo.usuario_usuario_id
				JOIN recurso ON recurso.id_recurso = accesos.componente AND recurso.proceso_id_proceso = '$proceso'
				JOIN proceso ON proceso.id_proceso = recurso.proceso_id_proceso
				WHERE usuario.usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function recursos_X_proceso1($proceso){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT r.*, p.id_proceso,p.proceso_nombre FROM recurso r
		JOIN proceso p ON p.id_proceso = r.proceso_id_proceso
		WHERE r.proceso_id_proceso = '$proceso'";;
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}
//================================================================================
	public function recursos(){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT  DISTINCT recurso.id_recurso,
				recurso.recurso_nombre,
				recurso.proceso_id_proceso,
				recurso.status
				FROM accesos accesos
				JOIN usuario_has_grupo ON usuario_has_grupo.grupo_usuario_id_grupo_usuario = accesos.grupo AND accesos.tipo = 1
				JOIN usuario ON usuario.usuario_id = usuario_has_grupo.usuario_usuario_id
				JOIN recurso ON recurso.id_recurso = accesos.componente
				
				
				WHERE usuario.usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function elementos_x_recurso($recurso){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT  DISTINCT
				elemento.id_elemento,elemento.elemento_nombre,elemento.status, recurso.recurso_nombre, proceso.proceso_nombre
				FROM usuario 
				JOIN usuario_has_grupo ON usuario_has_grupo.usuario_usuario_id = '$usuario'
				JOIN accesos ON accesos.grupo = usuario_has_grupo.grupo_usuario_id_grupo_usuario
				JOIN recurso_has_elemento ON recurso_has_elemento.recurso_id_recurso = accesos.componente 
				JOIN elemento ON elemento.id_elemento = recurso_has_elemento.elemento_id_elemento 
				JOIN recurso ON recurso.id_recurso = '$recurso'
				JOIN proceso ON proceso.id_proceso =  recurso.proceso_id_proceso
				WHERE usuario.usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}
//================================================================================
	public function elemento($grupo,$componente){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT e.id_elemento, re.recurso_id_recurso, e.elemento_nombre, e.status ,r.recurso_nombre, p.proceso_nombre
				FROM elemento e 
				JOIN recurso_has_elemento re ON re.elemento_id_elemento = '$componente'
				JOIN recurso r ON r.id_recurso = re.recurso_id_recurso
				JOIN proceso p ON p.id_proceso = r.proceso_id_proceso
				WHERE e.id_elemento = '$componente'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function documentos_x_elemento($elemento,$recurso){
		try {
			$sql = "SELECT document.id_document, document.nombre,elemento.elemento_nombre
					FROM document 
					JOIN elemento ON elemento.id_elemento = '$elemento'
					WHERE document.elemento = '$elemento' AND document.recurso = '$recurso' AND document.status = 'Activo'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query;
			}else{
				return false;
			}
		} catch (Exception $e) {
			return $e;
		}
	}

	public function validar(){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT a.grupo,a.componente,a.tipo FROM usuario_has_grupo ug 
				JOIN accesos a ON a.grupo = ug.grupo_usuario_id_grupo_usuario
				WHERE ug.usuario_usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function recurso($componente,$grupo,$tipo){
		// echo "<script>console.log(".$componente." ,".$grupo." , ".$tipo.");</script>";
		$sql = "SELECT r.id_recurso, r.recurso_nombre, r.status, p.proceso_nombre FROM recurso r 
				JOIN proceso p ON p.id_proceso = r.proceso_id_proceso
				WHERE r.id_recurso = '$componente'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}

	}
//================================== New =====================================================
	public function grupos_nav(){
		$usuario_id = $this->session->userdata('usuario_id');
		$sql = "SELECT gu.* FROM usuario_has_grupo ug JOIN grupo_usuario gu ON gu.id_grupo_usuario = ug.grupo_usuario_id_grupo_usuario WHERE ug.usuario_usuario_id = '$usuario_id'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function accesos_x_grupo($grupo){
		$sql = "SELECT * FROM accesos WHERE grupo ='$grupo' ORDER BY tipo ASC";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function procesos_x_grupo($grupo,$componente,$tipo){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT DISTINCT proceso.id_proceso,
				proceso.proceso_nombre,
				proceso.usuario_id_responsable,
				proceso.status,
				seccion_company.seccion_company_nombre,
				oficina_company.oficina_company_nombre
				FROM accesos accesos
				JOIN usuario_has_grupo ON usuario_has_grupo.grupo_usuario_id_grupo_usuario = accesos.grupo
				JOIN usuario ON usuario.usuario_id = usuario_has_grupo.usuario_usuario_id
				JOIN proceso ON proceso.id_proceso = accesos.componente AND accesos.tipo = '$tipo'
				JOIN oficina_company ON oficina_company.id_oficina_company = proceso.oficina_company_id_oficina_company
				JOIN seccion_company ON seccion_company.id_seccion_company = proceso.seccion_company_id_seccion_company
				WHERE usuario.usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function recursos_x_grupo($grupo,$componente,$tipo){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT DISTINCT recurso.id_recurso,
				recurso.recurso_nombre,
				proceso.proceso_nombre,
				recurso.status
				FROM accesos accesos
				JOIN usuario_has_grupo ON usuario_has_grupo.grupo_usuario_id_grupo_usuario = accesos.grupo AND accesos.tipo = '$tipo'
				JOIN usuario ON usuario.usuario_id = usuario_has_grupo.usuario_usuario_id
				JOIN recurso ON recurso.id_recurso = accesos.componente
				JOIN proceso ON proceso.id_proceso = recurso.proceso_id_proceso
				WHERE usuario.usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	// public function elementos_x_grupo($grupo,$componente,$tipo){
	// 	$usuario = $this->session->userdata('usuario_id');
	// 	$sql = "SELECT DISTINCT e.id_elemento, re.recurso_id_recurso, e.elemento_nombre, e.status ,r.recurso_nombre, p.proceso_nombre
	// 			FROM accesos a 
	// 			JOIN elemento e ON e.id_elemento = a.componente
	// 			JOIN recurso_has_elemento re ON re.elemento_id_elemento = a.componente
	// 			JOIN recurso r ON r.id_recurso = re.recurso_id_recurso
	// 			JOIN proceso p ON p.id_proceso = r.proceso_id_proceso
	// 			JOIN usuario_has_grupo ug ON ug.grupo_usuario_id_grupo_usuario = a.tipo  AND ug.usuario_usuario_id = '$usuario'";
	// 	$result = $this->db->query($sql);
	// 	if ($result->num_rows() > 0) {
	// 		return $result;
	// 	}else{
	// 		return false;
	// 	}
	// }

	public function elementos_x_grupo($grupo,$componente,$tipo){
		$usuario = $this->session->userdata('usuario_id');
		$sql = "SELECT DISTINCT e.id_elemento, e.elemento_nombre, e.status 
				FROM accesos a 
				JOIN elemento e ON e.id_elemento = a.componente
				JOIN usuario_has_grupo ug ON ug.grupo_usuario_id_grupo_usuario = a.tipo  
				AND ug.usuario_usuario_id = '$usuario'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function elemento_procesos($elemento){
		$sql = "SELECT DISTINCT p.id_proceso, p.proceso_nombre, p.status
				FROM elemento e 
				JOIN recurso_has_elemento re ON re.elemento_id_elemento = e.id_elemento
				JOIN recurso r ON r.id_recurso = re.recurso_id_recurso
				JOIN proceso p ON p.id_proceso = r.proceso_id_proceso
				WHERE e.id_elemento = '$elemento'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}

	public function elemento_procesos_recurso($recurso,$elemento){
		$sql ="SELECT e.* FROM elemento e
				JOIN recurso r ON r.id_recurso = '$recurso'
				WHERE e.id_elemento = '$elemento'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result;
		}else{
			return false;
		}
	}
}

/* End of file m_user.php */
/* Location: ./application/models/m_user.php */