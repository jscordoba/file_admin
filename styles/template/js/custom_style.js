jQuery(document).ready(function($) {
	
	$(".btn_asignarUserGroup").click(function(event) {
		var base_url = window.location.origin+"/file_admin/";
		var id_grupo = $(this).data('grupo_id');
		$.post(''+base_url+'Main/lista_usuarioGroup', {id_grupo: id_grupo }, function(data, textStatus, xhr) {
			if (data) {
				$("#tbody_UsuariosGroup").text("");
				$("#tbody_UsuariosGroup").append(data);
			};
		});
	});

	$(".btn-asignarPermisosProc").click(function(event) {
		var base_url = window.location.origin+"/file_admin/";
		var proceso_id = $(this).data('proceso_id');
		$("#componente").val(proceso_id);
		var parametros = {"componente":proceso_id,"tipo":"3"};
		$.ajax({
			url: ''+base_url+'Admin/listar_grupos_x_asignar',
			type: 'post',
			data: parametros,
		})
		.done(function(data) {
			$("#grupo").empty();
			$("#grupo").append('<option value="" selected disabled="">Seleccione una opción</option>');
			$("#grupo").append(data);
		})
		.fail(function(data_error) {
			alert(data_error);
		});
		
	});

	$(".btn-asignarPermisosRec").click(function(event) {
		var base_url = window.location.origin+"/file_admin/";
		var recurso_id = $(this).data('recurso_id');
		$("#componente").val(recurso_id);
		var parametros = {"componente":recurso_id,"tipo":"1"};
		$.ajax({
			url: ''+base_url+'Admin/listar_grupos_x_asignar',
			type: 'post',
			data: parametros,
		})
		.done(function(data) {
			$("#grupo").empty();
			$("#grupo").append('<option value="" selected disabled="">Seleccione una opción</option>');
			$("#grupo").append(data);
		})
		.fail(function(data_error) {
			alert(data_error);
		});
	});

	$(".btn-asignarPermisosEle").click(function(event) {
		var base_url = "http://[::1]/file_admin/";
		var id_elemento = $(this).data('id_elemento');
		$("#componente").val(id_elemento);
		var parametros = {"componente":id_elemento,"tipo":"2"};
		$.ajax({
			url: ''+base_url+'Admin/listar_grupos_x_asignar',
			type: 'post',
			data: parametros,
		})
		.done(function(data) {
			$("#grupo").empty();
			$("#grupo").append('<option value="" selected disabled="">Seleccione una opción</option>');
			$("#grupo").append(data);
		})
		.fail(function(data_error) {
			alert(data_error);
		});
	});

});



