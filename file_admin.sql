-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2017 a las 03:52:01
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `archive`
--
CREATE DATABASE IF NOT EXISTS `file_admin` DEFAULT CHARACTER SET ucs2 COLLATE ucs2_spanish2_ci;
USE `file_admin`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acceso`
--

CREATE TABLE IF NOT EXISTS `acceso` (
  `id_acceso` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_usuario_id_grupo_usuario` int(11) NOT NULL,
  `recurso_id_recurso` int(11) NOT NULL COMMENT 'Se especifica el ID del recurso o elemento al cual tiene acceso un USUARIO determinado.',
  `tipo_recurso_id_tipo_recurso` int(11) NOT NULL,
  PRIMARY KEY (`grupo_usuario_id_grupo_usuario`,`recurso_id_recurso`,`tipo_recurso_id_tipo_recurso`),
  UNIQUE KEY `id_acceso_UNIQUE` (`id_acceso`),
  KEY `fk_acceso_grupo_usuario1_idx` (`grupo_usuario_id_grupo_usuario`),
  KEY `fk_acceso_tipo_recurso1_idx` (`tipo_recurso_id_tipo_recurso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesos`
--

CREATE TABLE IF NOT EXISTS `accesos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grupo` bigint(20) NOT NULL,
  `componente` bigint(20) NOT NULL,
  `tipo` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish2_ci;

--
-- Volcado de datos para la tabla `accesos`
--

INSERT INTO `accesos` (`id`, `grupo`, `componente`, `tipo`) VALUES
(1, 1, 1, 2),
(2, 2, 1, 3),
(3, 3, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company_has_grupo_usuario`
--

CREATE TABLE IF NOT EXISTS `company_has_grupo_usuario` (
  `company_has_grupo_usuario_seccion` int(11) NOT NULL,
  `company_has_grupo_usuario_id_oficina` int(11) NOT NULL,
  `grupo_usuario_id_grupo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`company_has_grupo_usuario_seccion`,`company_has_grupo_usuario_id_oficina`,`grupo_usuario_id_grupo_usuario`),
  KEY `fk_seccion_company_has_oficina_company_has_grupo_usuario_gr_idx` (`grupo_usuario_id_grupo_usuario`),
  KEY `fk_company_has_grupo_usuario_oficina_company1_idx` (`company_has_grupo_usuario_id_oficina`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id_document` int(11) NOT NULL AUTO_INCREMENT,
  `proceso` bigint(20) NOT NULL,
  `recurso` bigint(20) NOT NULL,
  `elemento` bigint(20) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_document`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `document`
--

INSERT INTO `document` (`id_document`, `proceso`, `recurso`, `elemento`, `nombre`, `status`) VALUES
(1, 2, 2, 1, 'Documento_P2R2E1_v0.pdf', 'Inactivo'),
(2, 1, 3, 1, 'Documento_P1R3E1_v0.pdf', 'Inactivo'),
(3, 1, 1, 1, 'Documento_P1R1E1_v0.pdf', 'Activo'),
(4, 1, 3, 1, 'Documento_P1R3E1_v01.pdf', 'Activo'),
(5, 2, 2, 1, 'Documento_P2R2E1_v01.pdf', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document_version`
--

CREATE TABLE IF NOT EXISTS `document_version` (
  `id_version` int(11) NOT NULL AUTO_INCREMENT,
  `version_num` int(10) NOT NULL,
  `version_nombre` varchar(45) NOT NULL,
  `version_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_recurso_has_elemento` int(11) NOT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `document_version_visible` tinyint(1) NOT NULL,
  `document_id_document` int(11) NOT NULL,
  PRIMARY KEY (`id_version`,`id_recurso_has_elemento`),
  KEY `fk_version_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_document_version_document1_idx` (`document_id_document`),
  KEY `fk_document_version_recurso_has_elemento1_idx` (`id_recurso_has_elemento`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `document_version`
--

INSERT INTO `document_version` (`id_version`, `version_num`, `version_nombre`, `version_creacion`, `id_recurso_has_elemento`, `usuario_id_creacion`, `document_version_visible`, `document_id_document`) VALUES
(53, 1, 'ESTRUCTURA AT_v1', '2016-12-20 14:55:14', 9, 10101010, 2, 57),
(54, 2, 'ESTRUCTURA BT_v2', '2016-12-20 14:55:45', 9, 10101010, 2, 58),
(55, 3, 'ESTRUCTURA METIDA A CAJA_v3', '2016-12-20 15:06:53', 9, 10101010, 2, 59),
(56, 4, 'ESTRUCTURA ENSAMBLE_v4', '2016-12-20 15:07:11', 9, 10101010, 2, 60),
(57, 5, 'ESTRUCTURA METALMECANICA_v5', '2016-12-20 15:07:33', 9, 10101010, 1, 61),
(58, 1, 'F-PD1-05_v1', '2016-12-20 15:09:52', 10, 10101010, 1, 62),
(59, 1, 'LISTA DE MATERIALES_v1', '2016-12-20 15:10:45', 11, 10101010, 1, 63),
(60, 1, 'MEDIOS DE PRODUCCION_v1', '2016-12-20 15:11:04', 12, 10101010, 1, 64),
(61, 1, 'PARAMETRO_v1', '2016-12-20 15:12:03', 13, 10101010, 1, 65),
(62, 1, 'ALTA TAP 2_v1', '2016-12-20 15:12:37', 14, 10101010, 2, 66),
(63, 2, 'ANEXO ALTA_v2', '2016-12-20 15:12:57', 14, 10101010, 2, 67),
(64, 3, 'BAJA_v3', '2016-12-20 15:23:13', 14, 10101010, 2, 68),
(65, 4, 'CORTE AISLAMIENTO POR BOBINA_v4', '2016-12-20 15:23:22', 14, 10101010, 2, 69),
(66, 5, 'CRITERIOS DE ACEPTACION - RECHAZO_v5', '2016-12-20 15:23:30', 14, 10101010, 2, 70),
(67, 6, 'ENSAMBLE_v6', '2016-12-20 15:23:39', 14, 10101010, 2, 71),
(68, 7, 'NUCLEOS_v7', '2016-12-20 15:23:46', 14, 10101010, 1, 72),
(69, 1, 'BRIDA_v1', '2016-12-20 15:24:27', 15, 10101010, 2, 73),
(70, 2, 'CONFINAMIENTO Y CONEXIONES_v2', '2016-12-20 15:24:35', 15, 10101010, 2, 74),
(71, 3, 'TANQUE_v3', '2016-12-20 15:24:42', 15, 10101010, 1, 75),
(72, 1, 'PLATINAS_v1', '2016-12-20 15:27:09', 17, 10101010, 1, 76),
(73, 1, 'R-PD1-01_v1', '2016-12-20 15:28:38', 19, 10101010, 1, 77),
(74, 1, 'R-PD1-03_v1', '2016-12-20 15:28:57', 20, 10101010, 1, 78),
(75, 1, 'R-PD1-04_v1', '2016-12-20 15:29:11', 21, 10101010, 1, 79);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elemento`
--

CREATE TABLE IF NOT EXISTS `elemento` (
  `id_elemento` int(11) NOT NULL AUTO_INCREMENT,
  `elemento_nombre` varchar(45) DEFAULT NULL,
  `elemento_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `elemento_update` timestamp NULL DEFAULT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `tipo_recurso_id_tipo_recurso` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_elemento`),
  KEY `fk_elemento_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_tipo_recurso_elemento1_idx` (`tipo_recurso_id_tipo_recurso`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `elemento`
--

INSERT INTO `elemento` (`id_elemento`, `elemento_nombre`, `elemento_creacion`, `elemento_update`, `usuario_id_creacion`, `tipo_recurso_id_tipo_recurso`, `status`) VALUES
(1, 'Elemento #1', '2017-06-01 05:13:57', '2017-06-01 05:13:57', 1002593980, 2, 'Activo'),
(2, 'Elemento #2', '2017-06-04 02:43:14', '2017-06-04 02:43:14', 1002593980, 2, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_usuario`
--

CREATE TABLE IF NOT EXISTS `grupo_usuario` (
  `id_grupo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_usuario_nombre` varchar(45) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_grupo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo_usuario`
--

INSERT INTO `grupo_usuario` (`id_grupo_usuario`, `grupo_usuario_nombre`, `status`) VALUES
(1, 'Grupo #1', 'Activo'),
(2, 'Grupo #2', 'Activo'),
(3, 'Grupo #3', 'Activo'),
(4, 'Grupo #4', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficina_company`
--

CREATE TABLE IF NOT EXISTS `oficina_company` (
  `id_oficina_company` int(11) NOT NULL AUTO_INCREMENT,
  `oficina_company_nombre` varchar(45) DEFAULT NULL,
  `oficina_company_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_oficina_company`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `oficina_company`
--

INSERT INTO `oficina_company` (`id_oficina_company`, `oficina_company_nombre`, `oficina_company_update`, `status`) VALUES
(1, 'Planta #1', '2017-05-10 16:53:38', 'Activo'),
(2, 'Planta #2', '2017-05-10 16:53:30', 'Activo'),
(3, 'Zona Franca', '2016-10-21 15:34:18', 'Activo'),
(4, 'Barranquilla', '2016-11-11 16:32:12', 'Activo'),
(5, 'Planta #3', '2017-05-11 20:18:27', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_usuario`
--

CREATE TABLE IF NOT EXISTS `perfil_usuario` (
  `id_perfil_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_usuario_nombre` varchar(45) DEFAULT NULL,
  `perfil_usuario_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_perfil_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfil_usuario`
--

INSERT INTO `perfil_usuario` (`id_perfil_usuario`, `perfil_usuario_nombre`, `perfil_usuario_update`) VALUES
(1, 'SuperAdmin', '2016-10-21 15:30:12'),
(2, 'Admin', '2016-10-21 15:30:12'),
(3, 'User', '2016-10-21 15:30:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE IF NOT EXISTS `proceso` (
  `id_proceso` int(11) NOT NULL AUTO_INCREMENT,
  `proceso_nombre` varchar(45) DEFAULT NULL,
  `proceso_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `proceso_update` timestamp NULL DEFAULT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `usuario_id_responsable` int(11) NOT NULL,
  `seccion_company_id_seccion_company` int(11) NOT NULL,
  `oficina_company_id_oficina_company` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_proceso`),
  KEY `fk_proceso_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_proceso_seccion_company1_idx` (`seccion_company_id_seccion_company`),
  KEY `fk_proceso_oficina_company1_idx` (`oficina_company_id_oficina_company`),
  KEY `fk_proceso_usuario2_idx` (`usuario_id_responsable`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id_proceso`, `proceso_nombre`, `proceso_creacion`, `proceso_update`, `usuario_id_creacion`, `usuario_id_responsable`, `seccion_company_id_seccion_company`, `oficina_company_id_oficina_company`, `status`) VALUES
(1, 'Proceso #1', '2017-06-01 05:50:38', NULL, 1088004425, 1002593980, 2, 1, 'Activo'),
(2, 'Proceso #2', '2017-06-01 05:15:03', NULL, 1088004425, 1002593980, 1, 1, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recurso`
--

CREATE TABLE IF NOT EXISTS `recurso` (
  `id_recurso` int(11) NOT NULL AUTO_INCREMENT,
  `recurso_nombre` varchar(45) DEFAULT NULL,
  `recurso_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `recurso_update` timestamp NULL DEFAULT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `proceso_id_proceso` int(11) NOT NULL,
  `tipo_recurso_id_tipo_recurso` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_recurso`,`proceso_id_proceso`),
  KEY `fk_recurso_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_tipo_recurso_id_tipo_recurso_idx` (`tipo_recurso_id_tipo_recurso`),
  KEY `fk_proceso_id_proceso_idx` (`proceso_id_proceso`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `recurso`
--

INSERT INTO `recurso` (`id_recurso`, `recurso_nombre`, `recurso_creacion`, `recurso_update`, `usuario_id_creacion`, `proceso_id_proceso`, `tipo_recurso_id_tipo_recurso`, `status`) VALUES
(1, 'Recurso #1', '2017-06-01 05:14:39', '2017-06-03 21:42:53', 1002593980, 1, 1, 'Activo'),
(2, 'Recurso #2', '2017-06-01 05:16:06', '2017-06-01 05:16:06', 1002593980, 2, 1, 'Activo'),
(3, 'Recurso #3', '2017-06-04 02:04:17', '2017-06-04 02:04:17', 1002593980, 1, 2, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recurso_has_elemento`
--

CREATE TABLE IF NOT EXISTS `recurso_has_elemento` (
  `id_recurso_has_elemento` int(11) NOT NULL AUTO_INCREMENT,
  `recurso_id_recurso` int(11) NOT NULL,
  `elemento_id_elemento` int(11) NOT NULL,
  PRIMARY KEY (`id_recurso_has_elemento`,`recurso_id_recurso`,`elemento_id_elemento`),
  KEY `fk_recurso_has_elemento_elemento1_idx` (`elemento_id_elemento`),
  KEY `fk_recurso_has_elemento_recurso1_idx` (`recurso_id_recurso`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `recurso_has_elemento`
--

INSERT INTO `recurso_has_elemento` (`id_recurso_has_elemento`, `recurso_id_recurso`, `elemento_id_elemento`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_company`
--

CREATE TABLE IF NOT EXISTS `seccion_company` (
  `id_seccion_company` int(11) NOT NULL AUTO_INCREMENT,
  `seccion_company_nombre` varchar(45) DEFAULT NULL,
  `seccion_company_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_seccion_company`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seccion_company`
--

INSERT INTO `seccion_company` (`id_seccion_company`, `seccion_company_nombre`, `seccion_company_update`, `status`) VALUES
(1, 'Tics', '2016-10-21 15:33:46', 'Activo'),
(2, 'Ingenieria', '2016-10-21 15:33:46', 'Activo'),
(3, 'Recursos Humanos', '2016-11-11 16:32:29', 'Activo'),
(4, 'Producción', '2016-11-11 16:32:43', 'Activo'),
(5, 'Soldadura', '2016-11-28 13:27:37', 'Activo'),
(7, 'Pintura', '2017-05-10 16:25:27', 'Activo'),
(8, 'Bodega', '2017-05-11 20:14:51', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_recurso`
--

CREATE TABLE IF NOT EXISTS `tipo_recurso` (
  `id_tipo_recurso` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_recurso_nombre` varchar(45) DEFAULT NULL COMMENT 'Nombre del Recurso para identificar que acceso esta siendo asignado a que usuario:\n\nRecurso (Nombre del diseño o de HV)\nElemento (componente del diseño o resgistro de HV)',
  PRIMARY KEY (`id_tipo_recurso`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_recurso`
--

INSERT INTO `tipo_recurso` (`id_tipo_recurso`, `tipo_recurso_nombre`) VALUES
(1, 'Recurso'),
(2, 'Elemento'),
(3, 'Proceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario_id` int(20) NOT NULL,
  `usuario_nombre` varchar(90) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `seccion_company_id_seccion_company` int(11) NOT NULL,
  `oficina_company_id_oficina_company` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`usuario_id`),
  KEY `fk_usuario_seccion_company1_idx` (`seccion_company_id_seccion_company`),
  KEY `fk_usuario_oficina_company1_idx` (`oficina_company_id_oficina_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario_nombre`, `username`, `password`, `email`, `fecha_creacion`, `seccion_company_id_seccion_company`, `oficina_company_id_oficina_company`, `status`) VALUES
(1010, 'Soldadura Planta 2', 'solda2', '123', 'soldadura@magnetron.com.co', '2016-11-28 13:44:49', 5, 2, 'Activo'),
(2020, 'Pintura Planta 2', 'pintura2', '123', 'pintura@magnetron.com.co', '2016-11-28 13:46:03', 7, 2, 'Activo'),
(2021, 'Pintura Planta 1', 'pintura1', '123', 'pintura1@magnetron.com.co', '2016-11-28 13:46:37', 7, 1, 'Activo'),
(55555, 'usuario', 'usuario', '123', 'usuario@gmail.com', '2017-05-12 19:04:06', 8, 4, 'Activo'),
(4510438, 'Isabela Adriana', 'isa', '123', 'isabela@magnetron.com.co', '2016-11-10 15:18:12', 1, 1, 'Activo'),
(10101010, 'Jeison Restrepo', 'jrestrepo', '123', 'jeisonrestrepo@magnetron.com.co', '2016-11-29 16:55:22', 2, 1, 'Activo'),
(34051895, 'Jaime Andres Quiceno', 'jaime', '123', 'jaimequiceno@magnetron.com.co', '2016-11-10 15:22:56', 2, 2, 'Activo'),
(1002593980, 'Diego Fernando Moya', 'dmoya', '123', 'dmoya@magnetron.com.co', '2016-11-01 14:13:22', 1, 2, 'Activo'),
(1088004425, 'Jeison Stevens Córdoba', 'scordoba', '123', 'jeisoncordoba@magnetron.com.co', '2016-10-24 15:05:53', 1, 1, 'Activo'),
(1112788755, 'Gallego Alejandro', 'Gallego', '123', 'gallego@gmail.com', '2017-05-30 22:08:43', 2, 1, 'Activo'),
(1234567890, 'usuario_admin', 'usuario_admin', '123', 'usuario_admin@gmail.com', '2017-05-12 19:02:42', 2, 2, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_has_grupo`
--

CREATE TABLE IF NOT EXISTS `usuario_has_grupo` (
  `usuario_usuario_id` int(20) NOT NULL,
  `grupo_usuario_id_grupo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`usuario_usuario_id`,`grupo_usuario_id_grupo_usuario`),
  KEY `fk_usuario_has_grupo_usuario_grupo_usuario1_idx` (`grupo_usuario_id_grupo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_has_grupo`
--

INSERT INTO `usuario_has_grupo` (`usuario_usuario_id`, `grupo_usuario_id_grupo_usuario`) VALUES
(55555, 1),
(34051895, 3),
(1112788755, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_has_perfil`
--

CREATE TABLE IF NOT EXISTS `usuario_has_perfil` (
  `usuario_usuario_id` int(20) NOT NULL,
  `perfil_usuario_id_perfil_usuario` int(11) NOT NULL,
  PRIMARY KEY (`usuario_usuario_id`,`perfil_usuario_id_perfil_usuario`),
  KEY `fk_usuario_has_perfil_usuario_perfil_usuario1_idx` (`perfil_usuario_id_perfil_usuario`),
  KEY `fk_usuario_has_perfil_usuario_usuario1_idx` (`usuario_usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_has_perfil`
--

INSERT INTO `usuario_has_perfil` (`usuario_usuario_id`, `perfil_usuario_id_perfil_usuario`) VALUES
(1010, 3),
(2020, 3),
(2021, 3),
(55555, 3),
(4510438, 2),
(10101010, 2),
(34051895, 3),
(1002593980, 2),
(1088004425, 1),
(1112784648, 1),
(1112788755, 3),
(1234567890, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
