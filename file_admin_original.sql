-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 05-05-2017 a las 07:53:13
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `file_admin`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acceso`
--

CREATE TABLE IF NOT EXISTS `acceso` (
  `id_acceso` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_usuario_id_grupo_usuario` int(11) NOT NULL,
  `recurso_id_recurso` int(11) NOT NULL COMMENT 'Se especifica el ID del recurso o elemento al cual tiene acceso un USUARIO determinado.',
  `tipo_recurso_id_tipo_recurso` int(11) NOT NULL,
  PRIMARY KEY (`grupo_usuario_id_grupo_usuario`,`recurso_id_recurso`,`tipo_recurso_id_tipo_recurso`),
  UNIQUE KEY `id_acceso_UNIQUE` (`id_acceso`),
  KEY `fk_acceso_grupo_usuario1_idx` (`grupo_usuario_id_grupo_usuario`),
  KEY `fk_acceso_tipo_recurso1_idx` (`tipo_recurso_id_tipo_recurso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=185 ;

--
-- Volcar la base de datos para la tabla `acceso`
--

INSERT INTO `acceso` (`id_acceso`, `grupo_usuario_id_grupo_usuario`, `recurso_id_recurso`, `tipo_recurso_id_tipo_recurso`) VALUES
(166, 1, 26, 1),
(167, 1, 20, 2),
(168, 1, 21, 2),
(169, 1, 22, 2),
(170, 1, 23, 2),
(171, 1, 24, 2),
(172, 1, 25, 2),
(173, 1, 26, 2),
(174, 1, 27, 2),
(175, 1, 28, 2),
(176, 1, 29, 2),
(177, 1, 30, 2),
(178, 1, 31, 2),
(179, 1, 32, 2),
(180, 1, 33, 2),
(181, 1, 34, 2),
(182, 1, 35, 2),
(183, 1, 36, 2),
(184, 1, 37, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company_has_grupo_usuario`
--

CREATE TABLE IF NOT EXISTS `company_has_grupo_usuario` (
  `company_has_grupo_usuario_seccion` int(11) NOT NULL,
  `company_has_grupo_usuario_id_oficina` int(11) NOT NULL,
  `grupo_usuario_id_grupo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`company_has_grupo_usuario_seccion`,`company_has_grupo_usuario_id_oficina`,`grupo_usuario_id_grupo_usuario`),
  KEY `fk_seccion_company_has_oficina_company_has_grupo_usuario_gr_idx` (`grupo_usuario_id_grupo_usuario`),
  KEY `fk_company_has_grupo_usuario_oficina_company1_idx` (`company_has_grupo_usuario_id_oficina`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `company_has_grupo_usuario`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id_document` int(11) NOT NULL AUTO_INCREMENT,
  `document_document` longblob NOT NULL,
  `document_tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`id_document`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- Volcar la base de datos para la tabla `document`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document_version`
--

CREATE TABLE IF NOT EXISTS `document_version` (
  `id_version` int(11) NOT NULL AUTO_INCREMENT,
  `version_num` int(10) NOT NULL,
  `version_nombre` varchar(45) NOT NULL,
  `version_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_recurso_has_elemento` int(11) NOT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `document_version_visible` tinyint(1) NOT NULL,
  `document_id_document` int(11) NOT NULL,
  PRIMARY KEY (`id_version`,`id_recurso_has_elemento`),
  KEY `fk_version_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_document_version_document1_idx` (`document_id_document`),
  KEY `fk_document_version_recurso_has_elemento1_idx` (`id_recurso_has_elemento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Volcar la base de datos para la tabla `document_version`
--

INSERT INTO `document_version` (`id_version`, `version_num`, `version_nombre`, `version_creacion`, `id_recurso_has_elemento`, `usuario_id_creacion`, `document_version_visible`, `document_id_document`) VALUES
(53, 1, 'ESTRUCTURA AT_v1', '2016-12-20 09:55:14', 9, 10101010, 2, 57),
(54, 2, 'ESTRUCTURA BT_v2', '2016-12-20 09:55:45', 9, 10101010, 2, 58),
(55, 3, 'ESTRUCTURA METIDA A CAJA_v3', '2016-12-20 10:06:53', 9, 10101010, 2, 59),
(56, 4, 'ESTRUCTURA ENSAMBLE_v4', '2016-12-20 10:07:11', 9, 10101010, 2, 60),
(57, 5, 'ESTRUCTURA METALMECANICA_v5', '2016-12-20 10:07:33', 9, 10101010, 1, 61),
(58, 1, 'F-PD1-05_v1', '2016-12-20 10:09:52', 10, 10101010, 1, 62),
(59, 1, 'LISTA DE MATERIALES_v1', '2016-12-20 10:10:45', 11, 10101010, 1, 63),
(60, 1, 'MEDIOS DE PRODUCCION_v1', '2016-12-20 10:11:04', 12, 10101010, 1, 64),
(61, 1, 'PARAMETRO_v1', '2016-12-20 10:12:03', 13, 10101010, 1, 65),
(62, 1, 'ALTA TAP 2_v1', '2016-12-20 10:12:37', 14, 10101010, 2, 66),
(63, 2, 'ANEXO ALTA_v2', '2016-12-20 10:12:57', 14, 10101010, 2, 67),
(64, 3, 'BAJA_v3', '2016-12-20 10:23:13', 14, 10101010, 2, 68),
(65, 4, 'CORTE AISLAMIENTO POR BOBINA_v4', '2016-12-20 10:23:22', 14, 10101010, 2, 69),
(66, 5, 'CRITERIOS DE ACEPTACION - RECHAZO_v5', '2016-12-20 10:23:30', 14, 10101010, 2, 70),
(67, 6, 'ENSAMBLE_v6', '2016-12-20 10:23:39', 14, 10101010, 2, 71),
(68, 7, 'NUCLEOS_v7', '2016-12-20 10:23:46', 14, 10101010, 1, 72),
(69, 1, 'BRIDA_v1', '2016-12-20 10:24:27', 15, 10101010, 2, 73),
(70, 2, 'CONFINAMIENTO Y CONEXIONES_v2', '2016-12-20 10:24:35', 15, 10101010, 2, 74),
(71, 3, 'TANQUE_v3', '2016-12-20 10:24:42', 15, 10101010, 1, 75),
(72, 1, 'PLATINAS_v1', '2016-12-20 10:27:09', 17, 10101010, 1, 76),
(73, 1, 'R-PD1-01_v1', '2016-12-20 10:28:38', 19, 10101010, 1, 77),
(74, 1, 'R-PD1-03_v1', '2016-12-20 10:28:57', 20, 10101010, 1, 78),
(75, 1, 'R-PD1-04_v1', '2016-12-20 10:29:11', 21, 10101010, 1, 79);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elemento`
--

CREATE TABLE IF NOT EXISTS `elemento` (
  `id_elemento` int(11) NOT NULL AUTO_INCREMENT,
  `elemento_nombre` varchar(45) DEFAULT NULL,
  `elemento_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `elemento_update` timestamp NULL DEFAULT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `tipo_recurso_id_tipo_recurso` int(11) NOT NULL,
  PRIMARY KEY (`id_elemento`),
  KEY `fk_elemento_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_tipo_recurso_elemento1_idx` (`tipo_recurso_id_tipo_recurso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Volcar la base de datos para la tabla `elemento`
--

INSERT INTO `elemento` (`id_elemento`, `elemento_nombre`, `elemento_creacion`, `elemento_update`, `usuario_id_creacion`, `tipo_recurso_id_tipo_recurso`) VALUES
(20, 'ANEXOS', '2016-11-29 12:05:20', '2016-11-29 12:05:20', 10101010, 2),
(21, 'ANULADOS', '2016-11-29 12:05:32', '2016-11-29 12:05:32', 10101010, 2),
(22, 'CHECKLIST PARA REVISIÓN DE TRANSFORMADORES TI', '2016-11-29 12:05:48', '2016-11-29 12:05:48', 10101010, 2),
(23, 'ESPECIFICACIONES', '2016-11-29 12:05:53', '2016-11-29 12:05:53', 10101010, 2),
(24, 'ESTRUCTURAS DE MATERIALES', '2016-11-29 12:05:58', '2016-11-29 12:05:58', 10101010, 2),
(25, 'F-PD1-05 (CONTENIDO HOJA DE VIDA DEL DISEÑO)', '2016-11-29 12:06:04', '2016-11-29 12:06:04', 10101010, 2),
(26, 'LISTAS DE MATERIALES (OPCIONAL)', '2016-11-29 12:06:09', '2016-11-29 12:06:09', 10101010, 2),
(27, 'MEDIO DE PRODUCCIÓN', '2016-11-29 12:06:15', '2016-11-29 12:06:15', 10101010, 2),
(28, 'PARÁMETROS', '2016-11-29 12:06:20', '2016-11-29 12:06:20', 10101010, 2),
(29, 'PLANOS PARTE ACTIVA', '2016-11-29 12:06:25', '2016-11-29 12:06:25', 10101010, 2),
(30, 'PLANOS PARTE MECANICA', '2016-11-29 12:06:30', '2016-11-29 12:06:30', 10101010, 2),
(31, 'PLATINA O TERMINAL', '2016-11-29 12:06:35', '2016-11-29 12:06:35', 10101010, 2),
(32, 'RESULTADOS DE PRUEBAS', '2016-11-29 12:06:42', '2016-11-29 12:06:42', 10101010, 2),
(33, 'R-PD1-01 (HOJA DE VIDA DEL DISEÑO)', '2016-11-29 12:06:47', '2016-11-29 12:06:47', 10101010, 2),
(34, 'R-PD1-03 (CALCULO PARTE ACTIVA)', '2016-11-29 12:06:51', '2016-11-29 12:06:51', 10101010, 2),
(35, 'R-PD1-04 ( CALCULO PARTE MECÁNICA)', '2016-11-29 12:06:56', '2016-11-29 12:06:56', 10101010, 2),
(36, 'R-PD1-09 (VALIDACIÓN DE PROTOTIPO)', '2016-11-29 12:07:02', '2016-11-29 12:07:02', 10101010, 2),
(37, 'TARJETA CALCULO AISLAMIENTO DE BOBINAS', '2016-11-29 12:07:07', '2016-11-29 12:07:07', 10101010, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_usuario`
--

CREATE TABLE IF NOT EXISTS `grupo_usuario` (
  `id_grupo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_usuario_nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_grupo_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcar la base de datos para la tabla `grupo_usuario`
--

INSERT INTO `grupo_usuario` (`id_grupo_usuario`, `grupo_usuario_nombre`) VALUES
(1, 'Diseños Maquinado'),
(2, 'Diseños Nucleos'),
(4, 'Diseños Soldadura'),
(5, 'Diseños Ventas PL1'),
(6, 'Gestión Humana'),
(8, 'Diseños Pintura'),
(9, 'TICS (Epicor)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficina_company`
--

CREATE TABLE IF NOT EXISTS `oficina_company` (
  `id_oficina_company` int(11) NOT NULL AUTO_INCREMENT,
  `oficina_company_nombre` varchar(45) DEFAULT NULL,
  `oficina_company_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_oficina_company`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `oficina_company`
--

INSERT INTO `oficina_company` (`id_oficina_company`, `oficina_company_nombre`, `oficina_company_update`) VALUES
(1, 'Planta 1', '2016-10-21 10:34:09'),
(2, 'Planta 2', '2016-10-21 10:34:09'),
(3, 'Zona Franca', '2016-10-21 10:34:18'),
(4, 'Barranquilla', '2016-11-11 11:32:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_usuario`
--

CREATE TABLE IF NOT EXISTS `perfil_usuario` (
  `id_perfil_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_usuario_nombre` varchar(45) DEFAULT NULL,
  `perfil_usuario_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_perfil_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `perfil_usuario`
--

INSERT INTO `perfil_usuario` (`id_perfil_usuario`, `perfil_usuario_nombre`, `perfil_usuario_update`) VALUES
(1, 'SuperAdmin', '2016-10-21 10:30:12'),
(2, 'Admin', '2016-10-21 10:30:12'),
(3, 'User', '2016-10-21 10:30:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE IF NOT EXISTS `proceso` (
  `id_proceso` int(11) NOT NULL AUTO_INCREMENT,
  `proceso_nombre` varchar(45) DEFAULT NULL,
  `proceso_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `proceso_update` timestamp NULL DEFAULT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `usuario_id_responsable` int(11) NOT NULL,
  `seccion_company_id_seccion_company` int(11) NOT NULL,
  `oficina_company_id_oficina_company` int(11) NOT NULL,
  PRIMARY KEY (`id_proceso`),
  KEY `fk_proceso_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_proceso_seccion_company1_idx` (`seccion_company_id_seccion_company`),
  KEY `fk_proceso_oficina_company1_idx` (`oficina_company_id_oficina_company`),
  KEY `fk_proceso_usuario2_idx` (`usuario_id_responsable`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcar la base de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id_proceso`, `proceso_nombre`, `proceso_creacion`, `proceso_update`, `usuario_id_creacion`, `usuario_id_responsable`, `seccion_company_id_seccion_company`, `oficina_company_id_oficina_company`) VALUES
(8, 'DISEÑOS DE INGENIERÍA (PRUEBA)', '2016-11-29 12:03:55', '2016-11-29 12:03:55', 1088004425, 10101010, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recurso`
--

CREATE TABLE IF NOT EXISTS `recurso` (
  `id_recurso` int(11) NOT NULL AUTO_INCREMENT,
  `recurso_nombre` varchar(45) DEFAULT NULL,
  `recurso_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `recurso_update` timestamp NULL DEFAULT NULL,
  `usuario_id_creacion` int(11) NOT NULL,
  `proceso_id_proceso` int(11) NOT NULL,
  `tipo_recurso_id_tipo_recurso` int(11) NOT NULL,
  PRIMARY KEY (`id_recurso`,`proceso_id_proceso`),
  KEY `fk_recurso_usuario1_idx` (`usuario_id_creacion`),
  KEY `fk_tipo_recurso_id_tipo_recurso_idx` (`tipo_recurso_id_tipo_recurso`),
  KEY `fk_proceso_id_proceso_idx` (`proceso_id_proceso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Volcar la base de datos para la tabla `recurso`
--

INSERT INTO `recurso` (`id_recurso`, `recurso_nombre`, `recurso_creacion`, `recurso_update`, `usuario_id_creacion`, `proceso_id_proceso`, `tipo_recurso_id_tipo_recurso`) VALUES
(26, '966-T-0115-0F-000-00', '2016-11-29 12:04:49', '2016-11-29 12:04:49', 10101010, 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recurso_has_elemento`
--

CREATE TABLE IF NOT EXISTS `recurso_has_elemento` (
  `id_recurso_has_elemento` int(11) NOT NULL AUTO_INCREMENT,
  `recurso_id_recurso` int(11) NOT NULL,
  `elemento_id_elemento` int(11) NOT NULL,
  PRIMARY KEY (`id_recurso_has_elemento`,`recurso_id_recurso`,`elemento_id_elemento`),
  KEY `fk_recurso_has_elemento_elemento1_idx` (`elemento_id_elemento`),
  KEY `fk_recurso_has_elemento_recurso1_idx` (`recurso_id_recurso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Volcar la base de datos para la tabla `recurso_has_elemento`
--

INSERT INTO `recurso_has_elemento` (`id_recurso_has_elemento`, `recurso_id_recurso`, `elemento_id_elemento`) VALUES
(5, 26, 20),
(6, 26, 21),
(7, 26, 22),
(8, 26, 23),
(9, 26, 24),
(10, 26, 25),
(11, 26, 26),
(12, 26, 27),
(13, 26, 28),
(14, 26, 29),
(15, 26, 30),
(16, 26, 30),
(17, 26, 31),
(18, 26, 32),
(19, 26, 33),
(20, 26, 34),
(21, 26, 35),
(22, 26, 36),
(23, 26, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_company`
--

CREATE TABLE IF NOT EXISTS `seccion_company` (
  `id_seccion_company` int(11) NOT NULL AUTO_INCREMENT,
  `seccion_company_nombre` varchar(45) DEFAULT NULL,
  `seccion_company_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_seccion_company`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcar la base de datos para la tabla `seccion_company`
--

INSERT INTO `seccion_company` (`id_seccion_company`, `seccion_company_nombre`, `seccion_company_update`) VALUES
(1, 'Tics', '2016-10-21 10:33:46'),
(2, 'Ingenieria', '2016-10-21 10:33:46'),
(3, 'Recursos Humanos', '2016-11-11 11:32:29'),
(4, 'Producción', '2016-11-11 11:32:43'),
(5, 'Soldadura', '2016-11-28 08:27:37'),
(7, 'Pintura', '2016-11-28 08:45:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_recurso`
--

CREATE TABLE IF NOT EXISTS `tipo_recurso` (
  `id_tipo_recurso` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_recurso_nombre` varchar(45) DEFAULT NULL COMMENT 'Nombre del Recurso para identificar que acceso esta siendo asignado a que usuario:\n\nRecurso (Nombre del diseño o de HV)\nElemento (componente del diseño o resgistro de HV)',
  PRIMARY KEY (`id_tipo_recurso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `tipo_recurso`
--

INSERT INTO `tipo_recurso` (`id_tipo_recurso`, `tipo_recurso_nombre`) VALUES
(1, 'Recurso'),
(2, 'Elemento'),
(3, 'Proceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario_id` int(20) NOT NULL,
  `usuario_nombre` varchar(90) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `seccion_company_id_seccion_company` int(11) NOT NULL,
  `oficina_company_id_oficina_company` int(11) NOT NULL,
  PRIMARY KEY (`usuario_id`),
  KEY `fk_usuario_seccion_company1_idx` (`seccion_company_id_seccion_company`),
  KEY `fk_usuario_oficina_company1_idx` (`oficina_company_id_oficina_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario_nombre`, `username`, `password`, `email`, `fecha_creacion`, `seccion_company_id_seccion_company`, `oficina_company_id_oficina_company`) VALUES
(1010, 'Soldadura Planta 2', 'solda2', '123', 'soldadura@magnetron.com.co', '2016-11-28 08:44:49', 5, 2),
(2020, 'Pintura Planta 2', 'pintura2', '123', 'pintura@magnetron.com.co', '2016-11-28 08:46:03', 7, 2),
(2021, 'Pintura Planta 1', 'pintura1', '123', 'pintura1@magnetron.com.co', '2016-11-28 08:46:37', 7, 1),
(4510438, 'Isabela Adriana', 'isa', '123', 'isabela@magnetron.com.co', '2016-11-10 10:18:12', 1, 1),
(10101010, 'Jeison Restrepo', 'jrestrepo', '123', 'jeisonrestrepo@magnetron.com.co', '2016-11-29 11:55:22', 2, 1),
(34051895, 'Jaime Andres Quiceno', 'jaime', '123', 'jaimequiceno@magnetron.com.co', '2016-11-10 10:22:56', 2, 2),
(1002593980, 'Diego Fernando Moya', 'dmoya', '123', 'dmoya@magnetron.com.co', '2016-11-01 09:13:22', 1, 2),
(1088004425, 'Jeison Stevens Córdoba', 'scordoba', '123', 'jeisoncordoba@magnetron.com.co', '2016-10-24 10:05:53', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_has_grupo`
--

CREATE TABLE IF NOT EXISTS `usuario_has_grupo` (
  `usuario_usuario_id` int(20) NOT NULL,
  `grupo_usuario_id_grupo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`usuario_usuario_id`,`grupo_usuario_id_grupo_usuario`),
  KEY `fk_usuario_has_grupo_usuario_grupo_usuario1_idx` (`grupo_usuario_id_grupo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `usuario_has_grupo`
--

INSERT INTO `usuario_has_grupo` (`usuario_usuario_id`, `grupo_usuario_id_grupo_usuario`) VALUES
(1088004425, 1),
(2021, 5),
(1088004425, 5),
(2020, 8),
(2021, 8),
(4510438, 9),
(34051895, 9),
(1002593980, 9),
(1088004425, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_has_perfil`
--

CREATE TABLE IF NOT EXISTS `usuario_has_perfil` (
  `usuario_usuario_id` int(20) NOT NULL,
  `perfil_usuario_id_perfil_usuario` int(11) NOT NULL,
  PRIMARY KEY (`usuario_usuario_id`,`perfil_usuario_id_perfil_usuario`),
  KEY `fk_usuario_has_perfil_usuario_perfil_usuario1_idx` (`perfil_usuario_id_perfil_usuario`),
  KEY `fk_usuario_has_perfil_usuario_usuario1_idx` (`usuario_usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `usuario_has_perfil`
--

INSERT INTO `usuario_has_perfil` (`usuario_usuario_id`, `perfil_usuario_id_perfil_usuario`) VALUES
(1088004425, 1),
(4510438, 2),
(10101010, 2),
(1002593980, 2),
(1010, 3),
(2020, 3),
(2021, 3),
(34051895, 3),
(1088004425, 3);

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `acceso`
--
ALTER TABLE `acceso`
  ADD CONSTRAINT `fk_acceso_grupo_usuario1` FOREIGN KEY (`grupo_usuario_id_grupo_usuario`) REFERENCES `grupo_usuario` (`id_grupo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_acceso_tipo_recurso1` FOREIGN KEY (`tipo_recurso_id_tipo_recurso`) REFERENCES `tipo_recurso` (`id_tipo_recurso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `company_has_grupo_usuario`
--
ALTER TABLE `company_has_grupo_usuario`
  ADD CONSTRAINT `fk_company_has_grupo_usuario_oficina_company1` FOREIGN KEY (`company_has_grupo_usuario_id_oficina`) REFERENCES `oficina_company` (`id_oficina_company`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_company_has_grupo_usuario_seccion_company1` FOREIGN KEY (`company_has_grupo_usuario_seccion`) REFERENCES `seccion_company` (`id_seccion_company`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_seccion_company_has_oficina_company_has_grupo_usuario_grup1` FOREIGN KEY (`grupo_usuario_id_grupo_usuario`) REFERENCES `grupo_usuario` (`id_grupo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `document_version`
--
ALTER TABLE `document_version`
  ADD CONSTRAINT `fk_document_version_document1` FOREIGN KEY (`document_id_document`) REFERENCES `document` (`id_document`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_document_version_recurso_has_elemento1` FOREIGN KEY (`id_recurso_has_elemento`) REFERENCES `recurso_has_elemento` (`id_recurso_has_elemento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_version_usuario1` FOREIGN KEY (`usuario_id_creacion`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `elemento`
--
ALTER TABLE `elemento`
  ADD CONSTRAINT `fk_elemento_usuario1` FOREIGN KEY (`usuario_id_creacion`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tipo_recurso_elemento1` FOREIGN KEY (`tipo_recurso_id_tipo_recurso`) REFERENCES `tipo_recurso` (`id_tipo_recurso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD CONSTRAINT `fk_proceso_oficina_company1` FOREIGN KEY (`oficina_company_id_oficina_company`) REFERENCES `oficina_company` (`id_oficina_company`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proceso_seccion_company1` FOREIGN KEY (`seccion_company_id_seccion_company`) REFERENCES `seccion_company` (`id_seccion_company`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proceso_usuario1` FOREIGN KEY (`usuario_id_creacion`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_proceso_usuario2` FOREIGN KEY (`usuario_id_responsable`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `recurso`
--
ALTER TABLE `recurso`
  ADD CONSTRAINT `fk_proceso_id_proceso` FOREIGN KEY (`proceso_id_proceso`) REFERENCES `proceso` (`id_proceso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurso_usuario1` FOREIGN KEY (`usuario_id_creacion`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tipo_recurso_id_tipo_recurso` FOREIGN KEY (`tipo_recurso_id_tipo_recurso`) REFERENCES `tipo_recurso` (`id_tipo_recurso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recurso_has_elemento`
--
ALTER TABLE `recurso_has_elemento`
  ADD CONSTRAINT `fk_recurso_has_elemento_elemento1` FOREIGN KEY (`elemento_id_elemento`) REFERENCES `elemento` (`id_elemento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurso_has_elemento_recurso1` FOREIGN KEY (`recurso_id_recurso`) REFERENCES `recurso` (`id_recurso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_oficina_company1` FOREIGN KEY (`oficina_company_id_oficina_company`) REFERENCES `oficina_company` (`id_oficina_company`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usuario_seccion_company1` FOREIGN KEY (`seccion_company_id_seccion_company`) REFERENCES `seccion_company` (`id_seccion_company`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_has_grupo`
--
ALTER TABLE `usuario_has_grupo`
  ADD CONSTRAINT `fk_usuario_has_grupo_usuario_grupo_usuario1` FOREIGN KEY (`grupo_usuario_id_grupo_usuario`) REFERENCES `grupo_usuario` (`id_grupo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_grupo_usuario_usuario1` FOREIGN KEY (`usuario_usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_has_perfil`
--
ALTER TABLE `usuario_has_perfil`
  ADD CONSTRAINT `fk_usuario_has_perfil_usuario_perfil_usuario1` FOREIGN KEY (`perfil_usuario_id_perfil_usuario`) REFERENCES `perfil_usuario` (`id_perfil_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_perfil_usuario_usuario1` FOREIGN KEY (`usuario_usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE CASCADE;
